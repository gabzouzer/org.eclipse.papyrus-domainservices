/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * This helper provides some util method on {@link Collaboration}.
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class CollaborationHelper {

    /**
     * <pre>
     * This method retrieve all the role bindings related to a specific role.
     * Copied from org.eclipse.papyrus.uml.service.types.helper.advice.CollaborationHelperAdvice.getRelatedRoleBindings(Collaboration, ConnectableElement)
     * </pre>
     * 
     * @param collaboration
     *                      the {@link Collaboration} owning the role
     * @param role
     *                      the role (if null, all roles are considered)
     * @return role bindings connecting the role
     */
    public static Set<Dependency> getRelatedRoleBindings(Collaboration collaboration, ConnectableElement role,
            ECrossReferenceAdapter crossReferenceAdapter) {

        Set<Dependency> roleBindings = new HashSet<Dependency>();

        EReference[] ref = { UMLPackage.eINSTANCE.getCollaborationUse_Type() };
        Collection<?> refs = UMLService.getReferencers(collaboration, ref, crossReferenceAdapter);

        Iterator<?> it = refs.iterator();

        while (it.hasNext()) {
            Object object = it.next();
            if (object instanceof CollaborationUse) {

                CollaborationUse collaborationUse = (CollaborationUse) object;

                Iterator<Dependency> itBindings = collaborationUse.getRoleBindings().iterator();
                while (itBindings.hasNext()) {
                    Dependency roleBinding = itBindings.next();

                    if ((role == null) || (roleBinding.getClients().contains(role))) {
                        roleBindings.add(roleBinding);
                    }
                }
            }
        }
        return roleBindings;
    }
}

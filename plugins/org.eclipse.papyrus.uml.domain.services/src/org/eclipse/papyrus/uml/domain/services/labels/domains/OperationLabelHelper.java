/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;

/**
 * Helper for {@link Operation} label.
 * 
 * @author fbarbin
 */
public class OperationLabelHelper {

    private final ParameterLabelHelper parameterLabelHelper;
    private final VisibilityLabelHelper visibilityLabelHelper;
    private final INamedElementNameProvider namedElementNameProvider;

    public OperationLabelHelper(ParameterLabelHelper parameterLabelHelper, VisibilityLabelHelper visibilityLabelHelper,
            INamedElementNameProvider namedElementNameProvider) {
        this.parameterLabelHelper = Objects.requireNonNull(parameterLabelHelper);
        this.visibilityLabelHelper = Objects.requireNonNull(visibilityLabelHelper);
        this.namedElementNameProvider = Objects.requireNonNull(namedElementNameProvider);
    }

    public String getLabel(Operation operation) {
        String operationLabel = getNonNullString(visibilityLabelHelper.getVisibilityAsSign(operation))
                + UMLCharacters.SPACE;
        String parameters = getInnerParameters(operation);
        String returnType = getFirstReturnParameter(operation).map(this::computeReturnParameterLabel).orElse("");

        return operationLabel + this.namedElementNameProvider.getName(operation) + parameters + returnType;
    }

    private String getInnerParameters(Operation operation) {
        //@formatter:off
        return operation.getOwnedParameters().stream()
                .filter(p -> !ParameterDirectionKind.RETURN_LITERAL.equals(p.getDirection()))
                .map(this::computeOperationParameterLabel)
                .collect(Collectors.joining(UMLCharacters.COMMA + UMLCharacters.SPACE,
                        UMLCharacters.OPEN_PARENTHESE, UMLCharacters.CLOSE_PARENTHESE));
        //@formatter:on
    }

    private String computeOperationParameterLabel(Parameter parameter) {
        return this.parameterLabelHelper.getLabel(parameter);
    }

    private Optional<Parameter> getFirstReturnParameter(Operation operation) {
        return Optional.ofNullable(operation.getReturnResult());
    }

    private String computeReturnParameterLabel(Parameter parameter) {
        return UMLCharacters.D_DOTS + UMLCharacters.SPACE + Optional.ofNullable(parameter.getType())
                .map(type -> this.namedElementNameProvider.getName(type))
                .orElse(ParameterLabelHelper.UNDEFINED_TYPE_NAME);
    }
}

/*******************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.services;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Utility class for Classifiers. Copied from
 * org.eclipse.papyrus.uml.service.types.utils.ClassifierUtils.
 * 
 * @author Vincent Blain
 */
public class ClassifierUtils {

    /**
     * This method provides a switch to ease the addition of a new Property in a
     * Classifier.
     * 
     * @param classifier
     *                   the parent classifier.
     * @param property
     *                   the contained property.
     * @return false if the addition fails.
     */
    public static boolean addOwnedAttribute(Classifier classifier, Property property) {
        AddOwnedAttributeSwitch classifierSwitch = new AddOwnedAttributeSwitch(property);
        return classifierSwitch.doSwitch(classifier);
    }

    /**
     * Switch implementation for Property addition.
     */
    private static class AddOwnedAttributeSwitch extends UMLSwitch<Boolean> {

        private final Property property;

        AddOwnedAttributeSwitch(Property property) {
            super();

            this.property = property;
        }

        @Override
        public Boolean caseArtifact(Artifact object) {
            object.getOwnedAttributes().add(property);
            return Boolean.TRUE;
        }

        @Override
        public Boolean caseDataType(DataType object) {
            object.getOwnedAttributes().add(property);
            return Boolean.TRUE;
        }

        @Override
        public Boolean caseInterface(Interface object) {
            object.getOwnedAttributes().add(property);
            return Boolean.TRUE;
        }

        @Override
        public Boolean caseSignal(Signal object) {
            object.getOwnedAttributes().add(property);
            return Boolean.TRUE;
        }

        @Override
        public Boolean caseStructuredClassifier(StructuredClassifier object) {
            object.getOwnedAttributes().add(property);
            return Boolean.TRUE;
        }

        @Override
        public Boolean caseClass(Class object) {
            object.getOwnedAttributes().add(property);
            return Boolean.TRUE;
        }

        @Override
        public Boolean defaultCase(EObject object) {
            return Boolean.FALSE;
        }
    };

}

/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

import static java.util.stream.Collectors.toList;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.destroy.ElementDestroyer;
import org.eclipse.papyrus.uml.domain.services.services.CollaborationHelper;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Most generic UML {@link IFeatureModifier}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementFeatureModifier implements IFeatureModifier {

    private static final String CANNOT_EDIT_MSG = "Selected object cannot be edited";

    private final ECrossReferenceAdapter crossRef;

    private final IEditableChecker editableChecker;

    public ElementFeatureModifier(ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        super();
        this.crossRef = crossRef;
        this.editableChecker = editableChecker;
    }

    @Override
    public Status setValue(EObject featureOwner, String featureName, Object newValue) {
        if (!editableChecker.canEdit(featureOwner)) {
            return Status.createFailingStatus(CANNOT_EDIT_MSG);
        }

        return new ElementFeatureModifierSwitch(featureName, newValue, ModificationType.SET, editableChecker, crossRef)
                .doSwitch(featureOwner);
    }

    @Override
    public Status addValue(EObject featureOwner, String featureName, Object newValue) {
        if (!editableChecker.canEdit(featureOwner)) {
            return Status.createFailingStatus(CANNOT_EDIT_MSG);
        }
        return new ElementFeatureModifierSwitch(featureName, newValue, ModificationType.ADD, editableChecker, crossRef)
                .doSwitch(featureOwner);
    }

    @Override
    public Status removeValue(EObject featureOwner, String featureName, Object newValue) {
        if (!editableChecker.canEdit(featureOwner)) {
            return Status.createFailingStatus(CANNOT_EDIT_MSG);
        }
        return new ElementFeatureModifierSwitch(featureName, newValue, ModificationType.REMOVE, editableChecker,
                crossRef).doSwitch(featureOwner);
    }

    static class ElementFeatureModifierSwitch extends UMLSwitch<Status> {

        private final String featureName;

        private final Object newValue;

        private final ModificationType operationType;

        private IEditableChecker editableChecker;

        private ECrossReferenceAdapter crossRef;

        ElementFeatureModifierSwitch(String featureName, Object newValue, ModificationType operationType,
                IEditableChecker editableChecker, ECrossReferenceAdapter crossRef) {
            super();
            this.featureName = featureName;
            this.newValue = newValue;
            this.operationType = operationType;
            this.editableChecker = editableChecker;
            this.crossRef = crossRef;
        }

        @Override
        public Status caseComponent(Component object) {
            if (newValue instanceof Component
                    && UMLPackage.eINSTANCE.getClass_NestedClassifier().getName().equals(featureName)) {
                return Status.createFailingStatus(
                        "Cannot create a Component under another Component using the " + featureName + " feature.");
            }
            return super.caseComponent(object);
        }

        @Override
        public Status caseProperty(Property object) {

            setNoneLiteralToMemberEndsAggregation(object);

            boolean isNullTypeSet = deleteAssociationWithNullType(object);

            deleteConnectorEndPartWithRole(object);

            if (!isNullTypeSet) {
                // in case of type set to null, setting type was mandatory before several action
                // so setting type is not launch at the end.
                return super.caseProperty(object);
            } else {
                return Status.DONE;
            }
        }

        /**
         * See
         * org.eclipse.papyrus.uml.service.types.helper.advice.PropertyHelperAdvice.getBeforeSetCommand(SetRequest)
         * 
         * For a property referenced by an association, we ensure that all properties of
         * this association are not "shared" or "composite". Only one member end can be
         * "shared" or "composite".
         * 
         * @param property
         *                 the property to edit
         */
        private void setNoneLiteralToMemberEndsAggregation(Property property) {
            if (UMLPackage.eINSTANCE.getProperty_Aggregation().getName().equals(featureName)
                    && !(property instanceof Port) && newValue != AggregationKind.NONE_LITERAL) {
                Association association = property.getAssociation();
                if (association != null) {
                    Set<Property> members = new HashSet<>();
                    members.addAll(association.getMemberEnds());
                    members.remove(property);
                    ElementFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
                    for (Property member : members) {
                        if (member.getAggregation() != AggregationKind.NONE_LITERAL) {
                            // Two member ends of an association cannot be set to composite at the same
                            // time. To avoid such a situation, other ends are changed into aggregation
                            // "none".
                            modifier.setValue(member, UMLPackage.eINSTANCE.getProperty_Aggregation().getName(),
                                    AggregationKind.NONE_LITERAL);
                        }
                    }
                }
            }
        }

        /**
         * See
         * org.eclipse.papyrus.uml.service.types.helper.advice.PropertyHelperAdvice.getBeforeSetCommand(SetRequest)
         * 
         * Type set to null implies the property should be kept and the association
         * deleted: see https://bugs.eclipse.org/bugs/show_bug.cgi?id=477724
         * 
         * @param property
         *                 the property to edit
         */
        private boolean deleteAssociationWithNullType(Property property) {
            boolean isNullTypeSet = false;
            if (UMLPackage.eINSTANCE.getTypedElement_Type().getName().equals(featureName) && !(property instanceof Port)
                    && newValue == null) {
                // Type set to null implies the property should be kept and the association
                // deleted: see https://bugs.eclipse.org/bugs/show_bug.cgi?id=477724
                List<Association> associationRefs = crossRef
                        .getInverseReferences(property, UMLPackage.eINSTANCE.getAssociation_MemberEnd(), true).stream()
                        .map(s -> (Association) s.getEObject())//
                        .collect(toList());

                // Type set before destroy association to avoid to delete this property with
                // association
                property.eSet(UMLPackage.eINSTANCE.getTypedElement_Type(), newValue);
                isNullTypeSet = true;
                ElementDestroyer deleter = ElementDestroyer.buildDefault(crossRef, editableChecker);
                for (Association association : associationRefs) {
                    if (association.getMembers().size() <= 2) {
                        deleter.destroy(association);
                    }
                }
            }
            return isNullTypeSet;
        }

        /**
         * When the type of a property change we need to delete all ConnectorEnds with
         * portWithPart referencing the given property
         * 
         * @param property
         *                 a Property
         */
        private void deleteConnectorEndPartWithRole(Property property) {
            if (UMLPackage.eINSTANCE.getTypedElement_Type().getName().equals(featureName)
                    && !(property instanceof Port)) {

                List<ConnectorEnd> connectorEnds = crossRef
                        .getInverseReferences(property, UMLPackage.eINSTANCE.getConnectorEnd_PartWithPort(), true)
                        .stream().map(s -> (ConnectorEnd) s.getEObject())//
                        .collect(toList());

                ElementDestroyer deleter = ElementDestroyer.buildDefault(crossRef, editableChecker);

                List<Property> validProperties = new ArrayList<>();
                if (newValue instanceof StructuredClassifier) {
                    StructuredClassifier strucClassifier = (StructuredClassifier) newValue;
                    validProperties = strucClassifier.allAttributes();
                } else if (newValue instanceof DataType) {
                    validProperties = ((DataType) newValue).allAttributes();
                } else {
                    validProperties = List.of();
                }
                for (ConnectorEnd connectorEnd : connectorEnds) {
                    if (connectorEnd.getRole() == null || !validProperties.contains(connectorEnd.getRole())) {
                        deleter.destroy(connectorEnd);
                    }
                }
            }
        }

        /**
         * When removing a ConnectableElement from the Collaboration#collaborationRole
         * feature, destroy the related role bindings.
         * 
         * @see org.eclipse.papyrus.uml.service.types.helper.advice.CollaborationHelperAdvice.getBeforeSetCommand(SetRequest)
         *
         * @param collaboration
         *                      the modified collaboration
         */
        @Override
        public Status caseCollaboration(Collaboration collaboration) {
            if (UMLPackage.eINSTANCE.getCollaboration_CollaborationRole().getName().equals(featureName)
                    && ModificationType.REMOVE.equals(operationType) && newValue instanceof ConnectableElement) {
                Set<Dependency> relatedRoleBindings = CollaborationHelper.getRelatedRoleBindings(collaboration,
                        (ConnectableElement) newValue, crossRef);
                ElementDestroyer deleter = ElementDestroyer.buildDefault(crossRef, editableChecker);
                for (Dependency roleBinding : relatedRoleBindings) {
                    deleter.destroy(roleBinding);
                }
            }
            return super.caseCollaboration(collaboration);
        }

        @Override
        public Status caseCollaborationUse(CollaborationUse collaborationUse) {
            if (UMLPackage.eINSTANCE.getCollaborationUse_Type().getName().equals(featureName)
                    && collaborationUse.getType() != newValue
                    && (newValue instanceof Collaboration || newValue == null)) {
                List<Dependency> roleBindings = new ArrayList<>(collaborationUse.getRoleBindings());
                ElementDestroyer deleter = ElementDestroyer.buildDefault(crossRef, editableChecker);
                for (Dependency roleBinding : roleBindings) {
                    deleter.destroy(roleBinding);
                }
            }
            return super.caseCollaborationUse(collaborationUse);
        }

        /**
         * If the last InteractionOperand of a CombinedFragment is removed, remove the
         * CombinedFragment. See Papyrus behavior:
         * org.eclipse.papyrus.uml.service.types.helper.advice.InteractionOperandEditHelperAdvice.
         * 
         *
         * @param combinedFragment
         *                         the modified combinedFragment
         * @return the status of the modification
         */
        @Override
        public Status caseCombinedFragment(CombinedFragment combinedFragment) {
            if (UMLPackage.eINSTANCE.getCombinedFragment_Operand().getName().equals(featureName)
                    && newValue instanceof InteractionOperand && ModificationType.REMOVE.equals(operationType)) {
                if (combinedFragment.getOperands().size() == 1 && combinedFragment.getOperands().get(0) == newValue) {
                    ElementDestroyer deleter = ElementDestroyer.buildDefault(crossRef, editableChecker);
                    deleter.destroy((InteractionOperand) newValue);
                }
            }
            return super.caseCombinedFragment(combinedFragment);
        }

        /**
         * If an InteractionOperand is created under another InteractionOperand, it must
         * be set as a sibling, not a child. See Papyrus behavior:
         * org.eclipse.papyrus.uml.service.types.helper.advice.InteractionOperandEditHelperAdvice.
         *
         * @param interactionOperand
         *                           the edited operand
         * @return the modifier status
         */
        @Override
        public Status caseInteractionOperand(InteractionOperand interactionOperand) {
            if (UMLPackage.eINSTANCE.getInteractionOperand_Fragment().getName().equals(featureName)
                    && newValue instanceof InteractionOperand
                    && interactionOperand.getOwner() instanceof CombinedFragment) {
                CombinedFragment combinedFragment = (CombinedFragment) interactionOperand.getOwner();
                ElementFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
                return modifier.addValue(combinedFragment, UMLPackage.eINSTANCE.getCombinedFragment_Operand().getName(),
                        newValue);
            }
            return super.caseInteractionOperand(interactionOperand);
        }

        @Override
        public Status defaultCase(EObject object) {

            EClass eClass = object.eClass();
            EStructuralFeature feature = eClass.getEStructuralFeature(featureName);

            final Status result;
            if (feature == null) {
                result = Status.createFailingStatus(
                        MessageFormat.format("Unkown feature {0} on {1}", featureName, eClass.getName()));
            } else if (feature.isDerived() || !feature.isChangeable()) {
                result = Status.createFailingStatus(
                        MessageFormat.format("{0} can''t be modified on the selected element.", featureName));
            } else if (newValue != null && !feature.getEType().isInstance(newValue)) {
                return Status
                        .createFailingStatus("Invalid value type for reference " + featureName + " object " + newValue);
            } else {
                result = genericSet(object, feature, newValue);
            }

            return result;
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
        private Status genericSet(EObject owner, EStructuralFeature feature, Object newValueToSet) {

            switch (operationType) {
            case ADD:
                if (feature.isMany()) {
                    ((List) owner.eGet(feature)).add(newValueToSet);
                } else {
                    owner.eSet(feature, newValueToSet);
                }
                break;
            case REMOVE:
                if (feature.isMany()) {
                    ((List) owner.eGet(feature)).remove(newValueToSet);
                } else {
                    return Status.createFailingStatus("Unable to remove a value from an unary feature :" + featureName);
                }
                break;
            case SET:
                if (feature.isMany()) {
                    List currentValue = (List) owner.eGet(feature);
                    currentValue.clear();
                    currentValue.add(newValueToSet);
                } else {
                    owner.eSet(feature, newValueToSet);
                }
                break;

            default:
                break;
            }

            return Status.DONE;
        }

    }

}

/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.connector.ConnectorHelper;
import org.eclipse.papyrus.uml.domain.services.extension.ExtensionHelper;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class ElementDomainBasedEdgeReconnectionTargetChecker implements IDomainBasedEdgeReconnectionTargetChecker {

    private final IEditableChecker editableChecker;

    private final IViewQuerier representationQuery;

    public ElementDomainBasedEdgeReconnectionTargetChecker(IEditableChecker editableChecker,
            IViewQuerier representationQuery) {
        super();
        this.editableChecker = Objects.requireNonNull(editableChecker);
        this.representationQuery = Objects.requireNonNull(representationQuery);
    }

    @Override
    public CheckStatus canReconnect(EObject edgeToReconnect, EObject oldSemanticEdgeTarget,
            EObject newSemanticEdgeTarget, Object newTargetView, Object sourceView) {
        if (newSemanticEdgeTarget == null) {
            return CheckStatus.no("The new semantic edge target must not be null");
        }
        return new ElementDomainBasedEdgeReconnectionTargetCheckerSwitch(oldSemanticEdgeTarget, newSemanticEdgeTarget,
                newTargetView, sourceView, this.editableChecker, this.representationQuery).doSwitch(edgeToReconnect);
    }

    public static class ElementDomainBasedEdgeReconnectionTargetCheckerSwitch extends UMLSwitch<CheckStatus> {

        private static final String INVALID_USE_CASE_TARGET = "Invalid Use Case target";

        private static final String INVALID_PACKAGEABLE_ELEMENT_TARGET = "Invalid Packageable Element target";

        private static final String INVALID_PACKAGE_CONTAINER = "Invalid Package container";

        private static final String INVALID_NAMED_ELEMENT_SOURCE = "Invalid Named Element target";;

        private final EObject oldSemanticEdgeTarget;

        private final EObject newSemanticEdgeTarget;

        private final Object newTargetView;

        private final Object sourceView;

        private final IEditableChecker editableChecker;

        private final IViewQuerier representationQuery;

        public ElementDomainBasedEdgeReconnectionTargetCheckerSwitch(EObject oldSemanticEdgeTarget,
                EObject newSemanticEdgeTarget, Object newTargetView, Object sourceView,
                IEditableChecker editableChecker, IViewQuerier representationQuery) {
            super();
            this.oldSemanticEdgeTarget = oldSemanticEdgeTarget;
            this.newSemanticEdgeTarget = newSemanticEdgeTarget;
            this.newTargetView = newTargetView;
            this.sourceView = sourceView;
            this.editableChecker = editableChecker;
            this.representationQuery = representationQuery;
        }

        @Override
        public CheckStatus caseAssociation(Association association) {
            final CheckStatus result;
            Property targetProperty = association.getMemberEnds().get(1);
            if (!(oldSemanticEdgeTarget instanceof Classifier && newSemanticEdgeTarget instanceof Classifier)) {
                result = CheckStatus.no("Invalid Association target: the new target should be a Classifier");
            } else if (!association.getOwnedEnds().contains(targetProperty)) {
                // Check that the new target can contain the targetProperty if this one was not
                // owned by the association
                // CHECKSTYLE:OFF
                boolean cannotContainProperty = !(newSemanticEdgeTarget instanceof Artifact
                        || newSemanticEdgeTarget instanceof DataType || newSemanticEdgeTarget instanceof Interface
                        || newSemanticEdgeTarget instanceof Signal
                        || newSemanticEdgeTarget instanceof StructuredClassifier);
                // CHECKSTYLE:ON
                if (cannotContainProperty) {
                    result = CheckStatus.no(
                            "Invalid Association target: the new target must be able to contain the TargetProperty attribute");
                } else {
                    result = CheckStatus.YES;
                }
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseConnector(Connector connector) {
            ConnectorHelper connectorHelper = new ConnectorHelper();
            return connectorHelper.canCreateConnector(this.representationQuery, this.sourceView, this.newTargetView);
        }

        @Override
        public CheckStatus caseDependency(Dependency object) {
            final CheckStatus result;
            if (newSemanticEdgeTarget == null || !(newSemanticEdgeTarget instanceof NamedElement)) {
                result = CheckStatus.no("Dependency target can only be reconnected to a non null NamedElement.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseExtend(Extend extend) {
            final CheckStatus result;
            if (newSemanticEdgeTarget == null || !(newSemanticEdgeTarget instanceof UseCase)) {
                result = CheckStatus.no("Extend target can only be reconnected to a non null UseCase.");
            } else if (extend.getExtension() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Extend cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }
        
        @Override
        public CheckStatus caseExtension(Extension extension) {
            final CheckStatus result;
            ExtensionHelper extensionHelper = new ExtensionHelper();
            if (extensionHelper.canCreate(extension.getStereotype(), newSemanticEdgeTarget)) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Extension can only be connected between a Stereotype and a Metaclass.");
            }
            return result;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Classifier && newSemanticEdgeTarget instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier target");
            } else if (generalization.getSpecific() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Generalization cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof UseCase && newSemanticEdgeTarget instanceof UseCase)) {
                result = CheckStatus.no(INVALID_USE_CASE_TARGET);
            } else if (include.getIncludingCase() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Include cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInterfaceRealization(InterfaceRealization interfaceRealization) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Interface && newSemanticEdgeTarget instanceof Interface)) {
                result = CheckStatus.no("The target of a InterfaceRealization should be an Interface");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInformationFlow(InformationFlow informationFlow) {
            final CheckStatus result;
            if (!(newSemanticEdgeTarget instanceof NamedElement)) {
                result = CheckStatus.no(INVALID_NAMED_ELEMENT_SOURCE);
            } else if (!(informationFlow.eContainer() instanceof Package)) {
                result = CheckStatus.no(INVALID_PACKAGE_CONTAINER);
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseManifestation(Manifestation manifestation) {
            final CheckStatus result;
            if (!(newSemanticEdgeTarget instanceof PackageableElement)) {
                result = CheckStatus.no(INVALID_PACKAGEABLE_ELEMENT_TARGET);
            } else if (newSemanticEdgeTarget == manifestation.getUtilizedElement()) {
                result = CheckStatus.no("Manifestation cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport object) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Package && newSemanticEdgeTarget instanceof Package)) {
                result = CheckStatus.no("Invalid PackageImport source or target");
            } else if (!(object.eContainer() instanceof Package)) {
                result = CheckStatus.no(INVALID_PACKAGE_CONTAINER);
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge object) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Package && newSemanticEdgeTarget instanceof Package)) {
                result = CheckStatus.no("Invalid PackageMerge source or target");
            } else if (!(object.eContainer() instanceof Package)) {
                result = CheckStatus.no(INVALID_PACKAGE_CONTAINER);
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseSubstitution(Substitution substitution) {
            final CheckStatus result;
            if (!(newSemanticEdgeTarget instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier target");
            } else if (substitution.getSubstitutingClassifier() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Substitution cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            final CheckStatus result;
            if (!editableChecker.canEdit(transition)) {
                result = CheckStatus.no("Can't edit the Transition.");
            } else if (!(oldSemanticEdgeTarget instanceof Vertex && newSemanticEdgeTarget instanceof Vertex)) {
                result = CheckStatus.no("Invalid semantic source");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseUsage(Usage usage) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof NamedElement && newSemanticEdgeTarget instanceof NamedElement)) {
                result = CheckStatus.no(INVALID_NAMED_ELEMENT_SOURCE);
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.YES;
        }
    }
}

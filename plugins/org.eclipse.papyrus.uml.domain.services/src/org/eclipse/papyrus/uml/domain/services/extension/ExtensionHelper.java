/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.extension;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Stereotype;

/**
 * Helper providing common method related to the {@link Extension} concept.
 * 
 * @author fbarbin
 */
public class ExtensionHelper {

    /**
     * From org.eclipse.papyrus.uml.service.types.helper.ExtensionEditHelper. Checks
     * if the extension can be created between the given souce and target.
     * 
     * @param source
     *               the extension source (the Stereotype).
     * @param target
     *               the extension target (the MetaClass).
     * @return true if it can be created, false otherwise.
     */
    public boolean canCreate(Object source, Object target) {
        boolean canCreate = true;
        if (!(source instanceof Stereotype) || !(target instanceof Class)) {
            canCreate = false;
        } else if (target != null) {
            String metaclassQName = ((Class) target).getQualifiedName();
            if ("uml::Stereotype".equals(metaclassQName)) { //$NON-NLS-1$
                canCreate = false;
            }
        }
        return canCreate;
    }

}

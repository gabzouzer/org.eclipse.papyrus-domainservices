/*****************************************************************************
 * Copyright (c) 2009, 2023 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yann TANGUY (CEA LIST) yann.tanguy@cea.fr - Initial API and implementation
 *  Christian W. Damus (CEA) - bug 440263
 *  Vincent LORENZO (CEA LIST) vincent.lorenzo@cea.fr - bug 530155
 *  Obeo - Refactoring for Papyrus Web
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;

import org.eclipse.uml2.uml.NamedElement;

/**
 * Utility class to display name of
 * <code>org.eclipse.uml2.uml.NamedElement</code>.
 */
// CHECKSTYLE:OFF Papyrus org.eclipse.papyrus.uml.tools.utils.NamedElementUtil
public class DefaultNamedElementNameProvider implements INamedElementNameProvider {
    // CHECKSTYLE:ON

    /**
     * This allows to get the name of the named element.
     *
     * @param namedElement
     *                     The named element.
     * @return The name of the named element or the name if the label is null.
     */
    public String getName(final NamedElement namedElement) {
        if (namedElement == null) {
            return null;
        }
        return getNonNullString(namedElement.getName());

    }

}

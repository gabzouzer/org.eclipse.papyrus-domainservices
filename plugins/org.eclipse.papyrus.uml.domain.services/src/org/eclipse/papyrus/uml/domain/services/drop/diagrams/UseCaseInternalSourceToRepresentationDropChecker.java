/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;

public class UseCaseInternalSourceToRepresentationDropChecker implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        final CheckStatus result;
        switch (elementToDrop.eClass().getClassifierID()) {
        case UMLPackage.ACTOR:
            result = handleActor(newSemanticContainer);
            break;
        case UMLPackage.PACKAGE:
            result = handlePackage(newSemanticContainer);
            break;
        case UMLPackage.COMMENT:
            result = handleComment(newSemanticContainer);
            break;
        case UMLPackage.CONSTRAINT:
            result = handleConstraint(newSemanticContainer);
            break;
        case UMLPackage.USE_CASE:
            result = handleUseCase(newSemanticContainer);
            break;
        case UMLPackage.ACTIVITY:
            result = handleSubject(newSemanticContainer);
            break;
        case UMLPackage.CLASS:
            result = handleSubject(newSemanticContainer);
            break;
        case UMLPackage.COMPONENT:
            result = handleSubject(newSemanticContainer);
            break;
        case UMLPackage.INTERACTION:
            result = handleSubject(newSemanticContainer);
            break;
        case UMLPackage.STATE_MACHINE:
            result = handleSubject(newSemanticContainer);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handleActor(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Actor can only be drag and drop on Package Elements.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handlePackage(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Package can only be drag and drop on Package Elements.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleComment(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)
                && !(newSemanticContainer instanceof org.eclipse.uml2.uml.Class)) {
            result = CheckStatus.no("Comment can only be drag and drop on Package or subject Elements.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleConstraint(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)
                && !(newSemanticContainer instanceof org.eclipse.uml2.uml.Class)) {
            result = CheckStatus.no("Constraint can only be drag and drop on Package or subject Elements.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleUseCase(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)
                && !(newSemanticContainer instanceof org.eclipse.uml2.uml.Class)) {
            result = CheckStatus.no("Use Case can only be drag and drop on Package or subject Elements.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleSubject(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Subject can only be drag and drop on Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }
}

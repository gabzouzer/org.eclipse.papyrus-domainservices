/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.UMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Most generic {@link ICreator}.
 *
 * @author Arthur Daussy
 */
public class ElementCreator implements ICreator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElementCreator.class);

    private final IElementConfigurer elementInitializer;

    private IFeatureModifier featureModifier;

    public ElementCreator(IElementConfigurer elementInitializer, IFeatureModifier featureModifier) {
        super();
        this.elementInitializer = elementInitializer;
        this.featureModifier = featureModifier;
    }

    public static ElementCreator buildDefault(ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        return new ElementCreator(new ElementConfigurer(), new ElementFeatureModifier(crossRef, editableChecker));
    }

    /**
     * Creates a semantic element inside a container given its containment
     * reference.
     *
     * @param parent
     *                      the container
     * @param type
     *                      the type of the element to create
     * @param referenceName
     *                      the name of the reference
     * @return a new {@link EObject} or <code>null</code> if the type of the
     *         containment reference is invalid
     */
    private EObject genericCreate(EObject parent, String type, String referenceName) {

        EClass eClass = UMLHelper.toEClass(type);

        if (eClass != null) {
            EObject newInstance = UMLFactory.eINSTANCE.create(eClass);

            Status status = featureModifier.addValue(parent, referenceName, newInstance);
            if (status.getState() == State.DONE) {
                if (this.elementInitializer != null) {
                    this.elementInitializer.configure(newInstance, parent);
                }
                return newInstance;
            } else if (status.getState() == State.FAILED) {
                LOGGER.error(status.getMessage());
            }

        } else {
            LOGGER.error(MessageFormat.format("Invalid EClass name {0}", type)); //$NON-NLS-1$
        }

        return null;
    }

    @Override
    public CreationStatus create(EObject parent, String type, String referenceName) {
        if (parent == null || type == null || referenceName == null) {
            return null;
        }

        final CreationStatus result;
        EObject semanticElement = this.genericCreate(parent, type, referenceName);
        if (semanticElement != null) {
            result = CreationStatus.createOKStatus(semanticElement);
        } else {
            result = CreationStatus.createFailingStatus(
                    MessageFormat.format("Unable to create a {0} on reference {1}", type, referenceName)); //$NON-NLS-1$
        }
        return result;
    }

}

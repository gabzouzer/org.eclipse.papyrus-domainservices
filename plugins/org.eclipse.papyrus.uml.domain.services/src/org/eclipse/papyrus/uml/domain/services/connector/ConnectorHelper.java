/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.connector;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;

/**
 * Various common utility methods related to the {@link Connector} concept.
 * 
 * @author fbarbin
 */
public class ConnectorHelper {


    /**
     * Checks whether we can create a connector from the given source view to the
     * target view.
     * 
     * @param representationQuery
     *                            the {@link IViewQuerier}.
     * @param sourceView
     *                            the source view.
     * @param targetView
     *                            the target view.
     * @return a {@link CheckStatus}
     */
    public CheckStatus canCreateConnector(IViewQuerier representationQuery, Object sourceView, Object targetView) {
        final CheckStatus result;
        if (sourceView == targetView) {
            result = CheckStatus.no("Cannot connect a port to itself.");
        } else {
            if (representationQuery.getBorderedNodes(sourceView).contains(targetView)
                    || (representationQuery.getBorderedNodes(targetView).contains(sourceView))) {
                result = CheckStatus.no(
                        "Cannot create a connector from a view representing a Part to its own Port (or the opposite).");
            } else if (getStructureContainers(sourceView, representationQuery).contains(targetView)
                    || getStructureContainers(targetView, representationQuery).contains(sourceView)) {
                result = CheckStatus.no(
                        "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.");
            } else {
                result = CheckStatus.YES;
            }
        }
        return result;
    }

    /**
     * Collect container hierarchy for a view (including itself and keeping
     * containment order).
     * 
     * @param view
     *             the graphical view
     * @return the list of containing Views.
     */
    private List<Object> getStructureContainers(Object view, IViewQuerier representationQuery) {
        return representationQuery.getVisualAncestorNodes(view).stream()//
                .filter(v -> getStructuredClassifier(v, representationQuery) != null)//
                .collect(toList());

    }

    /**
     * Get the {@link StructuredClassifier} related to a View. If the view relates
     * to a Property, returns its type in case it is a StructuredClassifier.
     * 
     * @param view
     *             the graphical view
     * @return the related {@link StructuredClassifier}
     */
    private StructuredClassifier getStructuredClassifier(Object view, IViewQuerier querier) {
        StructuredClassifier structuredClassifier = null;
        EObject semanticElement = querier.getSemanticElement(view);
        if (semanticElement instanceof StructuredClassifier) {
            structuredClassifier = (StructuredClassifier) semanticElement;

        } else if (semanticElement instanceof Property) {
            Property property = (Property) semanticElement;
            if (property.getType() instanceof StructuredClassifier) {
                structuredClassifier = (StructuredClassifier) property.getType();
            }

        }
        return structuredClassifier;
    }
}

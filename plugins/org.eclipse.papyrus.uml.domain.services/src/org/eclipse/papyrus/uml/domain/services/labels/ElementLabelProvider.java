/*******************************************************************************
 * Copyright (c) 2022,2023 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.domains.CollaborationUseLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.INamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.OperationLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.ParameterLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.PropertyLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.TransitionPropertiesParser;
import org.eclipse.papyrus.uml.domain.services.labels.domains.ValueSpecificationLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.VisibilityLabelHelper;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Duration;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.TimeExpression;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Object in charge of providing a label for a semantic element.
 *
 * @author Arthur Daussy
 */
public final class ElementLabelProvider implements IViewLabelProvider {

    private ElementLabelProviderSwitch nameRenderer;

    private final Function<EObject, String> prefixLabelProvider;

    private final Function<EObject, String> keywordLabelProvider;

    private final String prefixSeparator;

    private final String keywordSeparator;

    private ElementLabelProvider(Builder builder) {
        if (builder.prefixLabelProvider != null) {
            this.prefixLabelProvider = builder.prefixLabelProvider;
        } else {
            this.prefixLabelProvider = e -> null;
        }
        if (builder.keywordLabelProvider != null) {
            this.keywordLabelProvider = builder.keywordLabelProvider;
        } else {
            this.keywordLabelProvider = e -> null;
        }
        INamedElementNameProvider nameProvider;
        if (builder.nameProvider == null) {
            nameProvider = new DefaultNamedElementNameProvider();
        } else {
            nameProvider = builder.nameProvider;
        }
        nameRenderer = new ElementLabelProviderSwitch(nameProvider);
        this.prefixSeparator = builder.prefixSeparator;
        this.keywordSeparator = builder.keywordSeparator;
    }

    public static ElementLabelProvider buildDefault() {
        return ElementLabelProvider.builder()//
                .withKeywordLabelProvider(new KeywordLabelProvider())//
                .withNameProvider(new DefaultNamedElementNameProvider())//
                .withPrefixLabelProvider(new StereotypeLabelPrefixProvider())//
                .build();
    }

    /**
     * Gets the label for the given element.
     *
     * @param element
     *                an element
     * @return a label (never returns <code>null</code> instead return an empty
     *         string)
     */
    @Override
    public String getLabel(EObject element) {
        if (element == null) {
            return ""; //$NON-NLS-1$
        }

        StringBuilder label = new StringBuilder();

        // add keywords
        String keyword = keywordLabelProvider.apply(element);
        if (keyword != null && !keyword.isBlank()) {
            label.append(keyword);
        }

        // add prefix
        String prefix = prefixLabelProvider.apply(element);
        if (prefix != null && !prefix.isBlank()) {
            if (label.length() > 0) {
                label.append(keywordSeparator);
            }
            label.append(prefix);
        }

        // add element name
        String baseLabel = this.nameRenderer.doSwitch(element);
        if (baseLabel != null && !baseLabel.isBlank()) {
            if (label.length() > 0) {
                label.append(prefixSeparator);
            }
            label.append(baseLabel);
        }

        return label.toString();
    }

    static final class ElementLabelProviderSwitch extends UMLSwitch<String> {

        private static final String NATURAL = "NATURAL";

        private static final String NULL_CONSTRAINT = "<NULL Constraint>";

        private CollaborationUseLabelHelper collaborationUseLabelHelper;

        private PropertyLabelHelper propertyLabelHelper;

        private ParameterLabelHelper parameterLabelHelper;

        private INamedElementNameProvider namedElementNameProvider;

        private VisibilityLabelHelper visibilityLabelHelper;

        private ValueSpecificationLabelHelper valueSpecificationHelper;

        private final OperationLabelHelper operationLabelHelper;

        ElementLabelProviderSwitch() {
            this(null);
        }

        ElementLabelProviderSwitch(INamedElementNameProvider namedElementNameProvider) {
            super();
            this.namedElementNameProvider = namedElementNameProvider;
            this.visibilityLabelHelper = new VisibilityLabelHelper();
            collaborationUseLabelHelper = new CollaborationUseLabelHelper(namedElementNameProvider,
                    visibilityLabelHelper);
            propertyLabelHelper = new PropertyLabelHelper(false, true, namedElementNameProvider, visibilityLabelHelper);
            parameterLabelHelper = new ParameterLabelHelper(false, true, namedElementNameProvider);
            this.valueSpecificationHelper = new ValueSpecificationLabelHelper(namedElementNameProvider);
            operationLabelHelper = new OperationLabelHelper(this.parameterLabelHelper, this.visibilityLabelHelper,
                    this.namedElementNameProvider);
        }

        @Override
        public String caseCollaborationUse(CollaborationUse collaborationUse) {
            return this.collaborationUseLabelHelper.getLabel(collaborationUse);
        }

        @Override
        public String caseComment(Comment comment) {
            return comment.getBody();
        }

        @Override
        public String caseInformationFlow(InformationFlow flow) {
            String result = "";
            if (flow != null) {
                result = getConveyeds(flow.getConveyeds());
                String flowName = this.namedElementNameProvider.getName(flow);
                if (flowName != null && !flowName.isBlank()) {
                    if (!result.isBlank()) {
                        result += UMLCharacters.EOL;
                    }
                    result += flowName;
                }
            }
            return result;
        }

        private String getConveyeds(EList<Classifier> conveyeds) {
            String result = "";
            if (!conveyeds.isEmpty()) {
                result += conveyeds.stream().map(NamedElement::getName).collect(Collectors.joining(", "));
            }
            return result;
        }

        @Override
        public String caseNamedElement(NamedElement object) {
            return this.namedElementNameProvider.getName(object);
        }

        @Override
        public String caseRegion(Region object) {
            return "";
        }

        @Override
        public String caseProperty(Property property) {
            return this.propertyLabelHelper.getLabel(property);
        }

        @Override
        public String caseParameter(Parameter parameter) {
            return this.parameterLabelHelper.getLabel(parameter);
        }

        /**
         * Copied from
         * org.eclipse.papyrus.uml.diagram.common.parser.ConstraintParser.getEditString(IAdaptable,
         * int)
         */
        @Override
        public String caseConstraint(Constraint constraint) {
            StringBuilder constLabel = new StringBuilder();
            String body = UMLCharacters.EMPTY;
            String lang = UMLCharacters.EMPTY;
            ValueSpecification valueSpec = constraint.getSpecification();
            constLabel.append(constraint.getName());
            constLabel.append(UMLCharacters.EOL);
            constLabel.append(UMLCharacters.OPEN_BRACKET);
            if (valueSpec == null) {
                constLabel.append(NULL_CONSTRAINT);
            } else if (valueSpec instanceof OpaqueExpression) {
                OpaqueExpression opaqueEsp = (OpaqueExpression) valueSpec;
                if (!opaqueEsp.getBodies().isEmpty() && !opaqueEsp.getLanguages().isEmpty()) {
                    body = opaqueEsp.getBodies().get(0);
                    lang = opaqueEsp.getLanguages().get(0);
                    constLabel.append(UMLCharacters.OPEN_BRACKET);
                    constLabel.append(lang);
                    constLabel.append(UMLCharacters.CLOSE_BRACKET);
                    constLabel.append(UMLCharacters.SPACE);
                    constLabel.append(body);
                } else {
                    constLabel.append(UMLCharacters.OPEN_BRACKET);
                    constLabel.append(NATURAL);
                    constLabel.append(UMLCharacters.CLOSE_BRACKET);
                    constLabel.append(UMLCharacters.SPACE);
                }
            }
            constLabel.append(UMLCharacters.CLOSE_BRACKET);
            return constLabel.toString();
        }

        @Override
        public String caseOperation(Operation operation) {
            return this.operationLabelHelper.getLabel(operation);
        }

        @Override
        public String caseTimeExpression(TimeExpression timeExpression) {
            StringBuilder constLabel = new StringBuilder();
            constLabel.append(this.namedElementNameProvider.getName(timeExpression));
            ValueSpecification expr = timeExpression.getExpr();
            if (expr != null) {
                constLabel.append(UMLCharacters.EQL);
                constLabel.append(this.valueSpecificationHelper.getSpecificationValue(expr, true));
            }
            return constLabel.toString();
        }

        @Override
        public String caseDuration(Duration duration) {
            StringBuilder constLabel = new StringBuilder();
            constLabel.append(this.namedElementNameProvider.getName(duration));
            ValueSpecification expr = duration.getExpr();
            if (expr != null) {
                constLabel.append(UMLCharacters.EQL);
                constLabel.append(this.valueSpecificationHelper.getSpecificationValue(expr, true));
            }
            return constLabel.toString();
        }

        @Override
        public String caseActivityPartition(ActivityPartition activityPartition) {
            String label;
            Element representedElement = activityPartition.getRepresents();
            if (representedElement instanceof NamedElement) {
                label = this.namedElementNameProvider.getName((NamedElement) representedElement);
            } else {
                label = super.caseActivityPartition(activityPartition);
            }
            return label;
        }

        public String caseTransition(Transition transition) {
            return new TransitionPropertiesParser(this.namedElementNameProvider).getValueString(transition);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private INamedElementNameProvider nameProvider;
        private Function<EObject, String> prefixLabelProvider;
        private Function<EObject, String> keywordLabelProvider;
        private String prefixSeparator = EOL;
        private String keywordSeparator = EOL;

        private Builder() {
        }

        /**
         * Provider of the base name of an element.
         * 
         * @param aNameProvider
         * @return this for convenience.
         */
        public Builder withNameProvider(INamedElementNameProvider aNameProvider) {
            this.nameProvider = aNameProvider;
            return this;
        }

        /**
         * Provider of a prefix for the element label (optional).
         * 
         * @param aPrefixLabelProvider
         * @return this for convenience.
         */
        public Builder withPrefixLabelProvider(Function<EObject, String> aPrefixLabelProvider) {
            this.prefixLabelProvider = aPrefixLabelProvider;
            return this;
        }

        /**
         * Provider of a keywords for the element label (optional).
         * 
         * @param aKeywordLabelProvider
         * @return this for convenience.
         */
        public Builder withKeywordLabelProvider(Function<EObject, String> aKeywordLabelProvider) {
            this.keywordLabelProvider = aKeywordLabelProvider;
            return this;
        }

        /**
         * The string separator used between the prefix and the subsequent part of the
         * label.
         * 
         * @param aPrefixSeparator
         * @return this for convenience.
         */
        public Builder withPrefixSeparator(String aPrefixSeparator) {
            this.prefixSeparator = aPrefixSeparator;
            return this;
        }

        /**
         * The string separator used between the keyword and the subsequent part of the
         * label.
         * 
         * @param aKeywordSeparator
         * @return this for convenience.
         */
        public Builder withKeywordSeparator(String aKeywordSeparator) {
            this.keywordSeparator = aKeywordSeparator;
            return this;
        }

        public ElementLabelProvider build() {
            return new ElementLabelProvider(this);
        }
    }
}

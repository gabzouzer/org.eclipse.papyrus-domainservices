/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.services;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * This helper provides interesting features for TimeConstraint objects. Copy
 * from
 * {@link org.eclipse.papyrus.uml.diagram.common.helper.TimeConstraintHelper}
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class TimeConstraintHelper {

    /**
     * Get the list of all TimeConstraint constraining a given element.
     *
     * @param element
     *                              the constrained element
     * @param crossReferenceAdapter
     *                              an adapter used to get inverse references
     * 
     * @return list of TimeConstraint
     */
    public static List<TimeConstraint> getTimeConstraintsOn(NamedElement element,
            ECrossReferenceAdapter crossReferenceAdapter) {
        Collection<Setting> inverseReferences = crossReferenceAdapter.getInverseReferences(element, false);
        // TimeConstraint referencing element
        List<TimeConstraint> referencing = new LinkedList<>();
        for (Setting ref : inverseReferences) {
            if (UMLPackage.eINSTANCE.getConstraint_ConstrainedElement().equals(ref.getEStructuralFeature())
                    && ref.getEObject() instanceof TimeConstraint && ref.getEObject().eContainer() != null) {
                referencing.add((TimeConstraint) ref.getEObject());
            }
        }
        return referencing;
    }
}

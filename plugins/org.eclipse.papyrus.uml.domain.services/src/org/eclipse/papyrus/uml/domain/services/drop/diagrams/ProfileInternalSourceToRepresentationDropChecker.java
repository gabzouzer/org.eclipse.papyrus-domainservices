/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * SourceToRepresentationDropChecker for Profile diagram.
 * 
 * @author fbarbin
 */
public class ProfileInternalSourceToRepresentationDropChecker implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        final CheckStatus result;
        switch (elementToDrop.eClass().getClassifierID()) {
        case UMLPackage.CLASS:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.COMMENT:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.CONSTRAINT:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.DATA_TYPE:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.ENUMERATION:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.ENUMERATION_LITERAL:
            result = handleEnumerationLiteral(newSemanticContainer);
            break;
        case UMLPackage.ELEMENT_IMPORT:
            result = handleElementImport(newSemanticContainer);
            break;
        case UMLPackage.PACKAGE:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.PRIMITIVE_TYPE:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.PROFILE:
            result = handlePackageContainer(newSemanticContainer);
            break;
        case UMLPackage.PROPERTY:
            result = handleOperationProperty(newSemanticContainer);
            break;
        case UMLPackage.OPERATION:
            result = handleOperationProperty(newSemanticContainer);
            break;
        case UMLPackage.STEREOTYPE:
            result = handlePackageContainer(newSemanticContainer);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handlePackageContainer(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Package)) {
            result = CheckStatus.no(String.format("{0} can only be drag and drop on a Package kind element.",
                    newSemanticContainer.eClass().getName()));
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleEnumerationLiteral(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Enumeration)) {
            result = CheckStatus.no(String.format("EnumerationLiteral can only be drag and drop on Enumeration.",
                    newSemanticContainer.eClass().getName()));
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleOperationProperty(EObject newSemanticContainer) {
        final CheckStatus result;

        // The Property and Operation Drop is authorized over Class, DataType but not
        // over Enumeration.
        if ((!(newSemanticContainer instanceof org.eclipse.uml2.uml.Class)
                && !(newSemanticContainer instanceof DataType)) || newSemanticContainer instanceof Enumeration) {
            result = CheckStatus.no("Operation or Property can only be drag and drop on a Class or a DataType.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleElementImport(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Profile)) {
            result = CheckStatus.no("Element Import can only be drag and drop on Profile.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }
}

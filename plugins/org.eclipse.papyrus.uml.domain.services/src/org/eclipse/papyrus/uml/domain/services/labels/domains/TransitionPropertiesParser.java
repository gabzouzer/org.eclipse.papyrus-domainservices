/**
 * Copyright (c) 2014, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *  Jeremie TATIBOUET (CEA LIST) - Fix https://bugs.eclipse.org/bugs/show_bug.cgi?id=477573
 *  Nicolas FAUVERGUE (ALL4TEC) nicolas.fauvergue@all4tec.net - Bug 496905
 */
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.ChangeEvent;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.SignalEvent;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.TimeExpression;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.ValueSpecification;

/**
 * This class is copied and adapted from
 * {@link org.eclipse.papyrus.uml.diagram.statemachine.custom.parsers.TransitionPropertiesParser}.
 */
//CHECKSTYLE:OFF
public class TransitionPropertiesParser {
    public static final String ONE_SPACE_STRING = " "; //$NON-NLS-1$
    public static final String EMPTY_STRING = ""; //$NON-NLS-1$
    protected Constraint guardConstraint = null;
    private final INamedElementNameProvider namedElementNameProvider;

    public TransitionPropertiesParser(INamedElementNameProvider namedElementNameProvider) {
        this.namedElementNameProvider = namedElementNameProvider;
    }

    /**
     * Get the unformatted registered string value which shall be displayed.
     */
    public String getValueString(Transition transition) {
        StringBuilder result = new StringBuilder();
        String textForTrigger = getTextForTrigger(transition);
        if (textForTrigger != null && !EMPTY_STRING.equals(textForTrigger)) {
            result.append(textForTrigger);
        }
        result.append(getTextForGuard(transition));
        String textForEffect = getTextForEffect(transition);
        if (textForEffect != null && !EMPTY_STRING.equals(textForEffect)) {
            result.append("/"); //$NON-NLS-1$
            if (PreferenceConstants.LINEBREAK_BEFORE_EFFECT) {
                result.append("\n"); //$NON-NLS-1$
            }
            result.append(textForEffect);
        }
        return result.toString();
    }

    /**
     * get the text concerning guard.
     *
     * @param trans
     * @return
     */
    protected String getTextForGuard(Transition trans) {
        Constraint constraint = trans.getGuard();
        if (constraint != null) {
            String value;
            if (constraint.getSpecification() != null) {
                value = new ValueSpecificationLabelHelper(namedElementNameProvider).getConstraintValue(constraint);
            } else {
                String name = namedElementNameProvider.getName(constraint);
                if (name == null) {
                    name = "<undef>"; //$NON-NLS-1$
                }
                value = String.format("%s (no spec)", name); //$NON-NLS-1$
            }
            if (value != null) {
                return String.format("[%s]", value); //$NON-NLS-1$
            }
        }
        return EMPTY_STRING;
    }

    /**
     * get the text concerning Effects.
     *
     * @param trans
     * @return
     */
    protected String getTextForEffect(Transition trans) {
        StringBuilder result = new StringBuilder();
        Behavior effect = trans.getEffect();
        if (effect != null) {
            EClass eClass = effect.eClass();
            if (effect instanceof OpaqueBehavior) {
                OpaqueBehavior ob = (OpaqueBehavior) effect;
                if (ob.getBodies().size() > 0) {
                    // return body of behavior (only handle case of a single body)
                    result.append(OpaqueBehaviorLabelHelper.retrieveBody(ob));
                    return result.toString();
                }
            }
            if (eClass != null) {
                result.append(eClass.getName()).append(": ") //$NON-NLS-1$
                        .append(namedElementNameProvider.getName(effect));
            }
        }
        return result.toString();
    }

    /**
     * Get the text concerning Trigger.
     *
     * @param trans
     * @return
     */
    protected String getTextForTrigger(Transition trans) {
        StringBuilder result = new StringBuilder();
        boolean isFirstTrigger = true;
        for (Trigger t : trans.getTriggers()) {
            if (t != null) {
                if (!isFirstTrigger) {
                    result.append(", "); //$NON-NLS-1$
                } else {
                    isFirstTrigger = false;
                }
                Event e = t.getEvent();
                if (e instanceof CallEvent) {
                    Operation op = ((CallEvent) e).getOperation();
                    if (op != null) {
                        result.append(namedElementNameProvider.getName(op));
                        if ((op.getOwnedParameters().size() > 0) && PreferenceConstants.INDICATE_PARAMETERS) {
                            result.append(OpaqueBehaviorLabelHelper.PARAM_DOTS);
                        }
                    } else {
                        result.append(namedElementNameProvider.getName(e));
                    }
                } else if (e instanceof SignalEvent) {
                    Signal signal = ((SignalEvent) e).getSignal();
                    if (signal != null) {
                        result.append(namedElementNameProvider.getName(signal));
                        if ((signal.getAttributes().size() > 0) && PreferenceConstants.INDICATE_PARAMETERS) {
                            result.append(OpaqueBehaviorLabelHelper.PARAM_DOTS);
                        }
                    } else {
                        result.append(namedElementNameProvider.getName(e));
                    }
                } else if (e instanceof ChangeEvent) {
                    ValueSpecification vs = ((ChangeEvent) e).getChangeExpression();
                    String value;
                    if (vs instanceof OpaqueExpression) {
                        value = OpaqueBehaviorLabelHelper.retrieveBody((OpaqueExpression) vs);
                    } else {
                        value = vs.stringValue();
                    }
                    result.append(value);
                } else if (e instanceof TimeEvent) {
                    TimeEvent timeEvent = (TimeEvent) e;
                    // absRelPrefix
                    result.append(timeEvent.isRelative() ? "after " : "at "); //$NON-NLS-1$ //$NON-NLS-2$
                    // body
                    TimeExpression te = timeEvent.getWhen();
                    String value;
                    if (te != null) {
                        ValueSpecification vs = te.getExpr();
                        if (vs instanceof OpaqueExpression) {
                            value = OpaqueBehaviorLabelHelper.retrieveBody((OpaqueExpression) vs);
                        } else {
                            value = vs.stringValue();
                        }
                    } else {
                        value = "undefined"; //$NON-NLS-1$
                    }
                    result.append(value);
                } else { // any receive event
                    result.append("all"); //$NON-NLS-1$
                }
            }
        }
        return result.toString();
    }
}

/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import java.text.MessageFormat;
import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.connector.ConnectorHelper;
import org.eclipse.papyrus.uml.domain.services.extension.ExtensionHelper;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class ElementDomainBasedEdgeReconnectionSourceChecker implements IDomainBasedEdgeReconnectionSourceChecker {

    private final IEditableChecker editableCheck;

    private final IViewQuerier representationQuery;

    public ElementDomainBasedEdgeReconnectionSourceChecker(IEditableChecker editableCheck,
            IViewQuerier representationQuery) {
        super();
        this.editableCheck = Objects.requireNonNull(editableCheck);
        this.representationQuery = Objects.requireNonNull(representationQuery);
    }

    @Override
    public CheckStatus canReconnect(EObject edgeToReconnect, EObject oldSemanticEdgeSource,
            EObject newSemanticEdgeSource, Object newSourceView, Object targetView) {
        if (newSemanticEdgeSource == null) {
            return CheckStatus.no("The new semantic edge source must not be null");
        }
        return new ElementDomainBasedEdgeReconnectionSourceCheckerSwitch(oldSemanticEdgeSource, newSemanticEdgeSource,
                newSourceView, targetView, editableCheck, this.representationQuery).doSwitch(edgeToReconnect);
    }

    public static class ElementDomainBasedEdgeReconnectionSourceCheckerSwitch extends UMLSwitch<CheckStatus> {

        private static final String INVALID_USE_CASE_SOURCE = "Invalid Use Case source";

        private static final String INVALID_CONTAINER = "Invalid {0} container for the type {1}";

        private static final String CANNOT_EDIT_NEW_SOURCE = "Can't edit new source.";

        private static final String INVALID_NAMED_ELEMENT_SOURCE = "Invalid Named Element source";

        private final EObject oldSemanticEdgeSource;

        private final EObject newSemanticEdgeSource;

        private final Object newSourceView;

        private final Object targetView;

        private final IEditableChecker editableChecker;

        private final IViewQuerier representationQuery;

        public ElementDomainBasedEdgeReconnectionSourceCheckerSwitch(EObject oldSemanticEdgeSource,
                EObject newSemanticEdgeSource, Object newSourceView, Object targetView,
                IEditableChecker editableChecker, IViewQuerier representationQuery) {
            super();
            this.oldSemanticEdgeSource = oldSemanticEdgeSource;
            this.newSemanticEdgeSource = newSemanticEdgeSource;
            this.newSourceView = newSourceView;
            this.targetView = targetView;
            this.editableChecker = editableChecker;
            this.representationQuery = representationQuery;
        }

        @Override
        public CheckStatus caseAssociation(Association association) {
            final CheckStatus result;
            Property sourceProperty = association.getMemberEnds().get(0);
            if (!(oldSemanticEdgeSource instanceof Classifier && newSemanticEdgeSource instanceof Classifier)) {
                result = CheckStatus.no("Invalid Association source: the new source should be a Classifier");
            } else if (!association.getOwnedEnds().contains(sourceProperty)) {
                // Check that the new source can contain the sourceProperty if this one was not
                // owned by the association
                // CHECKSTYLE:OFF
                boolean cannotContainProperty = !(newSemanticEdgeSource instanceof Artifact
                        || newSemanticEdgeSource instanceof DataType || newSemanticEdgeSource instanceof Interface
                        || newSemanticEdgeSource instanceof Signal
                        || newSemanticEdgeSource instanceof StructuredClassifier);
                // CHECKSTYLE:ON
                if (cannotContainProperty) {
                    result = CheckStatus.no(
                            "Invalid Association source: the new source must be able to contain the SourceProperty attribute");
                } else {
                    result = CheckStatus.YES;
                }
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseConnector(Connector connector) {
            ConnectorHelper connectorHelper = new ConnectorHelper();
            return connectorHelper.canCreateConnector(this.representationQuery, this.newSourceView, this.targetView);
        }

        @Override
        public CheckStatus caseDependency(Dependency object) {
            final CheckStatus result;
            if (newSemanticEdgeSource instanceof NamedElement && editableChecker
                    .canEdit(newSemanticEdgeSource) /* The will be the new container of the dependency */) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus
                        .no("Dependency source can only be reconnected to a non null editable NamedElement.");
            }
            return result;
        }

        @Override
        public CheckStatus caseExtend(Extend extend) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof UseCase && newSemanticEdgeSource instanceof UseCase)) {
                result = CheckStatus.no("Invalid Extend source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (!(extend.eContainer() instanceof UseCase)) {
                result = CheckStatus.no(MessageFormat.format(INVALID_CONTAINER, UseCase.class.getSimpleName(),
                        Extend.class.getSimpleName()));
            } else if (extend.getExtendedCase() == newSemanticEdgeSource) {
                result = CheckStatus.no("Extend cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseExtension(Extension extension) {
            final CheckStatus result;
            ExtensionHelper extensionHelper = new ExtensionHelper();
            if (extensionHelper.canCreate(newSemanticEdgeSource, extension.getMetaclass())) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Extension can only be connected between a Stereotype and a Metaclass.");
            }
            return result;
        }
        
        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Classifier && newSemanticEdgeSource instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                // the new semantic source contains the generalization
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (generalization.getGeneral() == newSemanticEdgeSource) {
                result = CheckStatus.no("Generalization cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof UseCase && newSemanticEdgeSource instanceof UseCase)) {
                result = CheckStatus.no(INVALID_USE_CASE_SOURCE);
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (include.getAddition() == newSemanticEdgeSource) {
                result = CheckStatus.no("Include cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInformationFlow(InformationFlow informationFlow) {
            final CheckStatus result;
            if (!(newSemanticEdgeSource instanceof NamedElement)) {
                result = CheckStatus.no(INVALID_NAMED_ELEMENT_SOURCE);
            } else if (!(informationFlow.eContainer() instanceof Package)) {
                result = CheckStatus.no(MessageFormat.format(INVALID_CONTAINER, Package.class.getSimpleName(),
                        InformationFlow.class.getSimpleName()));
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseManifestation(Manifestation manifestation) {
            final CheckStatus result;
            if (!(newSemanticEdgeSource instanceof NamedElement)) {
                result = CheckStatus.no(INVALID_NAMED_ELEMENT_SOURCE);
            } else if (manifestation.getClients().contains(newSemanticEdgeSource)) {
                result = CheckStatus.no("Manifestation cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInterfaceRealization(InterfaceRealization object) {
            final CheckStatus result;
            if (!(newSemanticEdgeSource instanceof BehavioredClassifier)) {
                result = CheckStatus.no("The source of an InterfaceRealization should be a BehavioredClassifier");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport packImport) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Namespace && newSemanticEdgeSource instanceof Namespace)) {
                result = CheckStatus.no("Invalid Namespace source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (!(packImport.eContainer() instanceof Package)) {
                result = CheckStatus.no(MessageFormat.format(INVALID_CONTAINER, Package.class.getSimpleName(),
                        PackageImport.class.getSimpleName()));
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge packMerge) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Package && newSemanticEdgeSource instanceof Package)) {
                result = CheckStatus.no("Invalid PackageMerge source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (!(packMerge.eContainer() instanceof Package)) {
                result = CheckStatus.no(MessageFormat.format(INVALID_CONTAINER, Package.class.getSimpleName(),
                        PackageMerge.class.getSimpleName()));
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseSubstitution(Substitution substitution) {
            final CheckStatus result;
            if (!(newSemanticEdgeSource instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (substitution.getContract() == newSemanticEdgeSource) {
                // the new semantic source contains the substitution
                result = CheckStatus.no("Substitution cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            final CheckStatus result;
            if (!editableChecker.canEdit(transition)) {
                result = CheckStatus.no("Can't edit the Transition.");
            } else if (!(oldSemanticEdgeSource instanceof Vertex && newSemanticEdgeSource instanceof Vertex)) {
                result = CheckStatus.no("Invalid semantic source");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseUsage(Usage usage) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof NamedElement && newSemanticEdgeSource instanceof NamedElement)) {
                result = CheckStatus.no(INVALID_NAMED_ELEMENT_SOURCE);
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.YES;
        }
    }

}

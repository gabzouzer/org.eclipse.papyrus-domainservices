/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.SPACE;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.INamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.MultiplicityLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.VisibilityLabelHelper;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provider of a label for domain based edge on the source (label located at the
 * beginning of an edge).
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeSourceLabelProvider implements IDomainBasedEdgeEndLabelProvider {

    private INamedElementNameProvider namedElementNameProvider;

    public ElementDomainBasedEdgeSourceLabelProvider(INamedElementNameProvider namedElementNameProvider) {
        this.namedElementNameProvider = namedElementNameProvider;
    }

    public static ElementDomainBasedEdgeSourceLabelProvider buildDefault() {
        return new ElementDomainBasedEdgeSourceLabelProvider(new DefaultNamedElementNameProvider());
    }

    @Override
    public String getLabel(EObject element, EObject semanticEnd) {
        String label = new ElementDomainBasedEdgeSourceLabelProviderSwitch(namedElementNameProvider).doSwitch(element);

        if (label == null) {
            label = "";
        }
        return label;
    }

    public static final class ElementDomainBasedEdgeSourceLabelProviderSwitch extends UMLSwitch<String> {

        private MultiplicityLabelHelper multiplicityLabelHelper;

        private VisibilityLabelHelper visibilityLabelHelper;

        public ElementDomainBasedEdgeSourceLabelProviderSwitch(INamedElementNameProvider namedElementNameProvider) {
            super();
            this.multiplicityLabelHelper = new MultiplicityLabelHelper(namedElementNameProvider);
            this.visibilityLabelHelper = new VisibilityLabelHelper();
        }

        @Override
        public String caseAssociation(Association association) {
            EList<Property> memberEnds = association.getMemberEnds();
            if (memberEnds.size() > 1) {
                StringBuilder associationSourceLabel = new StringBuilder();
                Property propert = memberEnds.get(1);
                // visibility
                associationSourceLabel.append(getNonNullString(visibilityLabelHelper.getVisibilityAsSign(propert)));
                // name
                associationSourceLabel.append(SPACE);
                associationSourceLabel.append(propert.getName());
                // multiplicity
                associationSourceLabel.append(SPACE);
                associationSourceLabel.append(multiplicityLabelHelper.formatMultiplicityNoBrackets(propert, false));
                return associationSourceLabel.toString();
            }
            return super.caseAssociation(association);
        }

        @Override
        public String caseConnector(Connector connector) {

            EList<ConnectorEnd> ends = connector.getEnds();
            if (ends.size() > 0) {
                return multiplicityLabelHelper.formatMultiplicityNoBrackets(ends.get(0), false);
            }
            return super.caseConnector(connector);
        }

    }
}

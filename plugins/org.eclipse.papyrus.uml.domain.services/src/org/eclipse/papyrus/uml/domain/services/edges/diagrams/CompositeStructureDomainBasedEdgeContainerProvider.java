/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges.diagrams;

import static java.util.stream.Collectors.toList;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.edges.IDomainBasedEdgeContainerProvider;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class CompositeStructureDomainBasedEdgeContainerProvider implements IDomainBasedEdgeContainerProvider {

    private final IDomainBasedEdgeContainerProvider delegate;

    public CompositeStructureDomainBasedEdgeContainerProvider(IDomainBasedEdgeContainerProvider delegate) {
        super();
        this.delegate = delegate;
    }

    public static CompositeStructureDomainBasedEdgeContainerProvider buildDefault(IEditableChecker editableChecker) {
        return new CompositeStructureDomainBasedEdgeContainerProvider(
                new ElementDomainBasedEdgeContainerProvider(editableChecker));
    }

    @Override
    public EObject getContainer(EObject semanticSource, EObject semanticTarget, EObject semanticEdge,
            IViewQuerier querier, Object sourceNode, Object targetNode) {
        EObject container = new CompositeStructureDomainBasedEdgeContainerProviderSwitch(querier, sourceNode,
                targetNode).doSwitch(semanticEdge);

        // If the semantic Edge is a connector, we don't want to call the delegate if
        // the container is null. If the container is null, it means that we did not
        // find a common ancestor and the edge should not be created.
        if (container == null && !(semanticEdge instanceof Connector)) {
            return delegate.getContainer(semanticSource, semanticTarget, semanticEdge, querier, sourceNode, targetNode);
        } else {
            return container;
        }
    }

    class CompositeStructureDomainBasedEdgeContainerProviderSwitch extends UMLSwitch<EObject> {

        private final Object sourceView;

        private final Object targetView;

        private final IViewQuerier representationQuery;

        CompositeStructureDomainBasedEdgeContainerProviderSwitch(IViewQuerier representationQuery, Object sourceView,
                Object targetView) {
            super();
            this.sourceView = sourceView;
            this.targetView = targetView;
            this.representationQuery = representationQuery;
        }

        @Override
        public EObject caseConnector(Connector object) {

            List<StructuredClassifier> sourceStructureClassifiers = representationQuery
                    .getVisualAncestorNodes(sourceView).stream()//
                    .map(representationQuery::getSemanticElement)//
                    .filter(e -> e instanceof StructuredClassifier)//
                    .map(e -> (StructuredClassifier) e)//
                    .collect(toList());
            List<StructuredClassifier> targetStructureClassifiers = representationQuery
                    .getVisualAncestorNodes(targetView).stream()//
                    .map(representationQuery::getSemanticElement)//
                    .filter(e -> e instanceof StructuredClassifier)//
                    .map(e -> (StructuredClassifier) e)//
                    .collect(toList());

            Iterator<StructuredClassifier> targetIte = targetStructureClassifiers.iterator();
            while (targetIte.hasNext()) {
                StructuredClassifier structuredClassifier = targetIte.next();
                if (sourceStructureClassifiers.contains(structuredClassifier)) {
                    return structuredClassifier;
                }
            }

            return super.caseConnector(object);
        }
    }
}

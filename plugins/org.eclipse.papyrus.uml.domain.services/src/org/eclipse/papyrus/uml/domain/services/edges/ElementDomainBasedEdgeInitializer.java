/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.services.ClassifierUtils;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.ExtensionPoint;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Element in charge of initializing a {@link EObject} in a given context.
 *
 * @author Arthur Daussy
 */
public class ElementDomainBasedEdgeInitializer implements IDomainBasedEdgeInitializer {

    /**
     * the extension's name begins by this string.
     */
    public static final String EXTENSION = "extension_"; //$NON-NLS-1$

    /**
     * the property's name in the stereotype begins by base_.
     */
    public static final String BASE = "base_"; //$NON-NLS-1$

    /** Constant for UML nature. */
    public static final String UML_NATURE = "UML_Nature";

    /**
     * The ID for Papyrus EAnnotations.
     */
    private static final String PAPYRUS_URI = "org.eclipse.papyrus"; //$NON-NLS-1$

    /**
     * The ID for element nature in Papyrus EAnnotations.
     */
    private static final String PAPYRUS_ELEMENT_NATURE = "nature"; //$NON-NLS-1$


    /**
     * Initialize a semantic element.
     *
     * @param toInit
     *               the element to init
     * @param parent
     *               the context parent
     * @return the given element for fluent API use
     */
    @Override
    public EObject initialize(EObject toInit, EObject source, EObject target, IViewQuerier representationQuery,
            Object sourceView, Object targetView) {
        if (toInit == null) {
            return null;
        }
        new ElementEdgeInitializerSwitch(source, target, representationQuery, sourceView, targetView).doSwitch(toInit);
        return toInit;
    }

    static class ElementEdgeInitializerSwitch extends UMLSwitch<EObject> {
        private final EObject source;

        private final EObject target;

        private final Object sourceView;

        private final Object targetView;

        private final IViewQuerier representationQuery;

        ElementEdgeInitializerSwitch(EObject source, EObject target, IViewQuerier representationQuery,
                Object sourceView, Object targetView) {
            super();
            this.source = source;
            this.target = target;
            this.sourceView = sourceView;
            this.targetView = targetView;
            this.representationQuery = representationQuery;
        }

        @Override
        public EObject defaultCase(EObject object) {
            return object;
        }

        @Override
        public EObject caseAssociation(Association association) {
            // add nature
            EMap<String, String> details = UML2Util.getEAnnotation(association, PAPYRUS_URI, true).getDetails();

            if (!details.containsKey(PAPYRUS_ELEMENT_NATURE)) {
                details.put(PAPYRUS_ELEMENT_NATURE, UML_NATURE);
            } else {
                details.removeKey(PAPYRUS_ELEMENT_NATURE);
                details.put(PAPYRUS_ELEMENT_NATURE, UML_NATURE);
            }

            // source and target are classifier (see {@link
            // ElementDomainBasedEdgeCreationChecker})
            Classifier sourceClassifier = (Classifier) source;
            Classifier targetClassifier = (Classifier) target;

            // create source property
            Property sourceProp = UMLFactory.eINSTANCE.createProperty();
            sourceProp.setType(targetClassifier);
            if (targetClassifier.getName() != null) {
                sourceProp.setName(targetClassifier.getName().toLowerCase());
            }
            association.getMemberEnds().add(sourceProp);

            // create target property
            Property targetProp = UMLFactory.eINSTANCE.createProperty();
            targetProp.setType(sourceClassifier);
            if (sourceClassifier.getName() != null) {
                targetProp.setName(sourceClassifier.getName().toLowerCase());
            }
            association.getMemberEnds().add(targetProp);

            // Add source {@link Property} in the correct container
            boolean added = ClassifierUtils.addOwnedAttribute(sourceClassifier, sourceProp);
            if (!added) {
                association.getOwnedEnds().add(sourceProp);
            }
            sourceProp.setIsNavigable(false);

            // Add target {@link Property} in the correct container
            added = ClassifierUtils.addOwnedAttribute(targetClassifier, targetProp);
            if (!added) {
                association.getOwnedEnds().add(targetProp);
            }
            targetProp.setIsNavigable(false);

            targetProp.setOwningAssociation(association);
            return association;
        }

        @Override
        public EObject caseDependency(Dependency dependency) {
            if (source instanceof NamedElement) {
                dependency.getClients().add((NamedElement) source);
            }
            if (target instanceof NamedElement) {
                dependency.getSuppliers().add((NamedElement) target);
            }
            return super.caseDependency(dependency);
        }

        /**
         * From org.eclipse.papyrus.uml.service.types.internal.ui.advice.ExtensionEditHelperAdvice.
         */
        @Override
        public EObject caseExtension(Extension extension) {
            ExtensionEnd endSource = UMLFactory.eINSTANCE.createExtensionEnd();
            endSource.setName(EXTENSION + ((NamedElement) source).getName()); // $NON-NLS-1$ //$NON-NLS-2$
            endSource.setType((Type) source);
            endSource.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            extension.getOwnedEnds().add(endSource);
            Property property = UMLFactory.eINSTANCE.createProperty();
            property.setName(BASE + ((NamedElement) target).getName());
            property.setType((Type) target);
            property.setAssociation(extension);
            property.setAggregation(AggregationKind.NONE_LITERAL);
            property.setLower(0);
            extension.getMemberEnds().add(property);
            ((Stereotype) source).getOwnedAttributes().add(property);
            return extension;
        }



        @Override
        public EObject caseInformationFlow(InformationFlow informationFlow) {
            if (source instanceof NamedElement) {
                informationFlow.getInformationSources().add((NamedElement) source);
            }
            if (target instanceof NamedElement) {
                informationFlow.getInformationTargets().add((NamedElement) target);
            }

            return super.caseInformationFlow(informationFlow);
        }

        @Override
        public EObject caseGeneralization(Generalization generalization) {
            if (source instanceof Classifier) {
                generalization.setSpecific((Classifier) source);
            }
            if (target instanceof Classifier) {
                generalization.setGeneral((Classifier) target);
            }
            return super.caseGeneralization(generalization);
        }

        @Override
        public EObject caseInclude(Include include) {
            if (source instanceof UseCase) {
                include.setIncludingCase((UseCase) source);
            }
            if (target instanceof UseCase) {
                include.setAddition((UseCase) target);
            }
            return super.caseInclude(include);
        }

        @Override
        public EObject caseSubstitution(Substitution substitution) {
            if (source instanceof Classifier) {
                substitution.setSubstitutingClassifier((Classifier) source);
            }
            if (target instanceof Classifier) {
                substitution.setContract((Classifier) target);
            }
            return substitution;
        }

        @Override
        public EObject caseManifestation(Manifestation manifestation) {
            if (source instanceof NamedElement) {
                manifestation.getClients().add((NamedElement) source);
            }
            if (target instanceof PackageableElement) {
                manifestation.setUtilizedElement((PackageableElement) target);
            }
            return manifestation;
        }

        @Override
        public EObject caseConnector(Connector connector) {
            ConnectorEnd sourceEnd = UMLFactory.eINSTANCE.createConnectorEnd();
            if (this.source instanceof ConnectableElement) {
                sourceEnd.setRole((ConnectableElement) this.source);
                connector.getEnds().add(sourceEnd);
            }

            Object sourceVisualParent = this.representationQuery.getVisualParent(this.sourceView);
            if (sourceVisualParent != null) {
                Object visualSourceSementicParent = this.representationQuery.getSemanticElement(sourceVisualParent);
                if (visualSourceSementicParent instanceof Property) {
                    Property sourceProperty = (Property) visualSourceSementicParent;
                    connector.getEnds().get(0).setPartWithPort(sourceProperty);
                }
            }

            ConnectorEnd targetEnd = UMLFactory.eINSTANCE.createConnectorEnd();

            if (this.target instanceof ConnectableElement) {
                targetEnd.setRole((ConnectableElement) this.target);
                connector.getEnds().add(targetEnd);
            }

            Object targetVisualParent = this.representationQuery.getVisualParent(this.targetView);

            if (targetVisualParent != null) {
                Object visualTargetParent = this.representationQuery.getSemanticElement(targetVisualParent);
                if (visualTargetParent instanceof Property) {
                    Property targetProperty = (Property) visualTargetParent;
                    connector.getEnds().get(1).setPartWithPort(targetProperty);
                }
            }

            return super.caseConnector(connector);
        }

        @Override
        public EObject casePackageImport(PackageImport object) {
            if (source instanceof Namespace) {
                object.setImportingNamespace((Namespace) source);
            }
            if (target instanceof Package) {
                object.setImportedPackage((Package) target);
            }

            return super.casePackageImport(object);
        }

        @Override
        public EObject casePackageMerge(PackageMerge object) {
            if (source instanceof Package) {
                object.setReceivingPackage((Package) source);
            }
            if (target instanceof Package) {
                object.setMergedPackage((Package) target);
            }

            return super.casePackageMerge(object);
        }

        @Override
        public EObject caseTransition(Transition transition) {
            if (source instanceof Vertex) {
                transition.setSource((Vertex) source);
            }
            if (target instanceof Vertex) {
                transition.setTarget((Vertex) target);
            }
            return super.caseTransition(transition);
        }

        @Override
        public EObject caseInterfaceRealization(InterfaceRealization interfaceRealization) {
            if (source instanceof BehavioredClassifier) {
                interfaceRealization.setImplementingClassifier((BehavioredClassifier) source);
            }
            if (target instanceof Interface) {
                interfaceRealization.setContract((Interface) target);
            }
            return super.caseInterfaceRealization(interfaceRealization);
        }

        @Override
        public EObject caseExtend(Extend extend) {
            if (source instanceof UseCase) {
                extend.setExtension((UseCase) source);
            }
            if (target instanceof UseCase) {
                ExtensionPoint targetExtensionpoint = UMLFactory.eINSTANCE.createExtensionPoint();

                extend.getExtensionLocations().add(targetExtensionpoint);
                ((UseCase) target).getExtensionPoints().add(targetExtensionpoint);
                extend.setExtendedCase((UseCase) target);
            }
            return extend;
        }
    }
}

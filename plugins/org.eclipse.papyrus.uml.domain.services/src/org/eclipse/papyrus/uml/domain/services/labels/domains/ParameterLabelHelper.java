/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.COMMA;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.D_DOTS;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EMPTY;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EQL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.SPACE;

import org.eclipse.uml2.uml.Parameter;

/**
 * Utility class for <code>org.eclipse.uml2.uml.Paramter</code>.
 */
// CHECKSTYLE:OFF Papyrus org.eclipse.papyrus.uml.tools.utils.ParameterUtil
public class ParameterLabelHelper {

    // CHECKSTYLE:ON
    public static final String UNDEFINED_TYPE_NAME = "<Undefined>"; //$NON-NLS-1$

    /**
     * Provider of element name.
     */
    private INamedElementNameProvider namedElementNameProvider;

    /**
     * <code>true</code> if modifiers should be displayed, <code>false</code>
     * otherwise
     */
    private final boolean displayModifier;

    /**
     * <code>true</code> if default value should be displayed, <code>false</code>
     * otherwise
     */
    private final boolean displayDefaultValue;

    /**
     * Constructor.
     *
     * @param displayModifier
     *                                 <code>true</code> if modifiers should be
     *                                 displayed, <code>false</code> otherwise
     * @param displayDefaultValue
     *                                 <code>true</code> if default value should be
     *                                 displayed, <code>false</code> otherwise
     * @param namedElementNameProvider
     *                                 provider of element name.
     */
    public ParameterLabelHelper(boolean displayModifier, boolean displayDefaultValue,
            INamedElementNameProvider namedElementNameProvider) {
        super();
        this.displayModifier = displayModifier;
        this.displayDefaultValue = displayDefaultValue;
        this.namedElementNameProvider = namedElementNameProvider;
    }

    /**
     * Get the custom label of the parameter.
     *
     * @param parameter
     *                  the parameter with the label to display
     *
     * @return the string corresponding to the label of the parameter
     */
    public String getLabel(Parameter parameter) {
        StringBuffer buffer = new StringBuffer();

        // direction
        buffer.append(parameter.getDirection().getLiteral());

        // name
        buffer.append(SPACE);
        buffer.append(getNonNullString(this.namedElementNameProvider.getName(parameter)));

        // Separator
        buffer.append(D_DOTS + SPACE);

        // type
        if (parameter.getType() != null) {
            buffer.append(this.namedElementNameProvider.getName(parameter.getType()));
        } else {
            buffer.append(UNDEFINED_TYPE_NAME);
        }

        // default value
        if (this.displayDefaultValue) {
            if (parameter.getDefaultValue() != null) {
                buffer.append(SPACE + EQL + SPACE);
                buffer.append(parameter.getDefault());
            }
        }

        // parameter modifiers
        if (this.displayModifier) {
            String modifiers = getNonNullString(this.getModifiersAsString(parameter, false));
            if (!modifiers.equals(EMPTY)) {

                if (!buffer.toString().endsWith(SPACE)) {
                    buffer.append(SPACE);
                }

                buffer.append(modifiers);
            }
        }
        return buffer.toString();
    }

    /**
     * Returns the modifier of the parameter, separated by a comma, as as single
     * line if <code>multiline</code> is <code>false</code> or as a multiline string
     * if <code>multiline</code> is <code>false</code>.
     *
     * @param parameter
     *                  the parameter with the label to display
     * @param multiLine
     *                  boolean that indicates if the string should have several
     *                  lines when set to <code>true</code> or only one line when
     *                  set to <code>false</code>.
     *
     * @return a string giving all modifiers for the parameter
     */
    // CHECKSTYLE:OFF Papyrus code
    private String getModifiersAsString(Parameter parameter, boolean multiLine) {
        StringBuffer buffer = new StringBuffer();
        boolean needsComma = false;
        String NL = (multiLine) ? EOL : SPACE;

        // Return parameter modifiers
        if (parameter.isOrdered()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "ordered");
            ;
        }
        if (parameter.isUnique()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "unique");
        }
        if (parameter.isException()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "exception");
        }
        if (parameter.isStream()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "stream");
        }

        if (!buffer.toString().equals(EMPTY)) {
            buffer.insert(0, OPEN_BRACKET);
            buffer.append(CLOSE_BRACKET);
        }

        return buffer.toString();
    }
    // CHECKSTYLE:ON Papyrus code

    /**
     * Update the modifiers string
     *
     * @param buffer
     *                   the existing bufferString to append
     * @param needsComma
     *                   if it needs coma
     * @param nl
     *                   if it is multiline
     * @param message
     *                   the message top
     * @return true because the modifier string is no more empty
     */
    private static boolean updateModifiersString(StringBuffer buffer, boolean needsComma, String nl, String message) {
        if (needsComma) {
            buffer.append(COMMA);
            buffer.append(nl);
        }
        buffer.append(message);
        return true;
    }
}

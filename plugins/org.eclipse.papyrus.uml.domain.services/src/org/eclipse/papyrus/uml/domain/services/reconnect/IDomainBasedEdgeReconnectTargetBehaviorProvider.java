/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Provider of behavior when reconnect target of semantic elements.
 * 
 * @author Jessy MALLET
 *
 */
public interface IDomainBasedEdgeReconnectTargetBehaviorProvider {

    /**
     * Reconnect target of a semantic element to an other one.
     * 
     * @param elementToReconnect
     *                           the element being reconnected
     * @param oldTarget
     *                           the semantic element pointed by the edge as Target
     *                           before reconnecting
     * @param newTarget
     *                           the semantic element pointed by the edge as Target
     *                           after reconnecting
     * @param newTargetView
     *                           the new target graphical view.
     * @return a Status of the drop
     */
    CheckStatus reconnectTarget(EObject elementToReconnect, EObject oldTarget,
            EObject newTarget, Object newTargetView);

}

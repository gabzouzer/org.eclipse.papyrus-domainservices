/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.connector.ConnectorHelper;
import org.eclipse.papyrus.uml.domain.services.extension.ExtensionHelper;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;

public class ElementDomainBasedEdgeCreationChecker implements IDomainBasedEdgeCreationChecker {

    @Override
    public CheckStatus canCreate(EObject semanticEdgeSource, EObject semanticEdgeTarget, String type,
            String referenceName, IViewQuerier representationQuery, Object sourceView, Object targetView) {
        final CheckStatus result;
        switch (UMLHelper.toEClass(type).getClassifierID()) {
        case UMLPackage.ASSOCIATION:
            result = handleAssociation(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.CONNECTOR:
            result = handleConnector(representationQuery, sourceView, targetView);
            break;
        case UMLPackage.USAGE:
            result = handleUsage(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.GENERALIZATION:
            result = handleGeneralization(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INFORMATION_FLOW:
            result = handleInformationFlow(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.SUBSTITUTION:
            result = handleSubstitution(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.MANIFESTATION:
            result = handleManifestation(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.PACKAGE_MERGE:
            result = handlePackageMerge(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.PACKAGE_IMPORT:
            result = handlePackageImport(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.TRANSITION:
            result = handleTransition(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INCLUDE:
            result = handleInclude(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.DEPENDENCY:
            result = handleDependency(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.EXTEND:
            result = handleExtend(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INTERFACE_REALIZATION:
            result = handleInterfaceRealization(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.EXTENSION:
            result = handleExtension(semanticEdgeSource, semanticEdgeTarget);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handleExtension(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        ExtensionHelper extensionHelper = new ExtensionHelper();
        if (!extensionHelper.canCreate(semanticEdgeSource, semanticEdgeTarget)) {
            result = CheckStatus.no("Extension can only be connected between a Stereotype and a Metaclass.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInterfaceRealization(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof BehavioredClassifier)) {
            result = CheckStatus.no("The source of an InterfaceRealization should be a BehaviorClassifier");
        } else if (!(semanticEdgeTarget instanceof Interface)) {
            result = CheckStatus.no("The target of an InterfaceRealization should be an Interface");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handlePackageMerge(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Package) || !(semanticEdgeTarget instanceof Package)) {
            result = CheckStatus.no("PackageImport can only be connected to Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handlePackageImport(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Namespace)) {
            result = CheckStatus.no("PackageImport can only be connected from a Namespace.");
        } else if (!(semanticEdgeTarget instanceof Package)) {
            result = CheckStatus.no("PackageImport can only be connected to Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleGeneralization(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Generalization can only be connected to Classifiers.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Generalization cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleAssociation(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Association can only be connected to Classifiers.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleSubstitution(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Substitution can only be connected to Classifiers.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Substitution cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleManifestation(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof PackageableElement)) {
            result = CheckStatus.no("Manifestation can only be connected between NamedElement and PackageableElement.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Manifestation cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInformationFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            return CheckStatus.no("InformationFlow can only be connected to NamedElements.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleDependency(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            result = CheckStatus.no("Dependency can only be connected to NamedElement.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInclude(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof UseCase) || !(semanticEdgeTarget instanceof UseCase)) {
            result = CheckStatus.no("Include can only be connected between Use Cases.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Include cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleUsage(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            return CheckStatus.no("Usage can only be connected to NamedElements.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleConnector(IViewQuerier representationQuery, Object sourceView, Object targetView) {
        ConnectorHelper connectorHelper = new ConnectorHelper();
        return connectorHelper.canCreateConnector(representationQuery, sourceView, targetView);
    }

    private CheckStatus handleTransition(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof Vertex) || !(semanticEdgeTarget instanceof Vertex)) {
            return CheckStatus.no("A Transition can only be connected to Vertex.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleExtend(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof UseCase) || !(semanticEdgeTarget instanceof UseCase)) {
            result = CheckStatus.no("Extend can only be connected between Use Cases.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Extend cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

}

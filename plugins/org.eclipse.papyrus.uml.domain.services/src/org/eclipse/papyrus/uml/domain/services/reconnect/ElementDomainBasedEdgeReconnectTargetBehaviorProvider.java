/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import java.text.MessageFormat;
import java.util.Objects;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Reconnect target behavior provider of a semantic element.
 * 
 * @author Jessy MALLET
 *
 */
public class ElementDomainBasedEdgeReconnectTargetBehaviorProvider
        implements IDomainBasedEdgeReconnectTargetBehaviorProvider {

    private final IViewQuerier representationQuery;

    /**
     * 
     * Constructor.
     *
     * @param representationQuery
     *                            the {@link IViewQuerier}.
     */
    public ElementDomainBasedEdgeReconnectTargetBehaviorProvider(IViewQuerier representationQuery) {
        this.representationQuery = Objects.requireNonNull(representationQuery);
    }

    @Override
    public CheckStatus reconnectTarget(EObject elementToReconnect, EObject oldTarget, EObject newTarget,
            Object newTargetView) {
        if (elementToReconnect == null || oldTarget == null || newTarget == null) {
            return CheckStatus.no(
                    MessageFormat.format("Invalid input for reconnection (element ={0} oldTarget ={1} newTarget = {2})", //$NON-NLS-1$
                            elementToReconnect, oldTarget, newTarget));
        }
        return new ReconnectTargetBehaviorProviderSwitch(this.representationQuery, oldTarget, newTarget, newTargetView)
                .doSwitch(elementToReconnect);
    }

    static class ReconnectTargetBehaviorProviderSwitch extends UMLSwitch<CheckStatus> {

        private final IViewQuerier representationQuery;

        private final EObject oldTarget;

        private final EObject newTarget;

        private final Object newTargetView;

        ReconnectTargetBehaviorProviderSwitch(IViewQuerier representationQuery, EObject oldTarget, EObject newTarget,
                Object newTargetView) {
            super();
            this.representationQuery = representationQuery;
            this.oldTarget = oldTarget;
            this.newTarget = newTarget;
            this.newTargetView = newTargetView;
        }

        @Override
        public CheckStatus caseAssociation(Association association) {
            Classifier newType = (Classifier) newTarget;
            Property sourceProperty = association.getMemberEnds().get(0);
            sourceProperty.setType(newType);

            // The reconnect of source also results in the modification of the semantic
            // target of the association (if not owned by the association) the new parent of
            // the semantic target has to be the new graphical source.
            Property targetProperty = association.getMemberEnds().get(1);
            if (!association.getOwnedEnds().contains(targetProperty)) {
                if (newTarget instanceof StructuredClassifier) {
                    // Move opposite member end (targetProperty) to its target container
                    ((StructuredClassifier) newTarget).getOwnedAttributes().add(targetProperty);
                }
            }
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInterfaceRealization(InterfaceRealization object) {
            // Type check is done in the ReconnectionChecker
            object.setContract((Interface) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseConnector(Connector connector) {
            Object targetVisualParent = this.representationQuery.getVisualParent(this.newTargetView);
            if (targetVisualParent != null) {
                Object visualTargetParent = this.representationQuery.getSemanticElement(targetVisualParent);
                ConnectorEnd connectorEnd = connector.getEnds().get(1);
                connectorEnd.setRole((ConnectableElement) this.newTarget);
                if (visualTargetParent instanceof Property) {
                    Property targetProperty = (Property) visualTargetParent;
                    connectorEnd.setPartWithPort(targetProperty);
                } else {
                    connectorEnd.setPartWithPort(null);
                }
                return CheckStatus.YES;
            }

            return super.caseConnector(connector);
        }

        @Override
        public CheckStatus caseDependency(Dependency dependency) {
            dependency.getSuppliers().remove(oldTarget);
            dependency.getSuppliers().add((NamedElement) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseExtend(Extend extend) {
            extend.setExtendedCase((UseCase) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseExtension(Extension extension) {
            String oldName = extension.getName();
            try {
                extension.setName(null); // We need to reset the name to force the getDefaultName to compute the
                // new one.
                ElementDefaultNameProvider elementDefaultNameProvider = new ElementDefaultNameProvider();
                String deducedName = elementDefaultNameProvider.getDefaultName(extension, extension.eContainer());
                Type sourceType = extension.getStereotype();
                // change the name and the type of the property
                if (sourceType instanceof Stereotype) {
                    EList<Property> attributes = ((Stereotype) sourceType).getOwnedAttributes();
                    for (Property property : attributes) {
                        if (property.getAssociation() == extension) {
                            property.setType((Type) newTarget);
                            property.setName(
                                    ElementDomainBasedEdgeInitializer.BASE + ((NamedElement) newTarget).getName());
                            break;
                        }
                    }
                    // change the extension name, if the user doesn't have rename the extension!
                    if (oldName.contains(deducedName)) {
                        if (oldName.indexOf(deducedName) == 0) {
                            oldName = oldName.substring(deducedName.length());
                            try {
                                // if there is not exception, the name didn't edited by the user
                                extension.setName(
                                        elementDefaultNameProvider.getDefaultName(extension, extension.eContainer()));
                            } catch (NumberFormatException e) {
                                // do nothing
                            }
                        }
                    }
                }
            } finally {
                if (extension.getName() == null) {
                    extension.setName(oldName);
                }
            }
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            generalization.setGeneral((Classifier) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            include.setAddition((UseCase) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInformationFlow(InformationFlow informationFlow) {
            informationFlow.getInformationTargets().remove(oldTarget);
            informationFlow.getInformationTargets().add((NamedElement) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseManifestation(Manifestation manifestation) {
            manifestation.setUtilizedElement((PackageableElement) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport packImport) {
            packImport.setImportedPackage((Package) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge object) {
            object.setMergedPackage((Package) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseSubstitution(Substitution substitution) {
            substitution.setContract((Classifier) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            transition.setTarget((Vertex) newTarget);
            return CheckStatus.YES;
        }

    }

}

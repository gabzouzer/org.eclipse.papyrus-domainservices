/*****************************************************************************
 * Copyright (c) 2009, 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yann TANGUY (CEA LIST) yann.tanguy@cea.fr - Initial API and implementation
 *  Nicolas FAUVERGUE (ALL4TEC) nicolas.fauvergue@all4tec.net - Bug 496905
 *  Obeo - Papyrus Web refactoring
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.COMMA;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.D_DOTS;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EMPTY;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EQL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.SLASH;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.SPACE;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.TILDE;

import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

/**
 * Utility class for <code>org.eclipse.uml2.uml.Property</code>.
 */
// CHECKSTYLE:OFF Papyrus org.eclipse.papyrus.uml.tools.utils.PropertyUtil
public class PropertyLabelHelper {

    // CHECKSTYLE:ON
    public static final String UNDEFINED_TYPE_NAME = "<Undefined>"; //$NON-NLS-1$

    private ValueSpecificationLabelHelper valueSpecificationLabelHelper;

    private MultiplicityLabelHelper multiplicityLabelHelper;

    private INamedElementNameProvider namedElementNameProvider;

    private final boolean displayModifier;

    private final boolean displayDefaultValue;

    private VisibilityLabelHelper visibilityLabelHelper;

    public PropertyLabelHelper(boolean displayModifier, boolean displayDefaultValue,
            INamedElementNameProvider namedElementNameProvider, VisibilityLabelHelper visibilityLabelHelper) {
        super();
        this.displayModifier = displayModifier;
        this.displayDefaultValue = displayDefaultValue;
        this.namedElementNameProvider = namedElementNameProvider;
        this.valueSpecificationLabelHelper = new ValueSpecificationLabelHelper(namedElementNameProvider);
        this.multiplicityLabelHelper = new MultiplicityLabelHelper(namedElementNameProvider);
        this.visibilityLabelHelper = visibilityLabelHelper;
    }

    /**
     * return the custom label of the property, given UML2 specification and a
     * custom style.
     *
     * @param style
     *              the collection of label fragments to display
     *
     * @return the string corresponding to the label of the property
     */
    public String getLabel(Property property) {
        StringBuffer buffer = new StringBuffer();

        // visibility
        buffer.append(getNonNullString(visibilityLabelHelper.getVisibilityAsSign(property)));

        // derived property
        if (property.isDerived()) {
            buffer.append(SLASH);
        }
        // name
        buffer.append(SPACE);
        buffer.append(getNonNullString(this.namedElementNameProvider.getName(property)));

        // Separator
        buffer.append(D_DOTS + SPACE);

        // isConjugated
        if (property instanceof Port && ((Port) property).isConjugated()) {
            buffer.append(TILDE);
        }

        // type
        if (property.getType() != null) {
            buffer.append(this.namedElementNameProvider.getName(property.getType()));
        } else {
            buffer.append(UNDEFINED_TYPE_NAME);
        }

        // multiplicity -> do not display [1]
        String multiplicity = getNonNullString(this.multiplicityLabelHelper.formatMultiplicity(property));
        if (!multiplicity.isBlank()) {
            buffer.append(SPACE);
            buffer.append(multiplicity);
        }

        // default value
        if (this.displayDefaultValue) {
            if (property.getDefaultValue() != null) {
                buffer.append(SPACE + EQL + SPACE);
                buffer.append(getNonNullString(
                        this.valueSpecificationLabelHelper.getSpecificationValue(property.getDefaultValue(), true)));
            }
        }

        // property modifiers
        if (this.displayModifier) {
            String modifiers = getNonNullString(this.getModifiersAsString(property, false));
            if (!modifiers.equals(EMPTY)) {

                if (!buffer.toString().endsWith(SPACE)) {
                    buffer.append(SPACE);
                }

                buffer.append(modifiers);
            }
        }
        return buffer.toString();
    }

    /**
     * Returns the modifier of the property, separated by a comma, as as single line
     * if <code>multiline</code> is <code>false</code> or as a multiline string if
     * <code>multiline</code> is <code>false</code>.
     *
     * @param multiLine
     *                  boolean that indicates if the string should have several
     *                  lines when set to <code>true</code> or only one line when
     *                  set to <code>false</code>.
     *
     * @return a string giving all modifiers for the property
     */
    // CHECKSTYLE:OFF Papyrus code
    private String getModifiersAsString(Property property, boolean multiLine) {
        StringBuffer buffer = new StringBuffer();
        boolean needsComma = false;
        String NL = (multiLine) ? EOL : SPACE;

        // Return property modifiers
        if (property.isReadOnly()) {
            buffer.append("readOnly"); //$NON-NLS-1$
            needsComma = true;
        }
        if (property.isDerivedUnion()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "union"); //$NON-NLS-1$
        }
        if (property.isOrdered()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "ordered"); //$NON-NLS-1$
        }
        if (property.isUnique()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "unique"); //$NON-NLS-1$
        }

        // is the property redefining another property ?
        for (Property current : property.getRedefinedProperties()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "redefines "); //$NON-NLS-1$
            buffer.append(this.namedElementNameProvider.getName(current));
        }

        // is the property subsetting another property ?
        for (Property current : property.getSubsettedProperties()) {
            needsComma = updateModifiersString(buffer, needsComma, NL, "subsets "); //$NON-NLS-1$
            buffer.append(this.namedElementNameProvider.getName(current));
        }

        if (!buffer.toString().equals(EMPTY)) {
            buffer.insert(0, OPEN_BRACKET);
            buffer.append(CLOSE_BRACKET);
        }

        return buffer.toString();
    }
    // CHECKSTYLE:ON Papyrus code

    /**
     * Update the modifiers string
     *
     * @param buffer
     *                   the existing bufferString to append
     * @param needsComma
     *                   if it needs coma
     * @param nl
     *                   if it is multiline
     * @param message
     *                   the message top
     * @return true because the modifier string is no more empty
     */
    private static boolean updateModifiersString(StringBuffer buffer, boolean needsComma, String nl, String message) {
        if (needsComma) {
            buffer.append(COMMA);
            buffer.append(nl);
        }
        buffer.append(message);
        return true;
    }
}

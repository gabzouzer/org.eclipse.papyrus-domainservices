/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Duration;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.DurationInterval;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Interval;
import org.eclipse.uml2.uml.IntervalConstraint;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeExpression;
import org.eclipse.uml2.uml.TimeInterval;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Generic implementation of {@link IElementConfigurer} to be used for most UML
 * element.
 *
 * @author Arthur Daussy
 */
public class ElementConfigurer implements IElementConfigurer {

    /**
     * Initialize a semantic element.
     *
     * @param toInit
     *               the element to init
     * @param parent
     *               the context parent
     * @return the given element for fluent API use
     */
    @Override
    public EObject configure(EObject toInit, EObject parent) {
        if (toInit == null) {
            return null;
        }
        new ElementInitializerImpl(parent).doSwitch(toInit);
        return toInit;
    }

    static class ElementInitializerImpl extends UMLSwitch<Void> {
        private final EObject parent;

        ElementInitializerImpl(EObject parent) {
            super();
            this.parent = parent;
        }

        @Override
        public Void caseConstraint(Constraint constraint) {
            // Avoid overriding existing specification (specification is already set for
            // TimeConstraint, DurationConstraint, IntervalConstraint)
            if (constraint.getSpecification() == null) {
                OpaqueExpression spec = UMLFactory.eINSTANCE.createOpaqueExpression();
                spec.setName("constraintSpec"); //$NON-NLS-1$
                spec.getLanguages().add("OCL"); //$NON-NLS-1$
                spec.getBodies().add("true"); //$NON-NLS-1$
                constraint.setSpecification(spec);
            }
            return super.caseConstraint(constraint);
        }

        /**
         * When a DurationConstraint is created, set its specification with a new
         * DurationInterval and initialize the min/max feature with Durations. These
         * Durations have both a LiteralInteger as expr.
         *
         * @see org.eclipse.papyrus.uml.diagram.timing.custom.edit.commands.CustomDurationConstraintCreateCommand
         * 
         * @param durationConstraint
         *                           the element to configure
         * @return
         */
        @Override
        public Void caseDurationConstraint(DurationConstraint durationConstraint) {
            if (parent instanceof Element) {
                durationConstraint.getConstrainedElements().clear();
                durationConstraint.getConstrainedElements().add((Element) parent);
            }
            DurationInterval durationInterval = UMLFactory.eINSTANCE.createDurationInterval();
            Model minMaxContainer = durationConstraint.getModel();
            if (minMaxContainer != null) {
                Duration durationMin = (Duration) minMaxContainer.createPackagedElement("d1",
                        UMLPackage.eINSTANCE.getDuration());
                Duration durationMax = (Duration) minMaxContainer.createPackagedElement("d2",
                        UMLPackage.eINSTANCE.getDuration());
                LiteralInteger integerMin = UMLFactory.eINSTANCE.createLiteralInteger();
                LiteralInteger integerMax = UMLFactory.eINSTANCE.createLiteralInteger();
                durationMin.setExpr(integerMin);
                durationMax.setExpr(integerMax);
                durationInterval.setMin(durationMin);
                durationInterval.setMax(durationMax);
            }
            durationConstraint.setSpecification(durationInterval);
            return super.caseDurationConstraint(durationConstraint);
        }

        /**
         * When an IntervalConstraint is created, set its specification with a new
         * Interval and initialize the min/max feature. There is no specific code for
         * the initialization of min/max features, this was done to homogenize code in
         * comparison to DurationConstraint and TimeConstraint.
         * 
         * @see org.eclipse.papyrus.uml.service.types.helper.IntervalConstraintEditHelper
         *
         * @param intervalConstraint
         *                           the element to configure
         * @return
         */
        @Override
        public Void caseIntervalConstraint(IntervalConstraint intervalConstraint) {
            // Avoid overriding existing specification (specification is already set for
            // TimeConstraint, DurationConstraint)
            if (intervalConstraint.getSpecification() == null) {
                Interval interval = UMLFactory.eINSTANCE.createInterval();
                Model minMaxContainer = intervalConstraint.getModel();
                if (minMaxContainer != null) {
                    LiteralInteger literalIntegerMin = (LiteralInteger) minMaxContainer.createPackagedElement("i1",
                            UMLPackage.eINSTANCE.getLiteralInteger());
                    LiteralInteger literalIntegerMax = (LiteralInteger) minMaxContainer.createPackagedElement("i2",
                            UMLPackage.eINSTANCE.getLiteralInteger());
                    interval.setMin(literalIntegerMin);
                    interval.setMax(literalIntegerMax);
                }
                intervalConstraint.setSpecification(interval);
            }
            return super.caseIntervalConstraint(intervalConstraint);
        }

        /**
         * When a TimeConstraint is created, set its specification with a new
         * TimeInterval and initialize the min/max features with TimeExpressions. These
         * TimeExpressions have both a LiteralInteger as expr.
         *
         * @see org.eclipse.papyrus.uml.diagram.timing.custom.edit.commands.CustomTimeConstraintCreateCommand
         * 
         * @param timeConstraint
         *                       the element to configure
         * @return
         */
        @Override
        public Void caseTimeConstraint(TimeConstraint timeConstraint) {
            if (parent instanceof Element) {
                timeConstraint.getConstrainedElements().clear();
                timeConstraint.getConstrainedElements().add((Element) parent);
            }
            TimeInterval timeInterval = UMLFactory.eINSTANCE.createTimeInterval();
            Model minMaxContainer = timeConstraint.getModel();
            if (minMaxContainer != null) {
                TimeExpression timeExpressionMin = (TimeExpression) minMaxContainer.createPackagedElement("t1",
                        UMLPackage.eINSTANCE.getTimeExpression());
                TimeExpression timeExpressionMax = (TimeExpression) minMaxContainer.createPackagedElement("t2",
                        UMLPackage.eINSTANCE.getTimeExpression());
                LiteralInteger literalIntegerMin = UMLFactory.eINSTANCE.createLiteralInteger();
                LiteralInteger literalIntegerMax = UMLFactory.eINSTANCE.createLiteralInteger();
                timeExpressionMin.setExpr(literalIntegerMin);
                timeExpressionMax.setExpr(literalIntegerMax);
                timeInterval.setMin(timeExpressionMin);
                timeInterval.setMax(timeExpressionMax);
            }
            timeConstraint.setSpecification(timeInterval);
            return super.caseTimeConstraint(timeConstraint);
        }

        /**
         * UML spec indicates that an ExtensionEnd should have its Aggregation set to
         * "Composite".
         *
         * @param extensionEnd
         *                     the {@link ExtensionEnd} to configure
         */
        @Override
        public Void caseExtensionEnd(ExtensionEnd extensionEnd) {
            extensionEnd.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            return super.caseExtensionEnd(extensionEnd);
        }

        /**
         * See Papyrus behavior:
         * org.eclipse.papyrus.uml.service.types.helper.InteractionOperandEditHelper.getConfigureCommand(ConfigureRequest).
         */
        @Override
        public Void caseInteractionOperand(InteractionOperand interactionOperand) {
            interactionOperand.createGuard("guard");
            return super.caseInteractionOperand(interactionOperand);
        }

        @Override
        public Void casePort(Port port) {
            port.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            return super.casePort(port);
        }

        @Override
        public Void caseUseCase(UseCase useCase) {
            Element owner = useCase.getOwner();
            if (owner instanceof Classifier) {
                ((Classifier) owner).getUseCases().add(useCase);
            }
            return super.caseUseCase(useCase);
        }

        @Override
        public Void caseProperty(Property property) {
            Element owner = property.getOwner();
            if (owner instanceof Collaboration) {
                ((Collaboration) owner).getCollaborationRoles().add(property);
            }
            return super.caseProperty(property);
        }

        @Override
        public Void caseNamedElement(NamedElement namedElement) {
            namedElement.setName(new ElementDefaultNameProvider().getDefaultName(namedElement, this.parent));
            return super.caseNamedElement(namedElement);
        }
    }
}

/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create.diagrams;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.create.CreationStatus;
import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.create.ElementCreator;
import org.eclipse.papyrus.uml.domain.services.create.ICreator;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Specific implementation of {@link ICreator} for the Composite structure
 * diagram.
 *
 * @author Arthur Daussy
 */
public class CompositeStructureDiagramElementCreator implements ICreator {

    private final ICreator delegate;

    public CompositeStructureDiagramElementCreator(ICreator delegate) {
        super();
        this.delegate = delegate;
    }

    public static CompositeStructureDiagramElementCreator buildDefault(ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new CompositeStructureDiagramElementCreator(
                new ElementCreator(new ElementConfigurer(), new ElementFeatureModifier(crossRef, editableChecker)));
    }

    @Override
    public CreationStatus create(EObject parent, String type, String referenceName) {

        if (parent == null || type == null || referenceName == null) {
            return CreationStatus.createFailingStatus(
                    MessageFormat.format("Invalid input for creation (parent ={0} type ={1} referenceName = {2})", //$NON-NLS-1$
                            parent, type, referenceName));
        }

        CreatorSwitch creatorSwitch = new CreatorSwitch(type, referenceName);
        CreationStatus status = creatorSwitch.doSwitch(parent);
        final CreationStatus result;
        if (status.getState() == State.NOTHING) {
            result = this.delegate.create(parent, type, referenceName);
        } else {
            result = status;
        }
        return result;
    }

    class CreatorSwitch extends UMLSwitch<CreationStatus> {

        private final String type;

        private final String referenceName;

        CreatorSwitch(String type, String referenceName) {
            super();
            this.type = type;
            this.referenceName = referenceName;
        }

        @Override
        public CreationStatus caseProperty(Property parent) {

            final CreationStatus result;
            if (UMLHelper.toEClass(this.type) == UMLPackage.eINSTANCE.getPort() && UMLPackage.eINSTANCE
                    .getStructuredClassifier_OwnedAttribute().getName().equals(this.referenceName)) {
                // The creation of port on property on this diagram is in fact the creation of a
                // port on the type of the
                // property
                Type propType = parent.getType();
                if (propType instanceof StructuredClassifier) {
                    result = CompositeStructureDiagramElementCreator.this.delegate.create(propType, this.type,
                            this.referenceName);
                } else {
                    result = CreationStatus.createFailingStatus(
                            "A Port on a Property can only be created if the type of the property is a StructureClassifier"); //$NON-NLS-1$
                }
            } else if (UMLHelper.toEClass(this.type) == UMLPackage.eINSTANCE.getProperty() && UMLPackage.eINSTANCE
                    .getStructuredClassifier_OwnedAttribute().getName().equals(this.referenceName)) {
                // The creation of property on property on this diagram is in fact the creation
                // of a property on the type of the
                // property
                Type propType = parent.getType();
                if (propType instanceof StructuredClassifier) {
                    result = CompositeStructureDiagramElementCreator.this.delegate.create(propType, this.type,
                            this.referenceName);
                } else {
                    result = CreationStatus.createFailingStatus(
                            "A Property can only be created if the type of the property is a StructureClassifier"); //$NON-NLS-1$
                }
            } else {

                result = super.caseProperty(parent);
            }
            return result;
        }

        @Override
        public CreationStatus defaultCase(EObject parent) {
            return CreationStatus.NOTHING;
        }

    }

}

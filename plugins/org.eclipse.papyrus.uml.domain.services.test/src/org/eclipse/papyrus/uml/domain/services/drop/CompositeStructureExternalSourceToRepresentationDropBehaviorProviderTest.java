/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Property;
import org.junit.jupiter.api.Test;

public class CompositeStructureExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Test dropping a DataType on Property => Propert.type = DataType.
     */
    @Test
    public void testDataTypeDropOnProperty() {
        Property property = create(Property.class);
        DataType dt = create(DataType.class);

        Status status = new CompositeStructureExternalSourceToRepresentationDropBehaviorProvider().drop(dt, property, getCrossRef(),
                getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(dt, property.getType());
    }

    /**
     * Test dropping a Class on Property => Propert.type = Class.
     */
    @Test
    public void testClassDropOnProperty() {
        Property property = create(Property.class);
        Class clazz = create(org.eclipse.uml2.uml.Class.class);

        Status status = new CompositeStructureExternalSourceToRepresentationDropBehaviorProvider().drop(clazz, property, getCrossRef(),
                getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(clazz, property.getType());
    }
}

/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class tests the deletion of many diagram elements.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 *
 */
public class DiagramElementDeletorTest extends AbstractUMLTest {

    private static final String OPERAND_REF = "operand";
    private static final String END = UMLPackage.eINSTANCE.getConnector_End().getName();
    /**
     * UML domain service used to delete UML elements.
     */
    private IDestroyer destroyer;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        destroyer = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker());
    }

    /**
     * Check that deletion of a given {@link Property} from a {@link Class} deletes
     * the {@link Property}.
     */
    @Test
    public void deletePropertyTest() {
        Class clazz = create(Class.class);
        Property property = create(Property.class);
        clazz.getOwnedAttributes().add(property);

        assertEquals(clazz.getOwnedAttributes().size(), 1);
        assertNotNull(property.eContainer());

        destroyer.destroy(property);

        assertEquals(clazz.getOwnedAttributes().size(), 0);
        assertNull(property.eContainer());
    }

    /**
     * Check that deletion of a given {@link Port}, link to an other one with a
     * {@link Usage}, deletes the {@link Port} and the {@link Usage} (and references
     * to {@link Usage} from Ports).
     */

    @Test
    public void deletePortWithUsageTest() {
        Port port1 = create(Port.class);
        Port port2 = create(Port.class);
        Usage usage1 = create(Usage.class);
        usage1.getSuppliers().add(port2);
        usage1.getClients().add(port1);
        assertEquals(port1.getSourceDirectedRelationships().size(), 1);
        assertEquals(port2.getTargetDirectedRelationships().size(), 1);

        destroyer.destroy(port1);

        assertEquals(port1.getSourceDirectedRelationships().size(), 0);
        assertEquals(port2.getTargetDirectedRelationships().size(), 0);
    }

    /**
     * Check that deletion of a given {@link ConnectorEnd} deletes the
     * {@link ConnectorEnd}, its related {@link Connector} and the other
     * {@link ConnectorEnd} of the deleted {@link Connector}.
     */
    @Test
    public void deleteConnectorEndFromConnectorTest() {
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEnd1 = createIn(ConnectorEnd.class, connector, END);
        ConnectorEnd connectorEnd2 = createIn(ConnectorEnd.class, connector, END);

        destroyer.destroy(connectorEnd1);

        assertEquals(connector.getEnds().size(), 0);
        assertNull(connectorEnd1.getOwner());
        assertNull(connectorEnd2.getOwner());
    }

    /**
     * Checks that if at least one the dependency to delete can't be edited then the
     * complete deletion is forbidden.
     */
    @Test
    public void forbidDeletionOnUneditableDependency() {
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEnd1 = createIn(ConnectorEnd.class, connector, END);
        ConnectorEnd connectorEnd2 = createIn(ConnectorEnd.class, connector, END);

        destroyer = ElementDestroyer.buildDefault(getCrossRef(), e -> e == connector);

        DestroyerStatus destroyStatus = destroyer.destroy(connectorEnd1);

        assertFalse(isSuccessStatus(destroyStatus));
        assertEquals(connector.getEnds().size(), 2);
        assertEquals(connector, connectorEnd1.getOwner());
        assertEquals(connector, connectorEnd2.getOwner());
    }

    /**
     * Checks that the deletion on uneditable element is forbidden.
     */
    @Test
    public void forbidDeletionOnUneditableElement() {
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEnd1 = createIn(ConnectorEnd.class, connector, END);
        ConnectorEnd connectorEnd2 = createIn(ConnectorEnd.class, connector, END);

        destroyer = ElementDestroyer.buildDefault(getCrossRef(), e -> e != connectorEnd1);

        DestroyerStatus destroyStatus = destroyer.destroy(connectorEnd1);

        assertFalse(isSuccessStatus(destroyStatus));
        assertEquals(connector.getEnds().size(), 2);
        assertEquals(connector, connectorEnd1.getOwner());
        assertEquals(connector, connectorEnd2.getOwner());
    }

    /**
     * Checks that if, at least, a container of an element to delete or an element
     * that have a cross ref to the element to delete, is not editable,then the
     * complete deletion is forbidden.
     */
    @Test
    public void forbidDeletionOnUneditableParentElement() {
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEnd1 = createIn(ConnectorEnd.class, connector, END);
        ConnectorEnd connectorEnd2 = createIn(ConnectorEnd.class, connector, END);

        destroyer = ElementDestroyer.buildDefault(getCrossRef(), e -> e != connector);

        DestroyerStatus destroyStatus = destroyer.destroy(connectorEnd1);

        assertFalse(isSuccessStatus(destroyStatus));
        assertEquals(connector.getEnds().size(), 2);
        assertEquals(connector, connectorEnd1.getOwner());
        assertEquals(connector, connectorEnd2.getOwner());
    }

    private Boolean isSuccessStatus(DestroyerStatus destroyStatus) {
        return State.DONE.equals(destroyStatus.getState());
    }

    /**
     * Checks that deleting a Package imported by another package also deletes the
     * package import.
     */
    @Test
    public void deletePackageImportedByOther() {
        Package pack1 = create(Package.class);
        Package pack2 = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, pack1);
        packImport.setImportedPackage(pack2);

        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(pack2);

        assertTrue(isSuccessStatus(destroyStatus));
        assertTrue(destroyStatus.getElements().contains(pack2));
        assertTrue(destroyStatus.getElements().contains(packImport));
        assertTrue(pack1.getPackageImports().isEmpty());

    }

    /**
     * Checks that deleting a Package merged by another package also deletes the
     * package import.
     */
    @Test
    public void deletePackageMergedByOther() {
        Package pack1 = create(Package.class);
        Package pack2 = create(Package.class);

        PackageMerge packImport = createIn(PackageMerge.class, pack1);
        packImport.setMergedPackage(pack2);

        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(pack2);

        assertTrue(isSuccessStatus(destroyStatus));
        assertTrue(destroyStatus.getElements().contains(pack2));
        assertTrue(destroyStatus.getElements().contains(packImport));
        assertTrue(pack1.getPackageMerges().isEmpty());

    }

    /**
     * Check that deleting a {@link UseCase} link to an other one with
     * {@link Include} relation remove also {@link Include} relation.
     */
    @Test
    public void deleteUseCaseWithInclude() {
        Include include = create(Include.class);
        UseCase useCaseSource = create(UseCase.class);
        UseCase useCaseTarget = create(UseCase.class);
        useCaseSource.getIncludes().add(include);
        new ElementDomainBasedEdgeInitializer().initialize(include, useCaseSource, useCaseTarget, null, null, null);

        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(useCaseTarget);
        assertTrue(isSuccessStatus(destroyStatus));
        assertTrue(destroyStatus.getElements().contains(useCaseTarget));
        assertTrue(destroyStatus.getElements().contains(include));
        assertTrue(useCaseSource.getIncludes().isEmpty());
    }

    /**
     * Check that deleting a {@link UseCase} link to an other one with
     * {@link Extend} relation remove also {@link Extend} relation.
     */
    @Test
    public void deleteUseCaseWithExtend() {
        Extend extend = create(Extend.class);
        UseCase useCaseSource = create(UseCase.class);
        UseCase useCaseTarget = create(UseCase.class);
        useCaseSource.getExtends().add(extend);
        new ElementDomainBasedEdgeInitializer().initialize(extend, useCaseSource, useCaseTarget, null, null, null);

        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(useCaseTarget);
        assertTrue(isSuccessStatus(destroyStatus));
        assertTrue(destroyStatus.getElements().contains(useCaseTarget));
        assertTrue(destroyStatus.getElements().contains(extend));
        assertTrue(useCaseSource.getExtends().isEmpty());
    }

    @Test
    public void deleteLastInteractionOperandInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        InteractionOperand interactionOperand = createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);
        
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(interactionOperand);
        assertEquals(State.DONE, destroyStatus.getState());
        assertTrue(combinedFragment.getOperands().isEmpty());
        assertTrue(rootInteraction.getFragments().isEmpty());
    }

    @Test
    public void deleteNotLastInteractionOperandInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        InteractionOperand interactionOperand = createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);
        createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);

        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(interactionOperand);
        assertEquals(State.DONE, destroyStatus.getState());
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
    }

    @Test
    public void deleteInteractionOperandNotInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);
        InteractionOperand interactionOperand = create(InteractionOperand.class);

        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(interactionOperand);
        assertEquals(State.DONE, destroyStatus.getState());
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
    }

    @Test
    public void deleteInteractionOperandInInteraction() {
        Interaction rootInteraction = create(Interaction.class);
        InteractionOperand interactionOperand = createIn(InteractionOperand.class, rootInteraction);
        
        assertFalse(rootInteraction.getFragments().isEmpty());
        DestroyerStatus destroyStatus = ElementDestroyer.buildDefault(getCrossRef(), getEditableChecker())
                .destroy(interactionOperand);
        assertEquals(State.DONE, destroyStatus.getState());
        assertTrue(rootInteraction.getFragments().isEmpty());
    }
}

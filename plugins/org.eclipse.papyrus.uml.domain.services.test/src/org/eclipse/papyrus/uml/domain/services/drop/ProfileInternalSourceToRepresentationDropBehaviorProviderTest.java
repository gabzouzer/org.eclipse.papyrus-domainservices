/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ProfileInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link ProfileInternalSourceToRepresentationDropBehaviorProvider}.
 * 
 * @author fbarbin
 *
 */
public class ProfileInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {


    /**
     * Test dropping a {@link Class}.
     */
    @Test
    public void testClassDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        org.eclipse.uml2.uml.Class clazz = createIn(org.eclipse.uml2.uml.Class.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(clazz));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(clazz, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(clazz));
        assertTrue(newContainer.getPackagedElements().contains(clazz));
    }

    /**
     * Test dropping a {@link Comment}.
     */
    @Test
    public void testCommentDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        Comment comment = createIn(Comment.class, oldContainer);

        assertTrue(oldContainer.getOwnedComments().contains(comment));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(comment, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getOwnedComments().contains(comment));
        assertTrue(newContainer.getOwnedComments().contains(comment));
    }

    /**
     * Test dropping a {@link Constraint}.
     */
    @Test
    public void testConstraintDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        Constraint constraint = createIn(Constraint.class, oldContainer);

        assertTrue(oldContainer.getOwnedRules().contains(constraint));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(constraint, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getOwnedRules().contains(constraint));
        assertTrue(newContainer.getOwnedRules().contains(constraint));

    }
    
    /**
     * Test dropping a {@link DataType}.
     */
    @Test
    public void testDataTypeDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        DataType dataType = createIn(DataType.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(dataType));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(dataType, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(dataType));
        assertTrue(newContainer.getPackagedElements().contains(dataType));
    }

    /**
     * Test dropping a {@link ElementImport}.
     */
    @Test
    public void testElementImportDrop() {
        Profile oldContainer = create(Profile.class);
        Profile newContainer = create(Profile.class);
        ElementImport elementImport = createIn(ElementImport.class, oldContainer);

        assertTrue(oldContainer.getElementImports().contains(elementImport));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(elementImport, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getElementImports().contains(elementImport));
        assertTrue(newContainer.getElementImports().contains(elementImport));
    }

    /**
     * Test dropping a {@link Enumeration}.
     */
    @Test
    public void testEnumerationDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        Enumeration enumeration = createIn(Enumeration.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(enumeration));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(enumeration, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(enumeration));
        assertTrue(newContainer.getPackagedElements().contains(enumeration));
    }

    /**
     * Test dropping a {@link EnumerationLiteral}.
     */
    @Test
    public void testEnumerationLiteralDrop() {
        Enumeration oldContainer = create(Enumeration.class);
        Enumeration newContainer = create(Enumeration.class);
        EnumerationLiteral enumerationLiteral = createIn(EnumerationLiteral.class, oldContainer);

        assertTrue(oldContainer.getOwnedLiterals().contains(enumerationLiteral));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(enumerationLiteral, oldContainer,
                newContainer, getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getOwnedLiterals().contains(enumerationLiteral));
        assertTrue(newContainer.getOwnedLiterals().contains(enumerationLiteral));
    }

    /**
     * Test dropping a {@link Operation}.
     */
    @Test
    public void testOperationDrop() {
        org.eclipse.uml2.uml.Class oldContainer = create(org.eclipse.uml2.uml.Class.class);
        org.eclipse.uml2.uml.Class newContainer = create(org.eclipse.uml2.uml.Class.class);
        Operation operation = createIn(Operation.class, oldContainer);

        assertTrue(oldContainer.getOwnedOperations().contains(operation));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(operation, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getOwnedOperations().contains(operation));
        assertTrue(newContainer.getOwnedOperations().contains(operation));
    }

    /**
     * Test dropping a {@link Package}.
     */
    @Test
    public void testPackageDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        Package pack = createIn(Package.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(pack));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(pack, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(pack));
        assertTrue(newContainer.getPackagedElements().contains(pack));

    }

    /**
     * Test dropping a {@link PrimitiveType}.
     */
    @Test
    public void testPrimitiveTypeDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        PrimitiveType primitiveType = createIn(PrimitiveType.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(primitiveType));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(primitiveType, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(primitiveType));
        assertTrue(newContainer.getPackagedElements().contains(primitiveType));
    }

    /**
     * Test dropping a {@link Profile}.
     */
    @Test
    public void testProfileDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        Profile profile = createIn(Profile.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(profile));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(profile, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(profile));
        assertTrue(newContainer.getPackagedElements().contains(profile));
    }

    /**
     * Test dropping a {@link Property}.
     */
    @Test
    public void testPropertyDrop() {
        org.eclipse.uml2.uml.Class oldContainer = create(org.eclipse.uml2.uml.Class.class);
        org.eclipse.uml2.uml.Class newContainer = create(org.eclipse.uml2.uml.Class.class);
        Property property = createIn(Property.class, oldContainer);

        assertTrue(oldContainer.getOwnedAttributes().contains(property));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(property, oldContainer,
                newContainer, getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getOwnedAttributes().contains(property));
        assertTrue(newContainer.getOwnedAttributes().contains(property));
    }

    /**
     * Test dropping a {@link Stereotype}.
     */
    @Test
    public void testStereotypeDrop() {
        Package oldContainer = create(Package.class);
        Package newContainer = create(Package.class);
        Stereotype stereotype = createIn(Stereotype.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(stereotype));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(stereotype, oldContainer, newContainer,
                getCrossRef(), getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(stereotype));
        assertTrue(newContainer.getPackagedElements().contains(stereotype));
    }

}

/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectionTargetCheckerTest extends AbstractUMLTest {

    private static final String OWNED_ATTRIBUTE = "ownedAttribute";

    private ElementDomainBasedEdgeReconnectionTargetChecker elementDomainBasedEdgeReconnectionTargetChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementDomainBasedEdgeReconnectionTargetChecker = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> true,
                new MockedViewQuerier());
    }

    /**
     * Check that the reconnect target is authorized when we try to reconnect an
     * association from a class to an other class (target property is owned by the
     * association).
     */
    @Test
    public void testCanReconnectAssociationTargetWithPropertyOwnedByAssociation() {
        Model model = create(Model.class);
        UseCase sourceUseCase = createIn(UseCase.class, model);
        UseCase oldTargetUseCase = createIn(UseCase.class, model);
        UseCase newTargetUseCase = createIn(UseCase.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, sourceUseCase, oldTargetUseCase, null, null,
                null);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, oldTargetUseCase,
                newTargetUseCase, null, null);
        assertTrue(status.isValid());

        Comment newTargetComment = createIn(Comment.class, model);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, oldTargetUseCase,
                newTargetComment, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect target is authorized when we try to reconnect an
     * association from a class to an other classifier which can contain attribute
     * (target property is not owned by the association).
     */
    @Test
    public void testCanReconnectAssociationTargetWithPropertyNotOwnedByAssociation() {
        Model model = create(Model.class);
        Class classSource = createIn(Class.class, model);
        Class classOldTarget = createIn(Class.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);

        // change owner of target Property
        Property targetProperty = association.getMemberEnds().get(1);
        classOldTarget.getOwnedAttributes().add(targetProperty);
        Class classNewTarget = createIn(Class.class, model);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, classOldTarget,
                classNewTarget, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Check that the reconnect target is not authorized when we try to reconnect an
     * association from a class to an other classifier which cannot contain
     * attribute (target property is not owned by the association).
     */
    @Test
    public void testCannotReconnectAssociationTargetWithPropertyNotOwnedByAssociation() {
        Model model = create(Model.class);
        Class classSource = createIn(Class.class, model);
        Class classOldTarget = createIn(Class.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);

        // change owner of target Property
        Property targetProperty = association.getMemberEnds().get(1);
        classOldTarget.getOwnedAttributes().add(targetProperty);
        Actor actorNewTarget = createIn(Actor.class, model);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, classOldTarget,
                actorNewTarget, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * from a port view to its self.
     */
    @Test
    public void testCanReconnectConnectorFromPortOnItself() {
        // create semantic elements
        Class clazz = create(Class.class);
        Port newPortTarget = createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Port oldPortTarget = createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = createIn(Connector.class, clazz);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPort = classNode.addChildren(newPortTarget);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldPortTarget, newPortTarget, visualPort, visualPort);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot connect a port to itself.", canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a port view and a property view with the port as border node.
     */
    @Test
    public void testCanReconnectConnectorFromPortOnItsPropertyContainerNode() {
        // create semantic elements
        Class type1 = create(Class.class);
        Property newPropertyTarget = create(Property.class);
        newPropertyTarget.setType(type1);
        Port sourcePort = createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Port oldPortTarget = createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Connector connector = create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode visualNewPropertyTarget = visualTree.addChildren(newPropertyTarget);
        VisualNode visualSourcePort = visualNewPropertyTarget.addBorderNode(sourcePort);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldPortTarget, newPropertyTarget, visualSourcePort, visualNewPropertyTarget);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot create a connector from a view representing a Part to its own Port (or the opposite).",
                canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a property view contained by an other one.
     */
    @Test
    public void testCanReconnectConnectorFromPropertyContainedInAnotherOne() {
        // create semantic elements
        Class type1 = create(Class.class);
        Class clazz = create(Class.class);
        Property oldPropertyTarget = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        oldPropertyTarget.setType(type1);
        Property propertySource = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        propertySource.setType(type1);
        Property newPropertyTarget = createIn(Property.class, type1, OWNED_ATTRIBUTE);
        Connector connector = create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertySource = classNode.addChildren(propertySource);
        VisualNode visualNewPropertyTarget = visualPropertySource.addChildren(newPropertyTarget);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldPropertyTarget, newPropertyTarget, visualPropertySource, visualNewPropertyTarget);
        assertFalse(canReconnectStatus.isValid());
        assertEquals(
                "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.",
                canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is authorized when we try to reconnect a connector
     * between two different properties not contained in each other.
     */
    @Test
    public void testCanReconnectConnectorFrom2DifferentProperty() {
        // create semantic elements
        Class clazz = create(Class.class);
        Property propertySource = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property propertyTarget = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property oldProperyTarget = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertySource = classNode.addChildren(propertySource);
        VisualNode visualPropertyTarget = classNode.addChildren(propertyTarget);

        // check reconnect is authorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldProperyTarget, propertyTarget, visualPropertySource, visualPropertyTarget);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Default reconnection for {@link Extend}.
     */
    @Test
    public void testCanReconnectExtendTarget() {
        // create semantic elements
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        extend.setExtension(source);
        extend.setExtendedCase(target);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extend, target, newTarget,
                null, null);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extend, target, source, null, null);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = create(Package.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extend, target, newTargetPackage, null,
                null);
        assertFalse(status.isValid());
    }

    /**
     * Test can reconnect the {@link Extension} target.
     */
    @Test
    public void testCanReconnectExtensionTarget() {
        Stereotype stereotype = create(Stereotype.class);
        PrimitiveType primitiveType = create(PrimitiveType.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Extension extension = create(Extension.class);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        CheckStatus canReconnect = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extension, class1,
                class2, null, null);
        assertTrue(canReconnect.isValid());
        canReconnect = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extension, class1,
                primitiveType, null, null);
        assertFalse(canReconnect.isValid());
    }

    /**
     * Default reconnection for PackageMerge.
     */
    @Test
    public void testCanReconnectPackageMergeTarget() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(merge, packTarget,
                packTarget2, null, null);
        assertTrue(status.isValid());

        // can't reconnect on non Package
        Actor errorTarget = create(Actor.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(merge, packTarget, errorTarget, null,
                null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for PackageImport.
     */
    @Test
    public void testCanReconnectPackageImportTarget() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(packImport, packTarget,
                packTarget2, null, null);
        assertTrue(status.isValid());

        // can reconnect on NameSpace
        Comment errorTarget = create(Comment.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(packImport, packTarget, errorTarget, null,
                null);
        assertFalse(status.isValid());
    }

    /**
     * Test reconnect target with null target => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNullTarget() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2, null,
                null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect target with no namedElement => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNoNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Comment comment = create(Comment.class);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2,
                comment, null, null);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Dependency target can only be reconnected to a non null NamedElement.",
                canCreateStatus.getMessage());
    }

    /**
     * Test reconnect target with a named element => reconnection is authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Class c3 = create(Class.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2, c3,
                null, null);
        assertTrue(canCreateStatus.isValid());
    }

    @Test
    public void testCanReconnectTransitionTarget() {
        State state1 = create(State.class);
        Pseudostate source = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate target1 = createIn(Pseudostate.class, region);
        Pseudostate target2 = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source);
        transition.setTarget(target1);

        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(transition, target1,
                target2, null, null);
        assertTrue(canCreateStatus.isValid());

        canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(transition, target1, region,
                null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Default reconnection for Generalization.
     */
    @Test
    public void testCanReconnectGeneralizationTarget() {
        // create semantic elements
        Generalization generalization = create(Generalization.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target,
                newTarget, null, null);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target, source, null,
                null);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = create(Package.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target, newTargetPackage,
                null, null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Substitution}.
     */
    @Test
    public void testCanReconnectSubstitutionTarget() {
        // create semantic elements
        Substitution substitution = create(Substitution.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        substitution.setSubstitutingClassifier(source);
        substitution.setContract(target);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target,
                newTarget, null, null);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target, source, null, null);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = create(Package.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target, newTargetPackage,
                null, null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Usage}.
     */
    @Test
    public void testCanReconnectUsageTarget() {
        // create semantic elements
        Usage usage = create(Usage.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        usage.getClients().add(source);
        usage.getSuppliers().add(target);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on NamedElement
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(usage, target, newTarget,
                null, null);
        assertTrue(status.isValid());

        // test reconnection on non NamedElement
        Comment newTargetComment = create(Comment.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(usage, target, newTargetComment, null,
                null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Include}.
     */
    @Test
    public void testCanReconnectIncludeTarget() {
        // create semantic elements
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newTarget = create(UseCase.class);

        // check reconnection on UseCase
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, newTarget,
                null, null);
        assertTrue(status.isValid());

        // cannot reconnect on the source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, source, null, null);
        assertFalse(status.isValid());

        // cannot reconnect on non {@link UseCase}
        Actor newErrorTarget = create(Actor.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, newErrorTarget, null,
                null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Manifestation}.
     */
    @Test
    public void testCanReconnectManifestationTarget() {
        Class target = create(Class.class);
        Class newTarget = create(Class.class);
        Include include = create(Include.class);
        Manifestation manifestation = create(Manifestation.class);
        manifestation.setUtilizedElement(target);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false,
                new MockedViewQuerier()).canReconnect(manifestation, target, include, null, null);
        assertFalse(canCreateStatus.isValid(), "the target should be a PackageableElement");
        canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(manifestation, target, newTarget, null, null);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Default reconnection for {@link InformationFlow}.
     */
    @Test
    public void testCanReconnectInformationFlowTarget() {
        Class target = create(Class.class);
        Class newTarget = create(Class.class);
        Package package1 = create(Package.class);
        Comment comment = create(Comment.class);
        InformationFlow informationFlow = create(InformationFlow.class);
        informationFlow.getInformationTargets().add(target);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false,
                new MockedViewQuerier()).canReconnect(informationFlow, target, newTarget, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow is not contained in a Package.");
        package1.getPackagedElements().add(informationFlow);
        canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, target, newTarget, null, null);
        assertTrue(canCreateStatus.isValid());
        canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, target, comment, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow new target should be a NamedElement");
    }
}

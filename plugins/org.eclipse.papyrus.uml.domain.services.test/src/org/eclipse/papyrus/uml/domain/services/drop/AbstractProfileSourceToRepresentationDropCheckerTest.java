/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.jupiter.api.Test;

/**
 * Abstract Test class for ProfileExternalSourceToRepresentationDropCheckerTest
 * and ProfileInternalSourceToRepresentationDropCheckerTest.
 * 
 * @author fbarbin
 *
 */
public abstract class AbstractProfileSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    /**
     * Test dropping a {@link Class}.
     */
    @Test
    public void testClassDrop() {
        Package pack = create(Package.class);
        Class clazz = create(Class.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(clazz, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(clazz, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(clazz, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(clazz, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment}.
     */
    @Test
    public void testCommentDrop() {
        Package pack = create(Package.class);
        Comment comment = create(Comment.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(comment, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(comment, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(comment, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint}.
     */
    @Test
    public void testConstraintDrop() {
        Package pack = create(Package.class);
        Constraint constraint = create(Constraint.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(constraint, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(constraint, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(constraint, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link DataType}.
     */
    @Test
    public void testDataTypeDrop() {
        Package pack = create(Package.class);
        DataType dataType = create(DataType.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(dataType, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(dataType, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(dataType, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(dataType, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Enumeration}.
     */
    @Test
    public void testEnumerationDrop() {
        Package pack = create(Package.class);
        Enumeration enumeration = create(Enumeration.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(enumeration, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(enumeration, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(enumeration, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(enumeration, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link EnumerationLiteral}.
     */
    @Test
    public void testEnumerationLiteralDrop() {
        Enumeration enumeration = create(Enumeration.class);
        EnumerationLiteral enumerationLiteral = create(EnumerationLiteral.class);

        // authorized D&D on Enumeration
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(enumerationLiteral, enumeration);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(enumerationLiteral, clazz2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link ElementImport}.
     */
    @Test
    public void testElementImportDrop() {
        Profile profile = create(Profile.class);
        ElementImport elementImport = create(ElementImport.class);

        // authorized D&D on Profile
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(elementImport, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(elementImport, clazz2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Operation}.
     */
    @Test
    public void testOperationDrop() {
        Class class1 = create(Class.class);
        Operation operation = create(Operation.class);

        // authorized D&D on Class
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(operation, class1);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on DataType
        DataType dataType = create(DataType.class);
        canDragAndDropStatus = this.canDragAndDrop(operation, dataType);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Enumeration
        Enumeration enumeration = create(Enumeration.class);
        canDragAndDropStatus = this.canDragAndDrop(operation, enumeration);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(operation, profile);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Package}.
     */
    @Test
    public void testPackageDrop() {
        Package pack = create(Package.class);
        Package pack2 = create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(pack, pack2);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(pack, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(pack, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link PrimitiveType}.
     */
    @Test
    public void testPrimitiveTypeDrop() {
        Package pack = create(Package.class);
        PrimitiveType primitiveType = create(PrimitiveType.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(primitiveType, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(primitiveType, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(primitiveType, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(primitiveType, stereotype);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Profile}.
     */
    @Test
    public void testProfileDrop() {
        Package pack = create(Package.class);
        Profile profile = create(Profile.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(profile, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile2 = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(profile, profile2);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(profile, clazz2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property}.
     */
    @Test
    public void testPropertyDrop() {
        Class class1 = create(Class.class);
        Property property = create(Property.class);

        // authorized D&D on Class
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(property, class1);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on DataType
        DataType dataType = create(DataType.class);
        canDragAndDropStatus = this.canDragAndDrop(property, dataType);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Enumeration
        Enumeration enumeration = create(Enumeration.class);
        canDragAndDropStatus = this.canDragAndDrop(property, enumeration);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(property, profile);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Stereotype}.
     */
    @Test
    public void testStereotypeDrop() {
        Package pack = create(Package.class);
        Stereotype stereotype = create(Stereotype.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.canDragAndDrop(stereotype, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Profile
        Profile profile = create(Profile.class);
        canDragAndDropStatus = this.canDragAndDrop(stereotype, profile);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Class
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = this.canDragAndDrop(stereotype, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Stereotype
        Stereotype stereotype2 = create(Stereotype.class);
        canDragAndDropStatus = this.canDragAndDrop(stereotype, stereotype2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    protected abstract CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer);
}

/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.UseCaseExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link UseCaseExternalSourceToRepresentationDropChecker}.
 * 
 * @author Jessy Mallet
 *
 */
public class UseCaseExternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private UseCaseExternalSourceToRepresentationDropChecker useCaseExternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        useCaseExternalSourceToRepresentationDropChecker = new UseCaseExternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Actor}.
     */
    @Test
    public void testActorDrop() {
        Actor actor = create(Actor.class);
        Package pack = create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(actor, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(actor, clazz);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Package}.
     */
    @Test
    public void testPackageDrop() {
        Package pack = create(Package.class);
        Package pack2 = create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(pack, pack2);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(pack, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment}.
     */
    @Test
    public void testCommentDrop() {
        Package pack = create(Package.class);
        Comment comment = create(Comment.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(comment,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(comment, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(comment, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint}.
     */
    @Test
    public void testConstraintDrop() {
        Package pack = create(Package.class);
        Constraint constraint = create(Constraint.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(constraint,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(constraint, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(constraint, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link UseCase}.
     */
    @Test
    public void testUseCaseDrop() {
        Package pack = create(Package.class);
        UseCase useCase = create(UseCase.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(useCase,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(useCase, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(useCase, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class}.
     */
    @Test
    public void testClassDrop() {
        Package pack = create(Package.class);
        Class clazz = create(Class.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Component}.
     */
    @Test
    public void testComponentDrop() {
        Package pack = create(Package.class);
        Component component = create(Component.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(component,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(component, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(component, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity}.
     */
    @Test
    public void testActivityDrop() {
        Package pack = create(Package.class);
        Activity activity = create(Activity.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(activity,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(activity, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(activity, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction}.
     */
    @Test
    public void testInteractionDrop() {
        Package pack = create(Package.class);
        Interaction interaction = create(Interaction.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(interaction,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(interaction, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(interaction, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine}.
     */
    @Test
    public void testStateMachineDrop() {
        Package pack = create(Package.class);
        StateMachine stateMachine = create(StateMachine.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

}

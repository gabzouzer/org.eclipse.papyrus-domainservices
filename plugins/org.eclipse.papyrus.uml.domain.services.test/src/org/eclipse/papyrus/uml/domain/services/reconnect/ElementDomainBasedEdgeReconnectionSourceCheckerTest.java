/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectionSourceCheckerTest extends AbstractUMLTest {

    private static final String OWNED_ATTRIBUTE = "ownedAttribute";

    private ElementDomainBasedEdgeReconnectionSourceChecker elementDomainBasedEdgeReconnectionSourceChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementDomainBasedEdgeReconnectionSourceChecker = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> true,
                new MockedViewQuerier());
    }

    /**
     * Check that the reconnect source is authorized when we try to reconnect an
     * association from a class to an other classifier (source property is owned by
     * the association).
     */
    @Test
    public void testCanReconnectAssociationSourceWithPropertyOwnedByAssociation() {
        Model model = create(Model.class);
        UseCase sourceUseCase = createIn(UseCase.class, model);
        UseCase targetUseCase = createIn(UseCase.class, model);
        UseCase newSourceUseCase = createIn(UseCase.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, sourceUseCase, targetUseCase, null, null, null);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, sourceUseCase,
                newSourceUseCase, null, null);
        assertTrue(status.isValid());

        Comment newSourceComment = createIn(Comment.class, model);
        status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, sourceUseCase,
                newSourceComment, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect source is authorized when we try to reconnect an
     * association from a class to an other classifier which can contain attribute
     * (source property is not owned by the association).
     */
    @Test
    public void testCanReconnectAssociationSourceWithPropertyNotOwnedByAssociation() {
        Model model = create(Model.class);
        Class classOldSource = createIn(Class.class, model);
        Class classTarget = createIn(Class.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);

        // change owner of source Property
        Property sourceProperty = association.getMemberEnds().get(0);
        classOldSource.getOwnedAttributes().add(sourceProperty);
        Class classNewSource = createIn(Class.class, model);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, classOldSource,
                classNewSource, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Check that the reconnect source is not authorized when we try to reconnect an
     * association from a class to an other classifier which cannot contain
     * attribute (source property is not owned by the association).
     */
    @Test
    public void testCannotReconnectAssociationSourceWithPropertyNotOwnedByAssociation() {
        Model model = create(Model.class);
        Class classOldSource = createIn(Class.class, model);
        Class classTarget = createIn(Class.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);

        // change owner of source Property
        Property sourceProperty = association.getMemberEnds().get(0);
        classOldSource.getOwnedAttributes().add(sourceProperty);
        Actor actorNewSource = createIn(Actor.class, model);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, classOldSource,
                actorNewSource, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Default source reconnection for PackageMerge.
     */
    @Test
    public void testPackageMerge() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(merge, packSource,
                packSource2, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Default source reconnection for PackageImport.
     */
    @Test
    public void testPackageImport() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packImport, packSource,
                packSource2, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * from a port view to its self.
     */
    @Test
    public void testCanReconnectConnectorFromPortOnItself() {
        // create semantic elements
        Class clazz = create(Class.class);
        Port newPortSource = createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Port oldPortSource = createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = createIn(Connector.class, clazz);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPort = classNode.addChildren(newPortSource);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldPortSource, newPortSource, visualPort, visualPort);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot connect a port to itself.", canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a port view and a property view with the port as border node.
     */
    @Test
    public void testCanReconnectConnectorFromPortOnItsPropertyContainerNode() {
        // create semantic elements
        Class type1 = create(Class.class);
        Property newPropertySource = create(Property.class);
        newPropertySource.setType(type1);
        Port portTarget = createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Port oldPortSource = createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Connector connector = create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode newPropertyNode = visualTree.addChildren(newPropertySource);
        VisualNode visualTargetPort = newPropertyNode.addBorderNode(portTarget);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldPortSource, newPropertySource, newPropertyNode, visualTargetPort);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot create a connector from a view representing a Part to its own Port (or the opposite).",
                canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a property view contained by an other one.
     */
    @Test
    public void testCanReconnectConnectorFromPropertyContainedInAnotherOne() {
        // create semantic elements
        Class type1 = create(Class.class);
        Class clazz = create(Class.class);
        Property oldPropertySource = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        oldPropertySource.setType(type1);
        Property propertyTarget = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        propertyTarget.setType(type1);
        Property newPropertySource = createIn(Property.class, type1, OWNED_ATTRIBUTE);
        Connector connector = create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertyTarget = classNode.addChildren(propertyTarget);
        VisualNode visualNewPropertySource = visualPropertyTarget.addChildren(newPropertySource);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldPropertySource, newPropertySource, visualNewPropertySource, visualPropertyTarget);
        assertFalse(canReconnectStatus.isValid());
        assertEquals(
                "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.",
                canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is authorized when we try to reconnect a connector
     * between two different properties not contained in each other.
     */
    @Test
    public void testCanReconnectConnectorFrom2DifferentProperty() {
        // create semantic elements
        Class clazz = create(Class.class);
        Property propertySource = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property propertyTarget = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property oldProperySource = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertySource = classNode.addChildren(propertySource);
        VisualNode visualPropertyTarget = classNode.addChildren(propertyTarget);

        // check reconnect is authorized
        CheckStatus canReconnectStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldProperySource, propertySource, visualPropertySource, visualPropertyTarget);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Test reconnect source with null source => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNullSource() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1, null,
                null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with no namedElement => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNoNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Comment comment = create(Comment.class);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1,
                comment, null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with a named element => reconnection is authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Class c3 = create(Class.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1, c3,
                null, null);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Extend} source on a {@link UseCase}, on its target and
     * on a {@link Package}.
     */
    @Test
    public void testCanReconnectExtendSource() {
        // create semantic elements
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        extend.setExtension(source);
        extend.setExtendedCase(target);
        UseCase newSource = create(UseCase.class);
        Package errorSource = create(Package.class);

        // check creation is authorized on an other UseCase
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source, target, null,
                null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non UseCase
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source, errorSource,
                null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect invalid Extend
        source.getExtends().remove(extend);
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source, newSource, null,
                null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test can reconnect the {@link Extension} source.
     */
    @Test
    public void testCanReconnectExtensionSource() {
        Stereotype stereotype = create(Stereotype.class);
        Stereotype stereotype2 = create(Stereotype.class);
        PrimitiveType primitiveType = create(PrimitiveType.class);
        Class class1 = create(Class.class);
        Extension extension = create(Extension.class);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        CheckStatus canReconnect = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extension, stereotype, stereotype2, null,
                null);
        assertTrue(canReconnect.isValid());
        canReconnect = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extension, stereotype,
                primitiveType, null, null);
        assertFalse(canReconnect.isValid());
    }

    /**
     * Test reconnect {@link Extend} source on a non editable {@link UseCase}
     * source.
     */
    @Test
    public void testCanReconnectExtendSourceOnNoEditableUseCase() {
        // create semantic elements
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        extend.setExtension(source);
        extend.setExtendedCase(target);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(extend, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link PackageMerge} source on a {@link Package} and on an
     * {@link Actor}.
     */
    @Test
    public void testCanReconnectPackageMergeSource() {
        // create semantic elements
        PackageMerge packageMerge = create(PackageMerge.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageMerge.setReceivingPackage(source);
        packageMerge.setMergedPackage(target);
        Package newSource = create(Package.class);
        Actor errorSource = create(Actor.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageMerge, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on non Package
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageMerge, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link PackageMerge} source on a non editable {@link Package}
     * source.
     */
    @Test
    public void testCanReconnectPackageMergeSourceNoEditable() {
        // create semantic elements
        PackageMerge packageMerge = create(PackageMerge.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageMerge.setReceivingPackage(source);
        packageMerge.setMergedPackage(target);
        Package newSource = create(Package.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(packageMerge, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link PackageImport} source on a {@link Package} and on
     * {@link Comment}.
     */
    @Test
    public void testCanReconnectPackageImportSource() {
        // create semantic elements
        PackageImport packageImport = create(PackageImport.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageImport.setImportingNamespace(source);
        packageImport.setImportedPackage(target);
        Package newSource = create(Package.class);
        Comment errorSource = create(Comment.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageImport,
                source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can reconnect on NameSpace
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageImport, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link PackageImport} source on a non editable {@link Package}
     * source.
     */
    @Test
    public void testCanReconnectPackageImportSourceOnNoEditablePackage() {
        // create semantic elements
        PackageImport packageImport = create(PackageImport.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageImport.setImportingNamespace(source);
        packageImport.setImportedPackage(target);
        Package newSource = create(Package.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(packageImport, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Generalization} source on a {@link Classifier}, on its
     * target and on a {@link Package}.
     */
    @Test
    public void testCanReconnectGeneralizationSource() {
        // create semantic elements
        Generalization generalization = create(Generalization.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newSource = create(UseCase.class);
        Package errorSource = create(Package.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization,
                source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization, source, target,
                null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Classifier
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Generalization} source on a non editable
     * {@link Classifier} source.
     */
    @Test
    public void testCanReconnectGeneralizationSourceOnNoEditableClassifier() {
        // create semantic elements
        Generalization generalization = create(Generalization.class);
        Actor source = create(Actor.class);
        UseCase target = create(UseCase.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(generalization, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Substitution} source on a {@link Classifier}, on its
     * target and on a {@link Package}.
     */
    @Test
    public void testCanReconnectSubstitutionSource() {
        // create semantic elements
        Substitution substitution = create(Substitution.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        substitution.setSubstitutingClassifier(source);
        substitution.setContract(target);
        UseCase newSource = create(UseCase.class);
        Package errorSource = create(Package.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source, target,
                null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Classifier
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Substitution} source on a non editable
     * {@link Classifier} source.
     */
    @Test
    public void testCanReconnectSubstitutionSourceOnNoEditableClassifier() {
        // create semantic elements
        Substitution substitution = create(Substitution.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        substitution.setContract(source);
        substitution.setSubstitutingClassifier(target);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(substitution, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    @Test
    public void testCanReconnectTransitionSource() {
        State state1 = create(State.class);
        Pseudostate source1 = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate source2 = createIn(Pseudostate.class, region);
        Pseudostate target = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source1);
        transition.setTarget(target);

        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(transition, source1,
                source2, null, null);
        assertTrue(canCreateStatus.isValid());

        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(transition, source1, region,
                null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Usage} source on a {@link NamedElement}, on its target
     * and on a {@link Comment}.
     */
    @Test
    public void testCanReconnectUsageSource() {
        // create semantic elements
        Usage usage = create(Usage.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        usage.getClients().add(source);
        usage.getSuppliers().add(target);
        UseCase newSource = create(UseCase.class);
        Comment errorSource = create(Comment.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(usage, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on non NamedElement
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(usage, source, errorSource, null,
                null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Include} with a {@link UseCase}.
     */
    @Test
    public void testCanReconnectIncludeSourceWithUseCase() {
        // create semantic elements
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newSource = create(UseCase.class);
        Actor errorSource = create(Actor.class);

        // check reconnection is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source, target, null,
                null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Use Case
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source, errorSource,
                null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect source with a non editable {@link Include} .
     */
    @Test
    public void testCanReconnectIncludeSourceNoEditable() {
        // create semantic elements
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(include, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Default reconnection for {@link Manifestation}.
     */
    @Test
    public void testCanReconnectManifestationSource() {
        Class source = create(Class.class);
        Class newSource = create(Class.class);
        Comment comment = create(Comment.class);
        Manifestation manifestation = create(Manifestation.class);
        manifestation.getClients().add(source);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(manifestation, source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());
        canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(manifestation, source, comment, null, null);
        assertFalse(canCreateStatus.isValid(), "the Manifestation new source should be a NamedElement");
    }

    /**
     * Default reconnection for {@link InformationFlow}.
     */
    @Test
    public void testCanReconnectInformationFlowSource() {
        Class source = create(Class.class);
        Class newSource = create(Class.class);
        Package package1 = create(Package.class);
        Comment comment = create(Comment.class);
        InformationFlow informationFlow = create(InformationFlow.class);
        informationFlow.getInformationSources().add(source);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(informationFlow, source, newSource, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow is not contained in a Package.");
        package1.getPackagedElements().add(informationFlow);
        canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());
        canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, source, comment, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow new source should be a NamedElement");
    }
}
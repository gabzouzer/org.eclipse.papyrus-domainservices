/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompositeStructureExternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private CompositeStructureExternalSourceToRepresentationDropChecker compositeStructureExternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        compositeStructureExternalSourceToRepresentationDropChecker = new CompositeStructureExternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Class} on {@link Property} => authorized.
     */
    @Test
    public void testClassDropOnProperty() {
        Property property = create(Property.class);
        Class clazz = create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Class} => authorized.
     */
    @Test
    public void testClassDropOnClass() {
        Class classContainer = create(Class.class);
        Class clazz = create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Comment} => not authorized.
     */
    @Test
    public void testClassDropOnComment() {
        Comment comment = create(Comment.class);
        Class clazz = create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Model} => authorized.
     */
    @Test
    public void testClassDropOnModel() {
        Model model = create(Model.class);
        Class clazz = create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, model);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Property} => authorized.
     */
    @Test
    public void testCollaborationDropOnProperty() {
        Property property = create(Property.class);
        Collaboration collaboration = create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Class} => authorized.
     */
    @Test
    public void testCollaborationDropOnClass() {
        Class classContainer = create(Class.class);
        Collaboration collaboration = create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Comment} => not authorized.
     */
    @Test
    public void testCollaborationDropOnComment() {
        Comment comment = create(Comment.class);
        Collaboration collaboration = create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Property} => authorized.
     */
    @Test
    public void testActivityDropOnProperty() {
        Property property = create(Property.class);
        Activity activity = create(org.eclipse.uml2.uml.Activity.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Class} => authorized.
     */
    @Test
    public void testActivityDropOnClass() {
        Class classContainer = create(Class.class);
        Activity activity = create(org.eclipse.uml2.uml.Activity.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Comment} => not authorized.
     */
    @Test
    public void testActivityDropOnComment() {
        Comment comment = create(Comment.class);
        Activity activity = create(org.eclipse.uml2.uml.Activity.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on its {@link Classifier} container =>
     * authorized.
     */
    @Test
    public void testPropertyDropOnContainerClassifier() {
        Property property = create(Property.class);
        Class classContainer = create(Class.class);
        classContainer.getOwnedAttributes().add(property);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Classifier} which is not its
     * container => not authorized.
     */
    @Test
    public void testPropertyDropOnNotContainerClassifier() {
        Property property = create(Property.class);
        Class classContainer = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, classContainer);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} typed with its
     * {@link Classifier} container => authorized.
     */
    @Test
    public void testPropertyDropOnPropertyTypedWithContainerClassifier() {
        Property propertyToDnD = create(Property.class);
        Property propertyTarget = create(Property.class);
        Class classContainer = create(Class.class);
        classContainer.getOwnedAttributes().add(propertyToDnD);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(propertyToDnD, propertyTarget);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} not typed with its
     * {@link Classifier} container => not authorized.
     */
    @Test
    public void testPropertyDropOnPropertyTypedWithoutContainerClassifier() {
        Property propertyToDnD = create(Property.class);
        Property propertyTarget = create(Property.class);
        Class classContainer = create(Class.class);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(propertyToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} not typed => not
     * authorized.
     */
    @Test
    public void testPropertyDropOnPropertyNotTyped() {
        Property propertyToDnD = create(Property.class);
        Property propertyTarget = create(Property.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(propertyToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on its {@link Classifier} container =>
     * authorized.
     */
    @Test
    public void testPortDropOnContainerClassifier() {
        Port port = create(Port.class);
        Class classContainer = create(Class.class);
        classContainer.getOwnedAttributes().add(port);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link Classifier} which is not its container
     * => not authorized.
     */
    @Test
    public void testPortDropOnNotContainerClassifier() {
        Port port = create(Port.class);
        Class classContainer = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, classContainer);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link port} on {@link Property} typed with its
     * {@link Classifier} container => authorized.
     */
    @Test
    public void testPortDropOnPropertyTypedWithContainerClassifier() {
        Port port = create(Port.class);
        Property propertyTarget = create(Property.class);
        Class classContainer = create(Class.class);
        classContainer.getOwnedAttributes().add(port);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, propertyTarget);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link Property} not typed with its
     * {@link Classifier} container => not authorized.
     */
    @Test
    public void testPortDropOnPropertyTypedWithoutContainerClassifier() {
        Port portToDnD = create(Port.class);
        Property propertyTarget = create(Property.class);
        Class classContainer = create(Class.class);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(portToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link Property} not typed => not authorized.
     */
    @Test
    public void testPortDropOnPropertyNotTyped() {
        Port portToDnD = create(Port.class);
        Property propertyTarget = create(Property.class);

        CheckStatus canDragAndDropStatus = compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(portToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

}

/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.jupiter.api.Test;

public class DefaultNameProviderTests extends AbstractUMLTest {

    private static final String SEPARATOR = "_"; //$NON-NLS-1$

    private static final String BASE = "E_"; //$NON-NLS-1$

    private static final String META_CLASS1 = "metaClass1"; //$NON-NLS-1$

    private static final String STEREOTYPE1 = "stereotype1"; //$NON-NLS-1$

    private static final String CLASS_1 = "Class1"; //$NON-NLS-1$

    private ElementDefaultNameProvider provider = new ElementDefaultNameProvider();

    @Test
    public void testNothingGiven() {
        assertNull(this.provider.getDefaultName(null, (EObject) null));
    }

    @Test
    public void testNoContainerGiven() {
        assertEquals(CLASS_1, this.provider.getDefaultName(create(Class.class), (EObject) null));
    }

    @Test
    public void testExtension() {
        Extension extension = create(Extension.class);
        Extension extension2 = create(Extension.class);
        extension2.setName(BASE + STEREOTYPE1 + SEPARATOR + META_CLASS1 + "2"); //$NON-NLS-1$
        Extension extension3 = create(Extension.class);
        extension3.setName(BASE + STEREOTYPE1 + SEPARATOR + META_CLASS1 + "3"); //$NON-NLS-1$
        Stereotype stereotype = create(Stereotype.class);
        stereotype.setName(STEREOTYPE1);
        Class metaClass = create(Class.class);
        metaClass.setName(META_CLASS1);
        ElementDomainBasedEdgeInitializer initializer = new ElementDomainBasedEdgeInitializer();
        initializer.initialize(extension, stereotype, metaClass, null, null, null);
        assertEquals(BASE + STEREOTYPE1 + SEPARATOR + META_CLASS1 + "4", //$NON-NLS-1$
                this.provider.getDefaultName(extension, List.of(extension2, extension3)));
    }

    @Test
    public void testSomeNamedElement() {

        var pack = create(Package.class);

        Class class1 = create(Class.class);
        assertEquals(CLASS_1, this.provider.getDefaultName(class1, pack));

        // No do set the name if already contained and has a name
        pack.getPackagedElements().add(class1);
        String defaultNameC1 = this.provider.getDefaultName(class1, pack);
        assertEquals(CLASS_1, defaultNameC1);
        class1.setName(defaultNameC1);

        Class class2 = create(Class.class);
        String defaultNameC2 = this.provider.getDefaultName(class2, pack);
        assertEquals("Class2", defaultNameC2); //$NON-NLS-1$
        pack.getPackagedElements().add(class2);
        class2.setName(defaultNameC2);

        Component component1 = create(Component.class);
        assertEquals("Component1", this.provider.getDefaultName(component1, pack)); //$NON-NLS-1$
    }

    @Test
    public void testGapInRange() {
        var pack = create(Package.class);

        Class class55 = create(Class.class);
        class55.setName("Class55"); //$NON-NLS-1$
        pack.getPackagedElements().add(class55);

        Class class60 = create(Class.class);
        class55.setName("Class60"); //$NON-NLS-1$
        pack.getPackagedElements().add(class60);

        Class class61 = create(Class.class);
        assertEquals("Class61", this.provider.getDefaultName(class61, pack)); //$NON-NLS-1$
    }

    /**
     * Test the configuration for the Pseudostate.
     */
    @Test
    public void testPseudostateConfiguration() {
        var region = create(Region.class);
        var choice = createIn(Pseudostate.class, region);

        choice.setKind(PseudostateKind.CHOICE_LITERAL);
        var defaultName = this.provider.getDefaultName(choice, region);
        assertEquals("Choice1", defaultName);
        choice.setName(defaultName);

        var choice2 = createIn(Pseudostate.class, region);
        choice2.setKind(PseudostateKind.CHOICE_LITERAL);
        defaultName = this.provider.getDefaultName(choice2, region);
        assertEquals("Choice2", defaultName);
        choice2.setName(defaultName);

        var fork = createIn(Pseudostate.class, region);
        fork.setKind(PseudostateKind.FORK_LITERAL);
        assertEquals("Fork1", this.provider.getDefaultName(fork, region));
        assertEquals(PseudostateKind.FORK_LITERAL, fork.getKind());
    }

    /**
     * An InterfaceRealization should not have a default name.
     */
    @Test
    public void noDefaultNameForInterfaceRealization() {
        var pack = create(Package.class);
        var interfaceRealization = createIn(InterfaceRealization.class, pack);
        assertNull(this.provider.getDefaultName(interfaceRealization, pack));
    }
}

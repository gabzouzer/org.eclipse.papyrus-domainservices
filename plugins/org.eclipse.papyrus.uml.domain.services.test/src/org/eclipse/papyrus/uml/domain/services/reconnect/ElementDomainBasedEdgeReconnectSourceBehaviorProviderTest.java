/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;
public class ElementDomainBasedEdgeReconnectSourceBehaviorProviderTest extends AbstractUMLTest {

    /* Name class2 */
    private static final String CLASS2 = "class2";

    /* Name class1 */
    private static final String CLASS1 = "class1";

    private static final String STEREOTYPE = "stereotype";
    
    private static final String STEREOTYPE2 = "stereotype2";

    /**
     * Test the source reconnection for a connector on a property port.
     */
    @Test
    public void testConnectorSourceReconnectOnPropertyPort() {
        // Create semantic elements
        Class class1 = create(Class.class);
        Port oldSourcePort = createIn(Port.class, class1);
        Port newSourcePort = createIn(Port.class, class1);
        Port targetPort = createIn(Port.class, class1);
        Property property = create(Property.class);
        property.setType(class1);
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEndSource = createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(oldSourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(targetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode propertyNode = visualTree.addChildren(property);
        VisualNode visualNewSourcePort = propertyNode.addChildren(newSourcePort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(connector, oldSourcePort, newSourcePort, visualNewSourcePort);
        assertTrue(status.isValid());
        assertEquals(newSourcePort, connectorEndSource.getRole(),
                "The new source port should have changed after the reconnect.");
        assertEquals(property, connectorEndSource.getPartWithPort());
    }

    /**
     * Test the source reconnection for a connector on a classifier port. This test
     * aims to check whether the partWithPort attribute is set at null.
     */
    @Test
    public void testConnectorSourceReconnectOnClassifierPort() {
        // Create semantic elements
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Port oldSourcePort = createIn(Port.class, class1);
        Port newSourcePort = createIn(Port.class, class2);
        Port targetPort = createIn(Port.class, class1);
        Property property = create(Property.class);
        property.setType(class1);
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEndSource = createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(oldSourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(targetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode class2Node = visualTree.addChildren(class2);
        VisualNode visualNewSourcePort = class2Node.addChildren(newSourcePort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(connector, oldSourcePort, newSourcePort, visualNewSourcePort);
        assertTrue(status.isValid());
        assertEquals(newSourcePort, connectorEndSource.getRole(),
                "The new source port should have changed after the reconnect.");
        assertNull(connectorEndSource.getPartWithPort(), "The old part with port should have been removed");
    }

    /**
     * Test reconnect the {@link Extension} source.
     */
    @Test
    public void testExtensionSourceReconnect() {
        Profile profile = create(Profile.class);
        Stereotype stereotype = create(Stereotype.class);
        stereotype.setName(STEREOTYPE);
        Stereotype stereotype2 = create(Stereotype.class);
        stereotype2.setName(STEREOTYPE2);
        Class class1 = create(Class.class);
        class1.setName(CLASS1);
        Extension extension = createIn(Extension.class, profile);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(extension, extension.eContainer());
        assertEquals("E_stereotype_class11", extension.getName());
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(extension, stereotype, stereotype2, null);
        assertTrue(status.isValid());
        assertEquals(stereotype2, extension.getStereotype());
        assertEquals("E_stereotype2_class11", extension.getName());

        // now check that the name is unchanged if the user has customized it.
        extension.setName("customName");
        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(extension, stereotype2, stereotype, null);
        assertTrue(status.isValid());
        assertEquals(stereotype, extension.getStereotype());
        assertEquals("customName", extension.getName());
    }

    /**
     * Default source reconnection for PackageMerge.
     */
    @Test
    public void testPackageMergeSourceReconnect() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(merge, packSource, packSource2, null);
        assertTrue(status.isValid());
        assertEquals(packSource2, merge.getReceivingPackage());
        assertEquals(packSource2, merge.eContainer());
    }

    /**
     * Default source reconnection for InterfaceRealization.
     */
    @Test
    public void testInterfaceRealizationSourceReconnect() {

        Class classSource = create(Class.class);
        Class classSource2 = create(Class.class);
        Interface interfaceTarget = create(Interface.class);

        InterfaceRealization interfaceRealization = createIn(InterfaceRealization.class, classSource);
        interfaceRealization.setImplementingClassifier(classSource);
        interfaceRealization.setContract(interfaceTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(interfaceRealization, classSource, classSource2, null);
        assertTrue(status.isValid());
        assertEquals(classSource2, interfaceRealization.getImplementingClassifier());
        assertEquals(classSource2, interfaceRealization.eContainer());
    }

    /**
     * Default source reconnection for PackageMerge.
     */
    @Test
    public void testPackageImportSourceReconnect() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(packImport, packSource, packSource2, null);
        assertTrue(status.isValid());
        assertEquals(packSource2, packImport.getImportingNamespace());
        assertEquals(packSource2, packImport.eContainer());
    }

    /**
     * Test reconnect source with null dependency or null old source or null new
     * source.
     */
    @Test
    public void testDependencySourceReconnectWithNullArguments() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(null, c1, c2, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, c1, null, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, null, c1, null);

        assertFalse(status.isValid());

    }

    /**
     * Test reconnect source of dependency on an other source in the same package.
     * Expected result : source changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencySourceReconnectInSamePackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, c1, c3, null);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getClients().size());
        assertEquals(c3, dependency.getClients().get(0));
    }

    /**
     * Test reconnect source of dependency on an other source in a different
     * package. Expected result : source changed and the dependency changes of owner
     * package
     */
    @Test
    public void testDependencySourceReconnectInDifferentPackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        org.eclipse.uml2.uml.Package p2 = create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, c1, c3, null);

        assertTrue(status.isValid());
        assertEquals(p2, dependency.getOwner());
        assertEquals(1, dependency.getClients().size());
        assertEquals(c3, dependency.getClients().get(0));
    }

    /**
     * Test reconnect source of {@link Extend} on an other {@link UseCase} source.
     */
    @Test
    public void testExtendSourceReconnect() {
        UseCase source = create(UseCase.class);
        UseCase newSource = create(UseCase.class);

        Extend extend = create(Extend.class);
        extend.setExtension(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(extend, source, newSource, null);

        assertTrue(status.isValid());
        assertEquals(newSource, extend.getExtension());
    }

    @Test
    public void testTransitionSourceReconnect() {
        State state1 = create(State.class);
        Pseudostate source1 = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate source2 = createIn(Pseudostate.class, region);
        Pseudostate target = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source1);
        transition.setTarget(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(transition, source1, source2, null);

        assertTrue(status.isValid());
        assertEquals(region, transition.getOwner());
        assertEquals(source2, transition.getSource());
        assertEquals(target, transition.getTarget());
    }

    /**
     * Test reconnect source of association on an other source (source property is
     * owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyOwnedByAssociation() {
        Model model = create(Model.class);
        Class classOldSource = createIn(Class.class, model);
        classOldSource.setName(CLASS1);
        Class classTarget = createIn(Class.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);
        Class classNewSource = createIn(Class.class, model);
        classNewSource.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(association, classOldSource, classNewSource, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(2, association.getOwnedMembers().size());
        Property sourceProperty = association.getMemberEnds().get(0);
        assertEquals(classTarget, sourceProperty.getType());
        Property targetProperty = association.getMemberEnds().get(1);
        assertEquals(classNewSource, targetProperty.getType());
        assertEquals(classOldSource.getName(), targetProperty.getName());
    }

    /**
     * Test reconnect source of association on an other source (source property is
     * not owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyNotOwnedByAssociation() {
        Model model = create(Model.class);
        Class classOldSource = createIn(Class.class, model);
        classOldSource.setName(CLASS1);
        Class classTarget = createIn(Class.class, model);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);

        // change owner of source Property
        Property sourceProperty = association.getMemberEnds().get(0);
        classOldSource.getOwnedAttributes().add(sourceProperty);
        Class classNewSource = createIn(Class.class, model);
        classNewSource.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(association, classOldSource, classNewSource, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(1, association.getOwnedMembers().size());
        assertFalse(classOldSource.getOwnedAttributes().contains(sourceProperty));
        assertTrue(classNewSource.getOwnedAttributes().contains(sourceProperty));
        Property targetProperty = association.getMemberEnds().get(1);
        assertEquals(classNewSource, targetProperty.getType());
        assertEquals(classOldSource.getName(), targetProperty.getName());
    }

    @Test
    public void testGeneralizationSourceReconnect() {
        UseCase source = create(UseCase.class);
        UseCase source2 = create(UseCase.class);

        Generalization generalization = create(Generalization.class);
        generalization.setSpecific(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(generalization, source, source2, null);

        assertTrue(status.isValid());
        assertEquals(source2, generalization.getSpecific());
    }

    /**
     * Default source reconnection for {@link Include}.
     */
    @Test
    public void testIncludeSourceReconnect() {

        UseCase source = create(UseCase.class);
        UseCase source2 = create(UseCase.class);

        Include include = create(Include.class);
        include.setIncludingCase(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(include, source, source2, null);

        assertTrue(status.isValid());
        assertEquals(source2, include.getIncludingCase());
    }

    /**
     * Default source reconnection for {@link Substitution}.
     */
    @Test
    public void testSubstitutionSourceReconnect() {
        Class oldSource = create(Class.class);
        Class newSource = create(Class.class);
        Class target = create(Class.class);
        Substitution substitution = create(Substitution.class);
        target.getSubstitutions().add(substitution);
        new ElementDomainBasedEdgeInitializer().initialize(substitution, oldSource, target, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(substitution, oldSource, newSource, null);

        assertTrue(status.isValid());
        assertEquals(newSource, substitution.getSubstitutingClassifier());
        assertEquals(newSource, substitution.eContainer());
    }

    /**
     * Default source reconnection for {@link InformationFlow}.
     */
    @Test
    public void testInformationFlowSourceReconnect() {
        Package package1 = create(Package.class);
        Class target = createIn(Class.class, package1);
        Class source = createIn(Class.class, package1);
        Class newSource = createIn(Class.class, package1);
        InformationFlow informationFlow = createIn(InformationFlow.class, package1);
        new ElementDomainBasedEdgeInitializer().initialize(informationFlow, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(informationFlow, source, newSource, null);
        assertTrue(status.isValid());
        assertTrue(informationFlow.getInformationSources().size() == 1);
        assertTrue(informationFlow.getInformationSources().contains(newSource));
    }
}

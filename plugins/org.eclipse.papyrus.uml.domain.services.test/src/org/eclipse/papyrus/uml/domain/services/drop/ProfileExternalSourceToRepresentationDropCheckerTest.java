/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ProfileExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.junit.jupiter.api.BeforeEach;

/**
 * Test class for {@link ProfileExternalSourceToRepresentationDropChecker}.
 * 
 * @author fbarbin
 *
 */
public class ProfileExternalSourceToRepresentationDropCheckerTest
        extends AbstractProfileSourceToRepresentationDropCheckerTest {

    private ProfileExternalSourceToRepresentationDropChecker profileExternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        profileExternalSourceToRepresentationDropChecker = new ProfileExternalSourceToRepresentationDropChecker();
    }

    @Override
    protected CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return this.profileExternalSourceToRepresentationDropChecker.canDragAndDrop(elementToDrop,
                newSemanticContainer);
    }

}

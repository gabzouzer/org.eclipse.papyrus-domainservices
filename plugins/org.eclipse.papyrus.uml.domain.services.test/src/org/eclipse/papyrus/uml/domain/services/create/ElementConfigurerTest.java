/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.DurationInterval;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Interval;
import org.eclipse.uml2.uml.IntervalConstraint;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeInterval;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

public class ElementConfigurerTest extends AbstractUMLTest {

    private UMLFactory fact = UMLFactory.eINSTANCE;

    @Test
    public void testNullInput() {
        assertNull(new ElementConfigurer().configure(null, null));
    }

    @Test
    public void testNamedElement() {

        var pack = create(Package.class);
        var c1 = create(Class.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(c1, pack);

        assertEquals("Class1", c1.getName()); //$NON-NLS-1$
        pack.getPackagedElements().add(c1);

        var c2 = this.fact.createClass();
        elementInitializer.configure(c2, pack);
        // Create a second one
        assertEquals("Class2", c2.getName()); //$NON-NLS-1$

        var c17 = this.fact.createClass();
        c17.setName("Class17"); //$NON-NLS-1$
        pack.getPackagedElements().add(c17);

        var c18 = this.fact.createClass();
        elementInitializer.configure(c18, pack);
        c17.setName("Class18"); //$NON-NLS-1$
    }

    @Test
    public void testPortCreation() {

        var c1 = create(Class.class);
        var port = create(Port.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(port, c1);

        assertEquals("Port1", port.getName()); //$NON-NLS-1$
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port.getAggregation());
        c1.getOwnedPorts().add(port);

        var port2 = this.fact.createPort();
        elementInitializer.configure(port2, c1);

        // Create a second one
        assertEquals("Port2", port2.getName()); //$NON-NLS-1$
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port2.getAggregation());

    }

    @Test
    public void testConstraintCreation() {

        var m1 = create(Model.class);
        var constraint = create(Constraint.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(constraint, m1);

        assertEquals("Constraint1", constraint.getName()); //$NON-NLS-1$
        assertEquals("constraintSpec", constraint.getSpecification().getName()); //$NON-NLS-1$
        m1.getOwnedRules().add(constraint);

        var constraint2 = this.fact.createConstraint();
        elementInitializer.configure(constraint2, m1);

        // Create a second one
        assertEquals("Constraint2", constraint2.getName()); //$NON-NLS-1$
        assertEquals("constraintSpec", constraint2.getSpecification().getName()); //$NON-NLS-1$
    }

    /**
     * Test the configuration of a usage => It should not have a default name.
     */
    @Test
    public void testUsageConfiguration() {
        var pack = create(Package.class);
        var usage = create(Usage.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(usage, pack);

        assertNull(usage.getName());
    }

    /**
     * Test the configuration of a UseCase => If its owner is a Classifier, useCase
     * should be added to useCase feature (subject feature of useCase should be
     * automatically updated).
     */
    @Test
    public void testUseCaseConfiguration() {
        Activity activity = create(Activity.class);
        UseCase useCase = create(UseCase.class);
        activity.getOwnedUseCases().add(useCase);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(useCase, activity);

        assertTrue(activity.getUseCases().contains(useCase));
        assertTrue(useCase.getSubjects().contains(activity));

        UseCase useCase2 = create(UseCase.class);
        Model model = create(Model.class);
        model.getPackagedElements().add(useCase2);
        elementInitializer.configure(useCase2, model);
        assertTrue(useCase2.getSubjects().isEmpty());
    }

    /**
     * Test the configuration of a Property => If its owner is a Collaboration, the
     * property should be added to collaborationRoles feature.
     */
    @Test
    public void testPropertyConfiguration() {
        Collaboration collaboration = create(Collaboration.class);
        Property property = create(Property.class);
        collaboration.getOwnedAttributes().add(property);
        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(property, collaboration);

        assertTrue(collaboration.getOwnedAttributes().contains(property));
        assertTrue(collaboration.getCollaborationRoles().contains(property));

        Property property2 = create(Property.class);
        Class class1 = create(Class.class);
        class1.getOwnedAttributes().add(property2);
        elementInitializer.configure(property2, class1);
        assertTrue(class1.getOwnedAttributes().contains(property2));
    }

    @Test
    public void testExtensionEndConfiguration() {
        ExtensionEnd extensionEnd = create(ExtensionEnd.class);
        Class clazz = create(Class.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(extensionEnd, clazz);

        assertEquals(AggregationKind.COMPOSITE_LITERAL, extensionEnd.getAggregation());
    }

    /**
     * Tests the configuration of an InteractionOperand => It should have an
     * InteractionConstraint set as guard named "guard".
     */
    @Test
    public void testInteractioOperandConfiguration() {
        CombinedFragment combinedFragment = create(CombinedFragment.class);
        InteractionOperand interactionOperand = createIn(InteractionOperand.class, combinedFragment);

        assertNull(interactionOperand.getGuard());

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(interactionOperand, combinedFragment);

        assertNotNull(interactionOperand.getGuard());
        assertEquals("guard", interactionOperand.getGuard().getName());
    }

    /**
     * A TimeConstraint should be initialized with a TimeInterval as specification
     * and min/max features of this TimeInterval have also been initialized and
     * created under the root package.
     */
    @Test
    public void testTimeConstraintConfiguration() {
        Model model = create(Model.class);
        Interaction interaction = createIn(Interaction.class, model);
        TimeConstraint timeConstraint = create(TimeConstraint.class);
        interaction.getPreconditions().add(timeConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(timeConstraint, interaction);

        assertNotNull(timeConstraint.getSpecification());
        assertEquals("TimeInterval", timeConstraint.getSpecification().eClass().getName());
        TimeInterval timeInterval = (TimeInterval) timeConstraint.getSpecification();
        assertNotNull(timeInterval.getMin());
        assertNotNull(timeInterval.getMax());
        assertTrue(model.getPackagedElements().contains(timeInterval.getMin()));
        assertTrue(model.getPackagedElements().contains(timeInterval.getMax()));
    }

    /**
     * If there is no root package, the min/max features of the TimeInterval should
     * not be initialized.
     */
    @Test
    public void testTimeConstraintConfigurationWithoutModel() {
        Interaction interaction = create(Interaction.class);
        TimeConstraint timeConstraint = create(TimeConstraint.class);
        interaction.getPreconditions().add(timeConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(timeConstraint, interaction);

        assertNotNull(timeConstraint.getSpecification());
        assertEquals("TimeInterval", timeConstraint.getSpecification().eClass().getName());
        TimeInterval timeInterval = (TimeInterval) timeConstraint.getSpecification();
        assertNull(timeInterval.getMin());
        assertNull(timeInterval.getMax());
    }

    /**
     * A DurationConstraint should be initialized with a DurationInterval as
     * specification and min/max features of this DurationInterval have also been
     * initialized and created under the root package.
     */
    @Test
    public void testDurationConstraintConfiguration() {
        Model model = create(Model.class);
        Interaction interaction = createIn(Interaction.class, model);
        DurationConstraint durationConstraint = create(DurationConstraint.class);
        interaction.getPreconditions().add(durationConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(durationConstraint, interaction);

        assertNotNull(durationConstraint.getSpecification());
        assertEquals("DurationInterval", durationConstraint.getSpecification().eClass().getName());
        DurationInterval durationInterval = (DurationInterval) durationConstraint.getSpecification();
        assertNotNull(durationInterval.getMin());
        assertNotNull(durationInterval.getMax());
        assertTrue(model.getPackagedElements().contains(durationInterval.getMin()));
        assertTrue(model.getPackagedElements().contains(durationInterval.getMax()));
    }

    /**
     * If there is no root package, the min/max features of the DurationInterval
     * should not be initialized.
     */
    @Test
    public void testDurationConstraintConfigurationWithoutModel() {
        Interaction interaction = create(Interaction.class);
        DurationConstraint durationConstraint = create(DurationConstraint.class);
        interaction.getPreconditions().add(durationConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(durationConstraint, interaction);

        assertNotNull(durationConstraint.getSpecification());
        assertEquals("DurationInterval", durationConstraint.getSpecification().eClass().getName());
        DurationInterval durationInterval = (DurationInterval) durationConstraint.getSpecification();
        assertNull(durationInterval.getMin());
        assertNull(durationInterval.getMax());
    }

    /**
     * An IntervalConstraint should be initialized with an Interval as specification
     * and min/max features of this Interval have also been initialized and created
     * under the root package.
     */
    @Test
    public void testIntervalConstraintConfiguration() {
        Model model = create(Model.class);
        Interaction interaction = createIn(Interaction.class, model);
        IntervalConstraint intervalConstraint = create(IntervalConstraint.class);
        interaction.getPreconditions().add(intervalConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(intervalConstraint, interaction);

        assertNotNull(intervalConstraint.getSpecification());
        assertEquals("Interval", intervalConstraint.getSpecification().eClass().getName());
        Interval interval = (Interval) intervalConstraint.getSpecification();
        assertNotNull(interval.getMin());
        assertNotNull(interval.getMax());
        assertTrue(model.getPackagedElements().contains(interval.getMin()));
        assertTrue(model.getPackagedElements().contains(interval.getMax()));
    }

    /**
     * If there is no root package, the min/max features of the Interval should not
     * be initialized.
     */
    @Test
    public void testIntervalConstraintConfigurationWithoutModel() {
        Interaction interaction = create(Interaction.class);
        IntervalConstraint intervalConstraint = create(IntervalConstraint.class);
        interaction.getPreconditions().add(intervalConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(intervalConstraint, interaction);

        assertNotNull(intervalConstraint.getSpecification());
        assertEquals("Interval", intervalConstraint.getSpecification().eClass().getName());
        Interval interval = (Interval) intervalConstraint.getSpecification();
        assertNull(interval.getMin());
        assertNull(interval.getMax());
    }

}

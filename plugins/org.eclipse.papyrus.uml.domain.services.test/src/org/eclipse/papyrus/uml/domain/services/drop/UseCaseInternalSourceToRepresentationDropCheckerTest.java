/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.UseCaseInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link UseCaseInternalSourceToRepresentationDropChecker}.
 * 
 * @author Jessy Mallet
 *
 */
public class UseCaseInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private UseCaseInternalSourceToRepresentationDropChecker useCaseInternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        useCaseInternalSourceToRepresentationDropChecker = new UseCaseInternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Actor}.
     */
    @Test
    public void testActorDrop() {
        Actor actor = create(Actor.class);
        Package pack = create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(actor, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(actor, clazz);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Package}.
     */
    @Test
    public void testPackageDrop() {
        Package pack = create(Package.class);
        Package pack2 = create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(pack, pack2);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(pack, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment}.
     */
    @Test
    public void testCommentDrop() {
        Package pack = create(Package.class);
        Comment comment = create(Comment.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(comment,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(comment, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(comment, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint}.
     */
    @Test
    public void testConstraintDrop() {
        Package pack = create(Package.class);
        Constraint constraint = create(Constraint.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(constraint,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(constraint, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(constraint, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link UseCase}.
     */
    @Test
    public void testUseCaseDrop() {
        Package pack = create(Package.class);
        UseCase useCase = create(UseCase.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(useCase,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(useCase, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(useCase, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class}.
     */
    @Test
    public void testClassDrop() {
        Package pack = create(Package.class);
        Class clazz = create(Class.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Component}.
     */
    @Test
    public void testComponentDrop() {
        Package pack = create(Package.class);
        Component component = create(Component.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(component,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(component, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(component, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity}.
     */
    @Test
    public void testActivityDrop() {
        Package pack = create(Package.class);
        Activity activity = create(Activity.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(activity,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(activity, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(activity, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction}.
     */
    @Test
    public void testInteractionDrop() {
        Package pack = create(Package.class);
        Interaction interaction = create(Interaction.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(interaction,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(interaction, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(interaction, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine}.
     */
    @Test
    public void testStateMachineDrop() {
        Package pack = create(Package.class);
        StateMachine stateMachine = create(StateMachine.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = create(Class.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = create(Actor.class);
        canDragAndDropStatus = useCaseInternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

}

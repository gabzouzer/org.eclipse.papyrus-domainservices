/*****************************************************************************
 * Copyright (c) 2022 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  vince - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.services.ClassifierUtils;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.junit.jupiter.api.Test;

/**
 * This class tests {@link ClassifierUtils} services.
 * 
 * @author <a href="mailto:vincent.blain@obeosoft.com">Vincent BLAIN</a>
 */
class ClassifierUtilsTest extends AbstractUMLTest {

    /**
     * Check that the {@link Property} is added to ownedAttribute feature if the
     * {@link Classifier} is an {@link Artifact}.
     */
    @Test
    void testAddOwnedAttributeOnArtifact() {
        Artifact classifier = create(Artifact.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(classifier, prop);

        assertTrue(isAdded);
        assertEquals(prop, classifier.getOwnedAttributes().get(0));
    }

    /**
     * Check that the {@link Property} is added to ownedAttribute feature if the
     * {@link Classifier} is a {@link DataType}.
     */
    @Test
    void testAddOwnedAttributeOnDataType() {
        DataType classifier = create(DataType.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(classifier, prop);

        assertTrue(isAdded);
        assertEquals(prop, classifier.getOwnedAttributes().get(0));
    }

    /**
     * Check that the {@link Property} is added to ownedAttribute feature if the
     * {@link Classifier} is an {@link Interface}.
     */
    @Test
    void testAddOwnedAttributeOnInterface() {
        Interface classifier = create(Interface.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(classifier, prop);

        assertTrue(isAdded);
        assertEquals(prop, classifier.getOwnedAttributes().get(0));
    }

    /**
     * Check that the {@link Property} is added to ownedAttribute feature if the
     * {@link Classifier} is a {@link Signal}.
     */
    @Test
    void testAddOwnedAttributeOnSignal() {
        Signal classifier = create(Signal.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(classifier, prop);

        assertTrue(isAdded);
        assertEquals(prop, classifier.getOwnedAttributes().get(0));
    }

    /**
     * Check that the {@link Property} is added to ownedAttribute feature if the
     * {@link Classifier} is a {@link StructuredClassifier}.
     */
    @Test
    void testAddOwnedAttributeOnStructuredClassifier() {
        Collaboration classifier = create(Collaboration.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(classifier, prop);

        assertTrue(isAdded);
        assertEquals(prop, classifier.getOwnedAttributes().get(0));
    }

    /**
     * Check that the {@link Property} is added to ownedAttribute feature if the
     * {@link Classifier} is a {@link org.eclipse.uml2.uml.Class}.
     */
    @Test
    void testAddOwnedAttributeOnClass() {
        org.eclipse.uml2.uml.Class classifier = create(org.eclipse.uml2.uml.Class.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(classifier, prop);

        assertTrue(isAdded);
        assertEquals(prop, classifier.getOwnedAttributes().get(0));
    }

    /**
     * Check that the {@link Property} is not added for the default case.
     */
    @Test
    void testAddOwnedAttributeOnDefaultCase() {
        Association association = create(Association.class);
        Property prop = create(Property.class);

        Boolean isAdded = ClassifierUtils.addOwnedAttribute(association, prop);

        assertFalse(isAdded);
    }
}

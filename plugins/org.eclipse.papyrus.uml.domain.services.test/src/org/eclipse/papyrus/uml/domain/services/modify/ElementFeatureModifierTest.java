/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionConstraint;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Usage;
import org.junit.jupiter.api.Test;

public class ElementFeatureModifierTest extends AbstractUMLTest {

    private static final String COLLABORATION_ROLE_REF = "collaborationRole";
    private static final String FRAGMENT_REF = "fragment";
    private static final String AGGREGATION_REF = "aggregation";
    private static final String NAME_REF = "name";
    private static final String OPERAND_REF = "operand";
    private static final String TYPE_REF = "type";
    private static final String CLASS1 = "class1";
    private static final String ANNOTATED_ELEMENT = UMLPackage.eINSTANCE.getComment_AnnotatedElement().getName();

    /**
     * Test basic use case of setting an EAttribute.
     */
    @Test
    public void basicSetUnaryAttribute() {
        Class clazz = create(Class.class);
        Status status = buildElementFeatureModifier().setValue(clazz, NAME_REF, CLASS1);
        assertEquals(State.DONE, status.getState());
        assertEquals(CLASS1, clazz.getName());
    }

    /**
     * Check that a feature can be modified on a element that can't be edited.
     */
    @Test
    public void testUnEditableElement() {
        Class clazz = create(Class.class);
        Status status = new ElementFeatureModifier(getCrossRef(), e -> false).setValue(clazz, NAME_REF, CLASS1);
        assertEquals(State.FAILED, status.getState());
        assertNull(clazz.getName());
    }

    /**
     * Test basic use case of setting an unary EReference.
     */
    @Test
    public void basicSetUnaryReference() {
        Property prop = create(Property.class);
        Class type = create(Class.class);

        Status status = buildElementFeatureModifier().setValue(prop, TYPE_REF, type);
        assertEquals(State.DONE, status.getState());
        assertEquals(type, prop.getType());
    }

    /**
     * Test basic use case of adding a EObject a "many" EReference.
     */
    @Test
    public void basicAddReference() {
        Comment comment = create(Comment.class);
        Class type = create(Class.class);

        Status status = buildElementFeatureModifier().addValue(comment, ANNOTATED_ELEMENT, type);
        assertEquals(State.DONE, status.getState());
        assertTrue(comment.getAnnotatedElements().contains(type));
    }

    /**
     * Test basic use case of setting a EObject in a "many" EReference => The only
     * element left is set object.
     */
    @Test
    public void basicSetOnManyReference() {
        Comment comment = create(Comment.class);
        Class type = create(Class.class);
        Class type2 = create(Class.class);
        comment.getAnnotatedElements().add(type2);

        Status status = buildElementFeatureModifier().setValue(comment, ANNOTATED_ELEMENT, type);
        assertEquals(State.DONE, status.getState());
        assertEquals(List.of(type), comment.getAnnotatedElements());
    }

    /**
     * Test basic use case of removing a EObject a "many" EReference.
     */
    @Test
    public void basicRemoveReference() {
        Comment comment = create(Comment.class);
        Class type = create(Class.class);
        comment.getAnnotatedElements().add(type);

        Status status = buildElementFeatureModifier().removeValue(comment, ANNOTATED_ELEMENT, type);
        assertEquals(State.DONE, status.getState());
        assertFalse(comment.getAnnotatedElements().contains(type));
    }

    /**
     * Test setting a derived feature => Failure.
     */
    @Test
    public void basicSetDeriveFeature() {
        Class clazz = create(Class.class);
        Class superClazz = create(Class.class);
        Status status = buildElementFeatureModifier().setValue(clazz, "superClass", superClazz);
        assertEquals(State.FAILED, status.getState());
        assertFalse(clazz.getSuperClasses().contains(superClazz));
    }

    /**
     * Test setting feature which is not existing => Failure.
     */
    @Test
    public void basicSetNonExisitingFeature() {
        Class clazz = create(Class.class);
        Class superClazz = create(Class.class);
        Status status = buildElementFeatureModifier().setValue(clazz, "superMegaClass", superClazz);
        assertEquals(State.FAILED, status.getState());
    }

    private ElementFeatureModifier buildElementFeatureModifier() {
        return new ElementFeatureModifier(getCrossRef(), getEditableChecker());
    }

    /**
     * Test unsetting property type for a member end of an Association: the
     * Association should be removed.
     */
    @Test
    public void testUnsetPropertyTypeAssociationMemberEnd() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        memberEnds.get(0).setType(class1);
        memberEnds.get(1).setType(class2);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(model.getPackagedElements().isEmpty());
        assertNull(property1.getAssociation());
    }

    /**
     * Test unsetting port type for a member end of an Association: the Association
     * should not be removed.
     */
    @Test
    public void testUnsetPortTypeAssociationMemberEnd() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Association asso = create(Association.class);
        Port port1 = create(Port.class);
        Port port2 = create(Port.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(port1);
        memberEnds.add(port2);
        memberEnds.get(0).setType(class1);
        memberEnds.get(1).setType(class2);

        Status status = buildElementFeatureModifier().setValue(port1, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(port1, asso.getMemberEnds().get(0));
        assertNotNull(port1.getAssociation());
    }

    /**
     * Test setting property type without adding a member end to the association:
     * the Association should not be removed.
     */
    @Test
    public void testSetPropertyTypeWithoutAssociation() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);

        model.getPackagedElements().add(asso);
        property1.setType(class1);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, class2);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertNull(property1.getAssociation());
    }

    /**
     * Test setting the unary EReference "type" for a member end of an Association
     * with 2 member end. Expected result : the Association shouldn't be removed.
     */
    @Test
    public void testChangeTypeAssociationMemberEnd() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);

        Association asso = createIn(Association.class, model);
        Property property1 = create(Property.class);
        property1.setType(class1);
        Property property2 = create(Property.class);
        property2.setType(class1);

        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, class2);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(asso, property1.getAssociation());
        assertEquals(asso, property2.getAssociation());
    }

    /**
     * Tests that removing the type of a property removes also the linked
     * association.
     */
    @Test
    public void testUnsetTypeAssociationMemberEndWithRemoveAssociation() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);

        Association asso = createIn(Association.class, model);
        Property property1 = create(Property.class);
        property1.setType(class1);
        Property property2 = create(Property.class);
        property2.setType(class1);

        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(model.getPackagedElements().isEmpty());
    }

    /**
     * Test setting the "aggregation" feature without setting memberEnds to the
     * association. The Association should not be removed and AggregationKind should
     * remain unchanged.
     */
    @Test
    public void testSetPropertyAggregationWithoutAssociation() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        property2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF,
                AggregationKind.COMPOSITE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property2.getAggregation());
    }

    /**
     * Test setting the "aggregation" feature with memberEnds added to the
     * association. The Association should not be deleted but others AggregationKind
     * should be updated to NONE_LITERAL.
     */
    @Test
    public void testSetPropertyAggregationWithAssociationMemberEnds() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        property2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF,
                AggregationKind.COMPOSITE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.NONE_LITERAL, property2.getAggregation());

        status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF, AggregationKind.NONE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.NONE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.NONE_LITERAL, property2.getAggregation());
    }

    /**
     * Test setting the "aggregation" feature with NONE_LITERAL value. The
     * Association should not be removed and AggregationKind should remain
     * unchanged.
     */
    @Test
    public void testSetPropertyNONEAggregationWithAssociationMemberEnds() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        property2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF,
                AggregationKind.NONE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.NONE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property2.getAggregation());
    }

    /**
     * Test setting the "aggregation" feature for a Port. The Association should not
     * be removed and AggregationKind should remain unchanged.
     */
    @Test
    public void testSetPortAggregationWithAssociationMemberEnds() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Port port1 = create(Port.class);
        Port port2 = create(Port.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(port1);
        memberEnds.add(port2);
        port2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(port1, AGGREGATION_REF,
                AggregationKind.COMPOSITE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port1.getAggregation());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port2.getAggregation());
    }

    /**
     * Test setting the unary EReference "type" for a member end of an Association
     * with 2 member end. Expected result : the Association still exists.
     */
    @Test
    public void testUnsetTypeAssociationMemberEndWithNoRemoveAssociation() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);

        Association asso = createIn(Association.class, model);
        Property property1 = create(Property.class);
        property1.setType(class1);
        Property property2 = create(Property.class);
        property2.setType(class1);
        Property property3 = create(Property.class);
        property3.setType(class1);

        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        memberEnds.add(property3);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, class2);
        assertEquals(State.DONE, status.getState());
        assertEquals(1, model.getPackagedElements().size());
        assertEquals(asso, model.getPackagedElements().get(0));
    }

    /**
     * Tests changing the type of a CollaborationUse. The RoleBindings feature
     * should be cleared.
     */
    @Test
    public void testChangeTypeCollaborationUse() {
        Collaboration collaboration1 = create(Collaboration.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        collaborationUse.setType(collaboration1);
        Usage usage = create(Usage.class);
        Abstraction abstraction = create(Abstraction.class);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(usage);
        collaborationUse.getRoleBindings().add(abstraction);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Collaboration c2 = create(Collaboration.class);
        Status status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, c2);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaborationUse.getRoleBindings().isEmpty());

        // Set the same type again doesn't clear the roleBindings feature
        usage = create(Usage.class);
        abstraction = create(Abstraction.class);
        dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(usage);
        collaborationUse.getRoleBindings().add(abstraction);
        collaborationUse.getRoleBindings().add(dependency);
        status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, c2);
        assertEquals(State.DONE, status.getState());
        assertFalse(collaborationUse.getRoleBindings().isEmpty());
    }

    /**
     * Tests removing a roleBinding from a CollaborationUse.
     */
    @Test
    public void testRemoveRoleBindingCollaborationUse() {
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Status status = buildElementFeatureModifier().removeValue(collaborationUse, "roleBinding", dependency);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaborationUse.getRoleBindings().isEmpty());
    }

    /**
     * Tests setting null value to the type of a CollaborationUse. The RoleBindings
     * feature should be cleared.
     */
    @Test
    public void testChangeTypeCollaborationUseWithNullValue() {
        Collaboration collaboration = create(Collaboration.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        collaborationUse.setType(collaboration);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Status status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaborationUse.getRoleBindings().isEmpty());
    }

    /**
     * Tests setting a non-Collaboration instance to the type of a CollaborationUse.
     * It should failed without doing anything.
     */
    @Test
    public void testChangeTypeCollaborationUseWithNotCollaborationInstance() {
        Property property = create(Property.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Status status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, property);
        assertEquals(State.FAILED, status.getState());
        assertFalse(collaborationUse.getRoleBindings().isEmpty());
    }

    @Test
    public void testRemoveLastInteractionOperandInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        InteractionOperand interactionOperand = createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);

        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
        Status status = buildElementFeatureModifier().removeValue(combinedFragment, OPERAND_REF, interactionOperand);
        assertEquals(State.DONE, status.getState());
        assertTrue(combinedFragment.getOperands().isEmpty());
        assertTrue(rootInteraction.getFragments().isEmpty());
    }

    @Test
    public void testAddInteractionOperandInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        InteractionOperand interactionOperand = create(InteractionOperand.class);

        assertTrue(combinedFragment.getOperands().isEmpty());
        Status status = buildElementFeatureModifier().addValue(combinedFragment, OPERAND_REF, interactionOperand);
        assertEquals(State.DONE, status.getState());
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
    }

    @Test
    public void testRemoveNotLastInteractionOperandInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        InteractionOperand interactionOperand = createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);
        createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);

        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
        Status status = buildElementFeatureModifier().removeValue(combinedFragment, OPERAND_REF, interactionOperand);
        assertEquals(State.DONE, status.getState());
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
    }

    @Test
    public void testRemoveNonExistingOperandInCombinedFragment() {
        Interaction rootInteraction = create(Interaction.class);
        CombinedFragment combinedFragment = createIn(CombinedFragment.class, rootInteraction);
        createIn(InteractionOperand.class, combinedFragment, OPERAND_REF);
        InteractionOperand interactionOperand = create(InteractionOperand.class);

        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
        Status status = buildElementFeatureModifier().removeValue(combinedFragment, OPERAND_REF, interactionOperand);
        assertEquals(State.DONE, status.getState());
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());

        buildElementFeatureModifier().removeValue(combinedFragment, OPERAND_REF, rootInteraction);
        assertEquals(State.DONE, status.getState());
        assertFalse(rootInteraction.getFragments().isEmpty());
        assertFalse(combinedFragment.getOperands().isEmpty());
    }

    /**
     * Tests creating nested InteractionOperand under an InteractionOperand:
     * InteractionOperands are added to the CombinedFragment.
     */
    @Test
    public void testCreateNestedInteractionOperandUnderInteractionOperand() {
        CombinedFragment combinedFragment = create(CombinedFragment.class);
        InteractionOperand operand1 = create(InteractionOperand.class);
        combinedFragment.getOperands().add(operand1);
        InteractionOperand operand2 = create(InteractionOperand.class);

        Status status = buildElementFeatureModifier().addValue(operand1, FRAGMENT_REF, operand2);
        assertEquals(State.DONE, status.getState());
        assertTrue(operand1.getFragments().isEmpty());
        assertTrue(combinedFragment.getOperands().contains(operand2));
        assertEquals(2, combinedFragment.getOperands().size());
    }

    /**
     * Tests creating nested InteractionOperand under an Interaction:
     * InteractionOperands may be nested.
     */
    @Test
    public void testCreateNestedInteractionOperandUnderInteraction() {
        Interaction interaction = create(Interaction.class);
        InteractionOperand operand1 = create(InteractionOperand.class);
        interaction.getFragments().add(operand1);
        InteractionOperand operand2 = create(InteractionOperand.class);

        Status status = buildElementFeatureModifier().addValue(operand1, FRAGMENT_REF, operand2);
        assertEquals(State.DONE, status.getState());
        assertTrue(operand1.getFragments().contains(operand2));
        assertFalse(interaction.getFragments().contains(operand2));
        assertEquals(1, interaction.getFragments().size());
    }

    /**
     * Tests that any kind of InteractionFragment can be added to the fragments list
     * of an InteractionOperand.
     */
    @Test
    public void testAddInteractionToInteractionOperandFragment() {
        CombinedFragment combinedFragment = create(CombinedFragment.class);
        InteractionOperand operand = create(InteractionOperand.class);
        combinedFragment.getOperands().add(operand);
        Interaction interaction = create(Interaction.class);

        Status status = buildElementFeatureModifier().addValue(operand, FRAGMENT_REF, interaction);
        assertEquals(State.DONE, status.getState());
        assertTrue(operand.getFragments().contains(interaction));
        assertEquals(1, combinedFragment.getOperands().size());
    }

    /**
     * Test modifying another feature than "fragment" to ensure a good code
     * coverage.
     */
    @Test
    public void testModifyInteractionOperandGuard() {
        CombinedFragment combinedFragment = create(CombinedFragment.class);
        InteractionOperand operand = create(InteractionOperand.class);
        combinedFragment.getOperands().add(operand);
        InteractionConstraint guard = create(InteractionConstraint.class);

        Status status = buildElementFeatureModifier().addValue(operand, "guard", guard);
        assertEquals(State.DONE, status.getState());
        assertEquals(guard, operand.getGuard());
    }

    /**
     * Test removing a ConnectableElement from the collaborationRole feature of a
     * Collaboration element: If the ConnectableElement is referenced by a
     * Dependency (here usage1) which is contained by a CollaborationUse (with
     * roleBinding feature) and this CollaborationUse is typed with the modified
     * Collaboration, so the Dependency should be removed.
     */
    @Test
    public void testRemoveCollaborationRoleWithRelatedRoleBindings() {
        Class clazz = create(Class.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);
        clazz.getOwnedAttributes().add(property1);
        clazz.getOwnedAttributes().add(property2);

        Collaboration collaboration = create(Collaboration.class);
        collaboration.getCollaborationRoles().add(property1);
        collaboration.getCollaborationRoles().add(property2);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        collaborationUse.setType(collaboration);
        collaboration.getCollaborationUses().add(collaborationUse);
        Usage usage1 = create(Usage.class);
        usage1.getClients().add(property1);
        collaborationUse.getRoleBindings().add(usage1);
        Usage usage2 = create(Usage.class);
        collaborationUse.getRoleBindings().add(usage2);

        Status status = buildElementFeatureModifier().removeValue(collaboration, COLLABORATION_ROLE_REF, property1);

        assertEquals(State.DONE, status.getState());
        assertFalse(collaborationUse.getRoleBindings().contains(usage1));
        assertTrue(collaborationUse.getRoleBindings().contains(usage2));
        assertFalse(collaboration.getCollaborationRoles().contains(property1));
        assertTrue(collaboration.getCollaborationRoles().contains(property2));
        assertTrue(clazz.getOwnedAttributes().contains(property1));
        assertTrue(clazz.getOwnedAttributes().contains(property2));
        assertTrue(collaboration.getCollaborationUses().contains(collaborationUse));

        // Check that removing a non-referenced property doesn't affect roleBindings.
        status = buildElementFeatureModifier().removeValue(collaboration, COLLABORATION_ROLE_REF, property2);

        assertEquals(State.DONE, status.getState());
        assertFalse(collaborationUse.getRoleBindings().contains(usage1));
        assertTrue(collaborationUse.getRoleBindings().contains(usage2));
        assertFalse(collaboration.getCollaborationRoles().contains(property1));
        assertFalse(collaboration.getCollaborationRoles().contains(property2));
        assertTrue(clazz.getOwnedAttributes().contains(property1));
        assertTrue(clazz.getOwnedAttributes().contains(property2));
        assertTrue(collaboration.getCollaborationUses().contains(collaborationUse));
    }

    @Test
    public void testRemoveCollaborationRoleNullElt() {
        Collaboration collaboration = create(Collaboration.class);
        Property property = create(Property.class);
        collaboration.getCollaborationRoles().add(property);

        Status status = buildElementFeatureModifier().removeValue(collaboration, COLLABORATION_ROLE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaboration.getCollaborationRoles().contains(property));
    }

    @Test
    public void testAddCollaborationRole() {
        Collaboration collaboration = create(Collaboration.class);
        Property property = create(Property.class);

        Status status = buildElementFeatureModifier().addValue(collaboration, COLLABORATION_ROLE_REF, property);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaboration.getCollaborationRoles().contains(property));
    }
}

/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompositeStructureInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private CompositeStructureInternalSourceToRepresentationDropChecker compositeStructureInternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        compositeStructureInternalSourceToRepresentationDropChecker = new CompositeStructureInternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Property} on {@link Property} => not authorized.
     */
    @Test
    public void testPropertyDropOnProperty() {
        Property p1 = create(Property.class);
        Property p2 = create(Property.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, p2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on typed {@link Property} => authorized.
     */
    @Test
    public void testPropertyDropOnTypedProperty() {
        Property p1 = create(Property.class);
        Property p2 = create(Property.class);
        Class c1 = create(Class.class);
        p2.setType(c1);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, p2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Port} => not authorized.
     */
    @Test
    public void testPropertyDropOnPort() {
        Property p1 = create(Property.class);
        Port port = create(Port.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, port);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testPropertyDropOnClass() {
        Property p1 = create(Property.class);
        Class c1 = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, c1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Comment} => not authorized.
     */
    @Test
    public void testPropertyDropOnComment() {
        Property p1 = create(Property.class);
        Comment comment = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on typed {@link Property} => authorized.
     */
    @Test
    public void testPortDropOnTypedProperty() {
        Port port1 = create(Port.class);
        Property p2 = create(Property.class);
        Class c1 = create(Class.class);
        p2.setType(c1);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(port1, p2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on typed {@link Port} => authorized.
     */
    @Test
    public void testPortDropOnTypedPort() {
        Port port1 = create(Port.class);
        Port port2 = create(Port.class);
        Class c1 = create(Class.class);
        port2.setType(c1);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(port1, port2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link StructuredClassifier} => authorized.
     */
    @Test
    public void testPortDropOnClass() {
        Port port = create(Port.class);
        Class c1 = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, c1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Class} => authorized.
     */
    @Test
    public void testClassDropOnClass() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(c1, c2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link org.eclipse.uml2.uml.Package} =>
     * authorized.
     */
    @Test
    public void testClassDropOnPackage() {
        Class c1 = create(Class.class);
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(c1, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Comment} => not authorized.
     */
    @Test
    public void testClassDropOnComment() {
        Class clazz = create(Class.class);
        Comment comment = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Class} => authorized.
     */
    @Test
    public void testCollaborationDropOnClass() {
        Collaboration collab = create(Collaboration.class);
        Class c2 = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collab, c2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link org.eclipse.uml2.uml.Package}
     * => authorized.
     */
    @Test
    public void testCollaborationDropOnPackage() {
        Collaboration collab = create(Collaboration.class);
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collab, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Comment} => not authorized.
     */
    @Test
    public void testCollaborationDropOnComment() {
        Collaboration collab = create(Collaboration.class);
        Comment comment = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collab, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Element} => authorized.
     */
    @Test
    public void testCommentDropOnPackage() {
        Comment comment1 = create(Comment.class);
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment1, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Comment} => not authorized.
     */
    @Test
    public void testCommentDropOnComment() {
        Comment comment1 = create(Comment.class);
        Comment comment2 = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment1, comment2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on non {@link Element} => not authorized.
     */
    @Test
    public void testCommentDropOnEObject() {
        EPackage ePackage = EcoreFactory.eINSTANCE.createEPackage();
        Comment comment1 = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment1, ePackage);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Namespace} => authorized.
     */
    @Test
    public void testConstraintDropOnPackage() {
        Constraint constraint1 = create(Constraint.class);
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint1, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on non {@link Namespace} => not
     * authorized.
     */
    @Test
    public void testConstraintDropOnComment() {
        Constraint constraint1 = create(Constraint.class);
        Comment comment2 = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint1, comment2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Class} => authorized.
     */
    @Test
    public void testInformationItemDropOnClass() {
        InformationItem informationItem = create(InformationItem.class);
        Class c2 = create(Class.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, c2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on
     * {@link org.eclipse.uml2.uml.Package} => authorized.
     */
    @Test
    public void testInformationItemDropOnPackage() {
        InformationItem informationItem = create(InformationItem.class);
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Comment} => not authorized.
     */
    @Test
    public void testInformationItemDropOnComment() {
        InformationItem informationItem = create(InformationItem.class);
        Comment comment = create(Comment.class);

        CheckStatus canDragAndDropStatus = compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

}

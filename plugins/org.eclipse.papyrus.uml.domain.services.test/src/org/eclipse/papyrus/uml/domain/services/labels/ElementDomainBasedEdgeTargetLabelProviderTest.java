/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeTargetLabelProviderTest}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeTargetLabelProviderTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeTargetLabelProvider labelProvider = ElementDomainBasedEdgeTargetLabelProvider
            .buildDefault();

    /**
     * Test source label for {@link Association}.
     */
    @Test
    public void testAssociationTargetLabel() {
        Property source = create(Property.class);
        Property target = create(Property.class);
        target.setName("myProp");
        Association association = create(Association.class);
        association.getMemberEnds().add(target);
        association.getMemberEnds().add(source);

        assertEquals("+ myProp 1", labelProvider.getLabel(association, null));

        target.setUpper(-1);
        assertEquals("+ myProp 1..*", labelProvider.getLabel(association, null));

        target.setLower(0);
        assertEquals("+ myProp *", labelProvider.getLabel(association, null));

        target.setLower(5);
        target.setUpper(6);
        assertEquals("+ myProp 5..6", labelProvider.getLabel(association, null));
    }

    /**
     * Test source label for {@link Connector}.
     */
    @Test
    public void testConnectorTargetLabel() {

        Port source = create(Port.class);
        Port target = create(Port.class);

        Connector connector = create(Connector.class);
        ConnectorEnd conEndSource = createIn(ConnectorEnd.class, connector);
        conEndSource.setRole(source);

        ConnectorEnd conEndTarget = createIn(ConnectorEnd.class, connector);
        conEndTarget.setRole(target);

        assertEquals("1", labelProvider.getLabel(connector, target));

        conEndTarget.setUpper(-1);
        assertEquals("1..*", labelProvider.getLabel(connector, target));

        conEndTarget.setLower(0);
        assertEquals("*", labelProvider.getLabel(connector, target));

        conEndTarget.setLower(5);
        conEndTarget.setUpper(6);
        assertEquals("5..6", labelProvider.getLabel(connector, target));
    }

}

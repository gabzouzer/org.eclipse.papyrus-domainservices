/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeSourceProviderTest}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeSourceProviderTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeSourceProvider sourceProvider = new ElementDomainBasedEdgeSourceProvider();

    /**
     * Tests source provider on dependency (and some inherited element).
     */
    @Test
    public void caseDependency() {

        Dependency dependency = create(Dependency.class);
        assertNull(sourceProvider.getSource(dependency));

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        dependency.getClients().add(class1);
        assertEquals(class1, sourceProvider.getSource(dependency));
    }

    /**
     * Tests the source of an InterfaceRealization.
     */
    @Test
    public void caseInterfaceRealization() {
        InterfaceRealization interfaceRealization = create(InterfaceRealization.class);
        assertNull(sourceProvider.getSource(interfaceRealization));

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        interfaceRealization.setImplementingClassifier(class1);
        assertEquals(class1, sourceProvider.getSource(interfaceRealization));
    }

    /**
     * Tests source provider for {@link Extension}.
     */
    @Test
    public void testExtension() {
        Extension extension = create(Extension.class);
        ExtensionEnd extensionEnd = create(ExtensionEnd.class);
        extension.getOwnedEnds().add(extensionEnd);
        Stereotype stereotype = create(Stereotype.class);
        extensionEnd.setType(stereotype);
        Property property = create(Property.class);
        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        property.setType(class1);
        property.setAssociation(extension);
        extension.getMemberEnds().add(property);
        stereotype.getOwnedAttributes().add(property);
        assertEquals(stereotype, sourceProvider.getSource(extension));

    }

    /**
     * Tests source provider on Generalization.
     */
    @Test
    public void caseGeneralization() {

        Generalization generalization = create(Generalization.class);
        assertNull(sourceProvider.getSource(generalization));

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        generalization.setSpecific(class1);
        assertEquals(class1, sourceProvider.getSource(generalization));
    }

    /**
     * Tests source provider on {link Manifestation}.
     */
    @Test
    public void caseManifestation() {

        Manifestation manifestation = create(Manifestation.class);
        assertNull(sourceProvider.getSource(manifestation));

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        manifestation.getClients().add(class1);
        assertEquals(class1, sourceProvider.getSource(manifestation));
    }

    /**
     * Tests source provider on {link Substitution}.
     */
    @Test
    public void caseSubstitution() {

        Substitution substitution = create(Substitution.class);
        assertNull(sourceProvider.getSource(substitution));

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        substitution.setSubstitutingClassifier(class1);
        assertEquals(class1, sourceProvider.getSource(substitution));
    }

    /**
     * Tests source provider on {link PackageImport}.
     */
    @Test
    public void casePackageImport() {
        PackageImport element = create(PackageImport.class);
        assertNull(sourceProvider.getSource(element));

        Package src = create(org.eclipse.uml2.uml.Package.class);
        element.setImportingNamespace(src);
        assertEquals(src, sourceProvider.getSource(element));
    }

    /**
     * Tests source provider on {link PackageMerge}.
     */
    @Test
    public void casePackageMerge() {
        PackageMerge element = create(PackageMerge.class);
        assertNull(sourceProvider.getSource(element));

        Package src = create(org.eclipse.uml2.uml.Package.class);
        element.setReceivingPackage(src);
        assertEquals(src, sourceProvider.getSource(element));
    }

    @Test
    public void caseTransition() {
        Pseudostate source = create(Pseudostate.class);
        Pseudostate target = create(Pseudostate.class);

        Transition transition = create(Transition.class);
        transition.setSource(source);
        transition.setTarget(target);

        assertEquals(source, sourceProvider.getSource(transition));
    }

    @Test
    public void caseAssociation() {
        Association association = create(Association.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);

        new ElementDomainBasedEdgeInitializer().initialize(association, source, target, null, null, null);

        assertEquals(source, sourceProvider.getSource(association));

        // if type of target Property is null, none source is found
        association.getMemberEnds().get(1).setType(null);
        assertEquals(null, sourceProvider.getSource(association));
    }

    @Test
    public void caseInclude() {
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(include, source, target, null, null, null);

        assertEquals(source, sourceProvider.getSource(include));
    }

    /**
     * Tests source provider on {@link InformationFlow}.
     */
    @Test
    public void caseInformationFlow() {

        InformationFlow informationFlow = create(InformationFlow.class);
        assertNull(sourceProvider.getSource(informationFlow));

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        informationFlow.getInformationSources().add(class1);
        assertEquals(class1, sourceProvider.getSource(informationFlow));
    }

    @Test
    public void caseExtend() {
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(extend, source, target, null, null, null);

        assertEquals(source, sourceProvider.getSource(extend));
    }

}

/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectTargetBehaviorProviderTest extends AbstractUMLTest {

    /* Name class2 */
    private static final String CLASS2 = "class2";

    /* Name class1 */
    private static final String CLASS1 = "class1";

    private static final String STEREOTYPE = "stereotype";

    /**
     * Test the target reconnection for a connector on a property port.
     */
    @Test
    public void testConnectorTargetReconnectOnPropertyPort() {
        // Create semantic elements
        Class class1 = create(Class.class);
        Port oldTargetPort = createIn(Port.class, class1);
        Port newTargetPort = createIn(Port.class, class1);
        Port sourcePort = createIn(Port.class, class1);
        Property property = create(Property.class);
        property.setType(class1);
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEndSource = createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(sourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(oldTargetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode propertyNode = visualTree.addChildren(property);
        VisualNode visualNewTargetPort = propertyNode.addChildren(newTargetPort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(connector, oldTargetPort, newTargetPort, visualNewTargetPort);
        assertTrue(status.isValid());
        assertEquals(newTargetPort, connectorEndTarget.getRole(),
                "The new source port should have changed after the reconnect.");
        assertEquals(property, connectorEndTarget.getPartWithPort());
    }

    /**
     * Test the target reconnection for a connector on a classifier port. This test
     * aims to check whether the partWithPort attribute is set at null.
     */
    @Test
    public void testConnectorTargetReconnectOnClassifierPort() {
        // Create semantic elements
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Port oldTargetPort = createIn(Port.class, class1);
        Port newTargetPort = createIn(Port.class, class2);
        Port sourcePort = createIn(Port.class, class1);
        Property property = create(Property.class);
        property.setType(class1);
        Connector connector = create(Connector.class);
        ConnectorEnd connectorEndSource = createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(sourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(oldTargetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode class2Node = visualTree.addChildren(class2);
        VisualNode visualNewTargetPort = class2Node.addChildren(newTargetPort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(connector, oldTargetPort, newTargetPort, visualNewTargetPort);
        assertTrue(status.isValid());
        assertEquals(newTargetPort, connectorEndTarget.getRole(),
                "The new source port should have changed after the reconnect.");
        assertNull(connectorEndTarget.getPartWithPort(), "The old part with port should have been removed");
    }

    /**
     * Test reconnect {@link Extension} target.
     */
    @Test
    public void testExtensionTargetReconnect() {
        Profile profile = create(Profile.class);
        Stereotype stereotype = create(Stereotype.class);
        stereotype.setName(STEREOTYPE);
        Class class1 = create(Class.class);
        class1.setName(CLASS1);
        Class class2 = create(Class.class);
        class2.setName(CLASS2);
        Extension extension = createIn(Extension.class, profile);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(extension, extension.eContainer());
        assertEquals("E_stereotype_class11", extension.getName());
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(extension, class1, class2, null);
        assertTrue(status.isValid());
        assertEquals(class2, extension.getMetaclass());
        assertEquals("E_stereotype_class21", extension.getName());

        // now check that the name is unchanged if the user has customized it.
        extension.setName("customName");
        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(extension, class2, class1, null);
        assertTrue(status.isValid());
        assertEquals(class1, extension.getMetaclass());
        assertEquals("customName", extension.getName());
    }

    /**
     * Default reconnection for PackageMerge.
     */
    @Test
    public void testPackageMergeTargetReconnect() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(merge, packTarget, packTarget2, null);
        assertTrue(status.isValid());
        assertEquals(packTarget2, merge.getMergedPackage());
    }

    /**
     * Default reconnection for InterfaceRealization.
     */
    @Test
    public void testIntefaceRealizationReconnect() {

        Class classSource = create(Class.class);
        Interface interfaceTarget = create(Interface.class);
        Interface interfaceTarget2 = create(Interface.class);

        InterfaceRealization interfaceRealization = createIn(InterfaceRealization.class, classSource);
        interfaceRealization.setImplementingClassifier(classSource);
        interfaceRealization.setContract(interfaceTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(interfaceRealization, interfaceTarget, interfaceTarget2, null);
        assertTrue(status.isValid());
        assertEquals(interfaceTarget2, interfaceRealization.getContract());
    }

    /**
     * Default reconnection for PackageImport.
     */
    @Test
    public void testPackageImportTargetReconnect() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(packImport, packTarget, packTarget2, null);
        assertTrue(status.isValid());
        assertEquals(packTarget2, packImport.getImportedPackage());
    }

    /**
     * Test reconnect target with null dependency or null old target or null new
     * target.
     */
    @Test
    public void testDependencyTargetReconnectWithNullArguments() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(null, c1, c2, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, c1, null, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, null, c1, null);

        assertFalse(status.isValid());

    }

    /**
     * Test reconnect target of dependency on an other target in the same package.
     * Expected result : target changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencyTargetReconnectInSamePackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, c2, c3, null);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getSuppliers().size());
        assertEquals(c3, dependency.getSuppliers().get(0));
    }

    /**
     * Test reconnect target of dependency on an other target in different package.
     * Expected result : target changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencyTargetReconnectInDifferentPackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        org.eclipse.uml2.uml.Package p2 = create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, c2, c3, null);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getSuppliers().size());
        assertEquals(c3, dependency.getSuppliers().get(0));
    }

    /**
     * Test reconnect target of {@link Extend} on an other {@link UseCase} source.
     */
    @Test
    public void testExtendTargetReconnect() {
        UseCase target = create(UseCase.class);
        UseCase newTarget = create(UseCase.class);

        Extend extend = create(Extend.class);
        extend.setExtendedCase(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(extend, target, newTarget, null);

        assertTrue(status.isValid());
        assertEquals(newTarget, extend.getExtendedCase());
    }

    @Test
    public void testTransitionTargetReconnect() {
        State state1 = create(State.class);
        Pseudostate source = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate target1 = createIn(Pseudostate.class, region);
        Pseudostate target2 = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source);
        transition.setTarget(target1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(transition, target1, target2, null);

        assertTrue(status.isValid());
        assertEquals(region, transition.getOwner());
        assertEquals(source, transition.getSource());
        assertEquals(target2, transition.getTarget());
    }

    /**
     * Test reconnect target of association on an other target in different actor.
     * Expected result : target changed
     */
    @Test
    public void testAssociationTargetReconnect() {
        Actor source = create(Actor.class);
        Property sourceProp = create(Property.class);
        sourceProp.setType(source);

        Actor oldTarget = create(Actor.class);
        Property targetProp = create(Property.class);
        targetProp.setType(oldTarget);

        Actor newTarget = create(Actor.class);

        Association association = create(Association.class);
        association.getMemberEnds().add(targetProp);
        association.getMemberEnds().add(sourceProp);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(association, oldTarget, newTarget, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(source, association.getMemberEnds().get(1).getType());
        assertEquals(newTarget, association.getMemberEnds().get(0).getType());

    }

    /**
     * Test reconnect target of association on an other target (target property is
     * owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyOwnedByAssociation() {
        Model model = create(Model.class);
        Class classSource = createIn(Class.class, model);
        Class classOldTarget = createIn(Class.class, model);
        classOldTarget.setName(CLASS1);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);
        Class classNewTarget = createIn(Class.class, model);
        classNewTarget.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(association, classOldTarget, classNewTarget, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(2, association.getOwnedMembers().size());
        Property targetProperty = association.getMemberEnds().get(1);
        assertEquals(classSource, targetProperty.getType());
        Property sourceProperty = association.getMemberEnds().get(0);
        assertEquals(classNewTarget, sourceProperty.getType());
        assertEquals(classOldTarget.getName(), sourceProperty.getName());
    }

    /**
     * Test reconnect target of association on an other target (target property is
     * not owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyNotOwnedByAssociation() {
        Model model = create(Model.class);
        Class classSource = createIn(Class.class, model);
        Class classOldTarget = createIn(Class.class, model);
        classOldTarget.setName(CLASS1);
        Association association = createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);

        // change owner of target Property
        Property targetProperty = association.getMemberEnds().get(1);
        classOldTarget.getOwnedAttributes().add(targetProperty);
        Class classNewTarget = createIn(Class.class, model);
        classNewTarget.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(association, classOldTarget, classNewTarget, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(1, association.getOwnedMembers().size());
        assertFalse(classOldTarget.getOwnedAttributes().contains(targetProperty));
        assertTrue(classNewTarget.getOwnedAttributes().contains(targetProperty));
        Property sourceProperty = association.getMemberEnds().get(0);
        assertEquals(classNewTarget, sourceProperty.getType());
        assertEquals(classOldTarget.getName(), sourceProperty.getName());
    }

    @Test
    public void testGeneralizationTargetReconnect() {
        UseCase target = create(UseCase.class);
        UseCase target2 = create(UseCase.class);

        Generalization generalization = create(Generalization.class);
        generalization.setGeneral(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(generalization, target, target2, null);

        assertTrue(status.isValid());
        assertEquals(target2, generalization.getGeneral());
    }

    /**
     * Default target reconnection for {@link Include}.
     */
    @Test
    public void testIncludeTargetReconnect() {

        UseCase target = create(UseCase.class);
        UseCase target2 = create(UseCase.class);

        Include include = create(Include.class);
        include.setAddition(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(include, target, target2, null);

        assertTrue(status.isValid());
        assertEquals(target2, include.getAddition());
    }

    /**
     * Default target reconnection for {@link Substitution}.
     */
    @Test
    public void testSubstitutionTargetReconnect() {
        Class clazzOldTarget = create(Class.class);
        Class clazzNewTarget = create(Class.class);
        Class clazzSource = create(Class.class);
        Substitution substitution = create(Substitution.class);
        clazzSource.getSubstitutions().add(substitution);
        new ElementDomainBasedEdgeInitializer().initialize(substitution, clazzSource, clazzOldTarget, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(substitution, clazzOldTarget, clazzNewTarget, null);

        assertTrue(status.isValid());
        assertEquals(clazzNewTarget, substitution.getContract());
    }

    /**
     * Default target reconnection for {@link Manifestation}.
     */
    @Test
    public void testManifestationTargetReconnect() {
        Class target = create(Class.class);
        Class source = create(Class.class);
        Class newTarget = create(Class.class);
        Manifestation manifestation = create(Manifestation.class);
        new ElementDomainBasedEdgeInitializer().initialize(manifestation, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(manifestation, target, newTarget, null);
        assertTrue(status.isValid());
        assertTrue(manifestation.getUtilizedElement().equals(newTarget));
    }

    /**
     * Default target reconnection for {@link InformationFlowo}.
     */
    @Test
    public void testInformationFlowTargetReconnect() {
        Package package1 = create(Package.class);
        Class target = createIn(Class.class, package1);
        Class source = createIn(Class.class, package1);
        Class newTarget = createIn(Class.class, package1);
        InformationFlow informationFlow = createIn(InformationFlow.class, package1);
        new ElementDomainBasedEdgeInitializer().initialize(informationFlow, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(informationFlow, target, newTarget, null);
        assertTrue(status.isValid());
        assertTrue(informationFlow.getInformationTargets().size() == 1);
        assertTrue(informationFlow.getInformationTargets().contains(newTarget));
    }
}
